from typing import List, Tuple

from dolfin import Constant, assemble, dx
from scipy.integrate import solve_ivp
from scipy.interpolate import interp1d
from sklearn.metrics import mean_squared_error, mean_absolute_percentage_error, mean_squared_log_error, \
    mean_absolute_error

import scipy
import scipy.optimize

from dosimetry.boundary import LinearFluxCellBoundaryConditionMaker
from dosimetry.initial_condition import UniformInitialConditionMaker
from dosimetry.model_maker import standard_constant_advection_diffusion_greedy_model_builder
from dosimetry.solver.standard import Solution, Model, TimestepLinearModelSolver, \
    ConstantAdvectionDiffusionNoFlux1DModelMaker
from dosimetry.solver.updating_boundary import AbstractSolutionDependentBoundaryConditionMaker, \
    updating_linear_boundary_solver
from dosimetry.solver.utility import solution_saver, progress_bar
from fitter.single_parameter_fitter import ParameterFitter
from utility.config_interpreter import ConfigInterpreter
from utility.config_to_solutions import get_adherent_rate_correction, \
    max_capacity_from_last_derivative_compared_to_max_derivative, ode_solutions_from_rate_function, \
    get_ODE_scaling_factor, _get_solutions, linear_1D_PDE
import numpy as np

get_carrying_capacity_equation = max_capacity_from_last_derivative_compared_to_max_derivative


def always_110_carrying_capacity(config):
    return 110


def always_1k_carrying_capacity(config):
    return 1000


def always_10_carrying_capacity(config):
    return 10


class Bunch:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)


class AssociationTranscytosedSolution:
    def __init__(self, uncellular_amount, associated_amount, transcytosed_amount):
        self.uncellular_amount = uncellular_amount
        self.associated_amount = associated_amount
        self.transcytosed_amount = transcytosed_amount

    def __str__(self):
        return "In solution: {:.2e}, Associated: {:.2e}, Transcytosed: {:.2e}".format(
            self.uncellular_amount, self.associated_amount, self.transcytosed_amount)


class LinearTranscytosisTimestepLinearModelSolver(TimestepLinearModelSolver):
    def __init__(self, transcytosis_rate: float):
        super().__init__()
        self.transcytosis_rate = transcytosis_rate

    def init_solver_loop(self, model):
        super().init_solver_loop(model)
        self.associated = 0
        self.transcytosed = 0

    def solve_timestep(self, model):
        super().solve_timestep(model)
        last_remaining = assemble(self._u_1 * dx)
        remaining = assemble(self._u * dx)

        self.associated = (self.associated + last_remaining - remaining) / (
                1 + model.dt * self.transcytosis_rate)  # derivation from backwards differencing linear rate
        self.transcytosed = self.transcytosed + model.dt * self.transcytosis_rate * self.associated

    def get_solution(self):
        end_amt = assemble(self._u * dx)
        total_start = self._start_amount
        total_end = end_amt + self.associated + self.transcytosed
        unaccounted_for = total_start - total_end
        if abs(unaccounted_for) > total_start / 1000000:
            raise ValueError('Unaccounted for mass in system!')

        solution = AssociationTranscytosedSolution(uncellular_amount=end_amt,
                                                   associated_amount=self.associated,
                                                   transcytosed_amount=self.transcytosed)
        return solution


def linear_constant_supply_adherent_ODE(config: ConfigInterpreter,
                                        association_rate: float,  # unused
                                        transcytosis_rate: float,
                                        mesh_scaling=0.8):
    culture = config.experimental_and_cell_detail.cell_culture_condition
    initial_amount = culture.particle_concentration * culture.volume
    # estimate_max_capacity = max_capacity_from_last_derivative_compared_to_max_derivative(config)

    scaling = get_ODE_scaling_factor(config)

    def linear_rate_fn():
        return scaling * transcytosis_rate * initial_amount

    # y[0] is amount IN SOLUTION, y[1] is amount associated, y[2] is amount transcytosed
    def ode_model(t, y):
        dydt = [0, 0, linear_rate_fn()]
        return dydt

    sol = solve_ivp(ode_model,
                    t_span=[0, max(config.times) * 60],
                    y0=[initial_amount, -1, 0])
    return sol


def linear_static_adherent_PDE(config: ConfigInterpreter,
                               association_rate: float,  # unused
                               transcytosis_rate: float,
                               mesh_scaling=0.8):
    sols = linear_1D_PDE(config, transcytosis_rate, mesh_scaling)

    t = np.array([s[0] for s in sols])
    y0s = [s[1].absolute_amount_remaining for s in sols]
    y1s = [-1 for s in sols]  # don't know associated amount, setting it to -1
    y2s = [s[1].absolute_amount_removed for s in sols]  # treating associated amount as transcytosed amount
    y = np.array([y0s, y1s, y2s])
    ans = Bunch(t=t, y=y)
    return ans


class CellCarryingCapacityAndTranscytosisAssociationRate(AbstractSolutionDependentBoundaryConditionMaker):
    def __init__(self, assoc_rate, capacity):
        self.assoc_rate = assoc_rate
        self.capacity = capacity

    def calculate_rate(self, solution: AssociationTranscytosedSolution):
        n = solution.associated_amount
        cc = self.capacity
        real_rate = self.assoc_rate * (cc - n) / cc
        return real_rate

    def update_boundaries(self, solution: AssociationTranscytosedSolution, model: Model):
        rate = self.calculate_rate(solution)
        bc_maker = LinearFluxCellBoundaryConditionMaker(rate)
        bc_maker.setup_boundaries(model)

    def setup_boundaries(self, model: Model):
        none_associated_solution = AssociationTranscytosedSolution(100.0, 0, 0)  # 100 is arbitrary
        self.update_boundaries(none_associated_solution, model)


class SafeCellCarryingCapacityAndTranscytosisAssociationRate(CellCarryingCapacityAndTranscytosisAssociationRate):
    def calculate_rate(self, solution: AssociationTranscytosedSolution):
        real_rate = super().calculate_rate(self, solution)
        return max(real_rate, 0.0)


def cell_carrying_capacity_linear_transcytosis_1D_PDE(config: ConfigInterpreter,
                                                      association_rate: float,
                                                      transcytosis_rate: float,
                                                      mesh_scaling=0.8) -> List[
    Tuple[float, AssociationTranscytosedSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(
        exp_detail,
        ConstantAdvectionDiffusionNoFlux1DModelMaker,
        mesh_scaling)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)

    model_solver_maker = LinearTranscytosisTimestepLinearModelSolver
    model_solver_maker = updating_linear_boundary_solver(model_solver_maker)
    model_solver_maker.equation_maker = model_builder.equation_maker

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * association_rate

    estimate_max_capacity = get_carrying_capacity_equation(config)

    boundary_conditioner = CellCarryingCapacityAndTranscytosisAssociationRate(corrected_rate, estimate_max_capacity)
    model_builder.boundary_condition_maker = boundary_conditioner

    model_solver_maker.bc_updater = CellCarryingCapacityAndTranscytosisAssociationRate(corrected_rate,
                                                                                       estimate_max_capacity)
    model_solver_maker = solution_saver(progress_bar(model_solver_maker))
    model_solver = model_solver_maker(transcytosis_rate)
    model = model_builder.build_model()
    model_solver.solve_model(model)
    sols = model_solver.solutions
    t = np.array([s[0] for s in sols])
    y0s = [s[1].uncellular_amount for s in sols]
    y1s = [s[1].associated_amount for s in sols]
    y2s = [s[1].transcytosed_amount for s in sols]
    y = np.array([y0s, y1s, y2s])
    ans = Bunch(t=t, y=y)
    return ans


def transcytosis_cell_carrying_capacity_adherent_constant_concentration_well_mixed_ODE(config: ConfigInterpreter,
                                                                                       association_rate: float,
                                                                                       transcytosis_rate: float,
                                                                                       mesh_scaling: float = 0):
    culture = config.experimental_and_cell_detail.cell_culture_condition
    initial_amount = culture.particle_concentration * culture.volume
    estimate_max_capacity = get_carrying_capacity_equation(config)

    scaling = get_ODE_scaling_factor(config)

    def carrying_capacity_rate_fn(amount_associated):
        current_rate = scaling * association_rate * (estimate_max_capacity - amount_associated) / estimate_max_capacity
        return current_rate * initial_amount

    def transcytosis_rate_fn(amount_associated):
        return scaling * transcytosis_rate * amount_associated

    # y[0] is amount outside of cells, y[1] is associated amount, y[2] is amount transcytosed)
    def ode_model(t, y):
        assoc = y[1]
        dydt = [0, carrying_capacity_rate_fn(assoc) - transcytosis_rate_fn(assoc), transcytosis_rate_fn(assoc)]
        return dydt

    sol = solve_ivp(ode_model,
                    t_span=[0, max(config.times) * 60],
                    y0=[initial_amount, 0, 0])
    return sol


def association_only_cell_carrying_capacity_adherent_constant_concentration_well_mixed_ODE(config: ConfigInterpreter,
                                                                                           association_rate: float,
                                                                                           transcytosis_rate: float,
                                                                                           mesh_scaling: float = 0):
    culture = config.experimental_and_cell_detail.cell_culture_condition
    initial_amount = culture.particle_concentration * culture.volume
    estimate_max_capacity = get_carrying_capacity_equation(config)

    scaling = get_ODE_scaling_factor(config)

    def carrying_capacity_rate_fn(amount_associated):
        current_rate = scaling * association_rate * (estimate_max_capacity - amount_associated) / estimate_max_capacity
        return current_rate * initial_amount

    # y[0] is amount outside of cells, y[1] is associated amount, y[2] is amount transcytosed)
    def ode_model(t, y):
        assoc = y[1]
        dydt = [0, carrying_capacity_rate_fn(assoc), 0]
        return dydt

    sol = solve_ivp(ode_model,
                    t_span=[0, max(config.times) * 60],
                    y0=[initial_amount, 0, 0])
    return sol


# def get_transcytosis_rate_parameter_fitter(config: ConfigInterpreter, config_to_solutions, association_rate, mesh_scaling):
def equal_weight_percentage_error(real, est):
    err1 = mean_absolute_percentage_error(real[0, :], est[0, :])
    err2 = mean_absolute_percentage_error(real[1, :], est[1, :])
    return (err1 + err2) / 2


num_interp_pts = 50
def interpolate_transcytosis_actual(actual: ConfigInterpreter):

    interp_times = np.linspace(min(actual.times), max(actual.times), num=num_interp_pts)

    interp_assoc = interp1d(actual.times, actual.associated_per_cell_means)(interp_times)
    interp_trans = interp1d(actual.times, actual.transcytosed_per_cell_means)(interp_times)
    both_interp = np.array([interp_assoc, interp_trans])
    return both_interp


def interpolate_transcytosis_solutions_to_times_extra_actual(config_to_solutions,
                                                             config: ConfigInterpreter,
                                                             rate,  # np array
                                                             mesh_scaling=0.8):  # returns numpy array
    sol = config_to_solutions(config, rate[0], rate[1], mesh_scaling)
    # sol is an OdeResult
    solved_times = sol.t / 60  # remember, solutions are in seconds, and configs are in minutes
    solved_assocs = sol.y[1]
    solved_trans = sol.y[2]

    interp_times = np.linspace(min(config.times), max(config.times), num=num_interp_pts)

    interp_assoc = interp1d(solved_times, solved_assocs)(interp_times)
    interp_trans = interp1d(solved_times, solved_trans)(interp_times)
    both_interp = np.array([interp_assoc, interp_trans])
    return both_interp


def interpolate_transcytosis_solutions_to_times(config_to_solutions,
                                                config: ConfigInterpreter,
                                                rate,  # np array
                                                mesh_scaling=0.8):  # returns numpy array
    sol = config_to_solutions(config, rate[0], rate[1], mesh_scaling)
    # sol is an OdeResult
    solved_times = sol.t / 60  # remember, solutions are in seconds, and configs are in minutes
    solved_assocs = sol.y[1]
    solved_trans = sol.y[2]

    interp_times = np.linspace(min(config.times), max(config.times), num=num_interp_pts)

    interp_assoc = interp1d(solved_times, solved_assocs)(config.times)
    interp_trans = interp1d(solved_times, solved_trans)(config.times)
    both_interp = np.array([interp_assoc, interp_trans])
    return both_interp

def both_rate_parameter_fitter(config_to_solutions, mesh_scaling, first_guess=[0, 0]):
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    rate_scale = 1e-9
    minimizer = lambda obj_fn: scipy.optimize.minimize(obj_fn, x0=np.array(first_guess))
    # assocd_and_transd = lambda c: interpolate_transcytosis_actual(c)
    assocd_and_transd = lambda c: np.array([c.associated_per_cell_means, c.transcytosed_per_cell_means])

    fitter = ParameterFitter(solution_interpolator=interpolate_transcytosis_solutions_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=mean_sq,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=None,
                             get_max_config_fn=None,
                             get_values_fn=assocd_and_transd,
                             minimizer_fn=minimizer)
    return fitter


def both_rate_parameter_fitter_extra(config_to_solutions, mesh_scaling, first_guess=[0, 0]):
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    rate_scale = 1e-9
    minimizer = lambda obj_fn: scipy.optimize.minimize(obj_fn, x0=np.array(first_guess), bounds=((0, 1e12), (0, 1e12)))
    assocd_and_transd = lambda c: interpolate_transcytosis_actual(c)
    # assocd_and_transd = np.array([c.associated_per_cell_means, c.transcytosed_per_cell_means])

    fitter = ParameterFitter(solution_interpolator=interpolate_transcytosis_solutions_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=mean_sq,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=None,
                             get_max_config_fn=None,
                             get_values_fn=assocd_and_transd,
                             minimizer_fn=minimizer)
    return fitter


def association_only_rate_parameter_fitter(config_to_solutions,
                                           transcytosis_rate,
                                           mesh_scaling):
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    rate_scale = 1e-9
    assocd = lambda c: np.array(c.associated_per_cell_means)

    def interpolate_sols_to_times(config_to_solutions, config, assoc_rate, mesh_scaling):
        both_rate = [assoc_rate, transcytosis_rate]
        both_sol = interpolate_transcytosis_solutions_to_times(config_to_solutions, config, both_rate, mesh_scaling)
        return both_sol[0]

    fitter = ParameterFitter(solution_interpolator=interpolate_sols_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=mean_sq,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=None,
                             get_max_config_fn=None,
                             get_values_fn=assocd,
                             minimizer_fn=lambda f: scipy.optimize.minimize_scalar(f))
    return fitter


def transcytosis_only_rate_parameter_fitter(config_to_solutions,
                                            association_rate,
                                            mesh_scaling):
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    rate_scale = 1e-9
    transd = lambda c: np.array(c.transcytosed_per_cell_means)

    def interpolate_sols_to_times(config_to_solutions, config, trans_rate, mesh_scaling):
        both_rate = [association_rate, trans_rate]
        both_sol = interpolate_transcytosis_solutions_to_times(config_to_solutions, config, both_rate, mesh_scaling)
        return both_sol[1]

    fitter = ParameterFitter(solution_interpolator=interpolate_sols_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=mean_sq,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=None,
                             get_max_config_fn=None,
                             get_values_fn=transd,
                             minimizer_fn=lambda f: scipy.optimize.minimize_scalar(f))
    return fitter


def combined_model(static_model, fluid_model):
    def get_combined_model(config: ConfigInterpreter,
                           association_rate: float,
                           transcytosis_rate: float,
                           mesh_scaling: float = 0.8):
        condition = config.config['model'].get('condition', 'static_adherent')
        if condition == 'static_adherent':
            return static_model(config, association_rate, transcytosis_rate, mesh_scaling)
        else:
            return fluid_model(config, association_rate, transcytosis_rate, mesh_scaling)

    return get_combined_model


def scipy_ode_sol_to_transcytosis_solutions(soln):
    sols = [AssociationTranscytosedSolution(uncellular_amount=pt[0],
                                            associated_amount=pt[1],
                                            transcytosed_amount=pt[2]) for pt in soln.y.transpose()]
    time_with_sols = list(zip(soln.t, sols))
    return time_with_sols
