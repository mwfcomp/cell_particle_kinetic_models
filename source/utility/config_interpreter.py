from typing import *
from configparser import ConfigParser
from dosimetry.solver.standard import Solution
from dosimetry.experimental_detail import *

import json
from flow_ctyometry.experiment import ChannelInfo, FullIncubationExperiment
from flow_ctyometry.metrics import AbstractMetric, ControlTransformWorkflow
import utility.flow_utility
import os.path
import pandas as pd
import numpy as np
import multiprocessing

from math import isnan
from scipy.stats import sem


configReaderLock = multiprocessing.Lock()

class ConfigInterpreter:
    def __init__(self, config_file: str,
                 exp_to_metric_fn: Callable[[FullIncubationExperiment], AbstractMetric],
                 exp_to_dispersion_fn: Callable[[FullIncubationExperiment], AbstractMetric],
                 preprocess_workflow_fn: Callable[[ChannelInfo], ControlTransformWorkflow]):
        with configReaderLock: #because we create a modified FCS file (and then delete it) while loading a configInterpreter
            #need a lock to keep it safe for multiprocessing. This is a lazy way to do it.
            self.config_file = config_file
            self.exp_to_metric_fn = exp_to_metric_fn
            self.exp_to_dispersion_fn = exp_to_dispersion_fn
            self.preprocess_workflow_fn = preprocess_workflow_fn
            utility.flow_utility.flow_filter_workflow = preprocess_workflow_fn
            self.config = ConfigParser()
            self.config.read(self.config_file)
            self.times = []
            self.all_associated_per_cell_values = []
            self.all_associated_per_cell_dispersions = []
            self.all_transcytosed_per_cell_values = []
            self.all_transcytosed_per_cell_dispersions = []
            self._assign_times_vals_dispersions()
            self.associated_per_cell_means = [np.mean(vals) for vals in self.all_associated_per_cell_values]
            self.associated_per_cell_means_std_dev = [np.std(vals) for vals in self.all_associated_per_cell_values]
            self.transcytosed_per_cell_means = [np.mean(vals) for vals in self.all_transcytosed_per_cell_values]
            self.transcytosed_per_cell_means_std_dev = [np.std(vals) for vals in self.all_transcytosed_per_cell_values]

            self.experimental_and_cell_detail = self._get_experimental_and_cell_detail()

    def __deepcopy__(self, memo): # this could be a lot more efficient (we don't have to re-read all the values)
        return ConfigInterpreter(self.config_file, self.exp_to_metric_fn,
                                 self.exp_to_metric_fn, self.preprocess_workflow_fn)

    @staticmethod
    def times_vals_dispersions_from_pre_specified_ini(config: ConfigParser, value_name) -> (Iterable[int], Iterable[float]):
        time_pt_sections = [s for s in config.sections() if s.startswith('time_point')]
        times = []
        all_vals = []
        for section in time_pt_sections:
            time = float(config[section]['time_in_mins'])
            vals = json.loads(config[section][value_name])
            times.append(time)
            all_vals.append(vals)
        #assuming we want dispersion to be standard error of the mean
        all_dispersions = [sem(vals) for vals in all_vals]
        all_dispersions = [0.0 if isnan(d) else d for d in all_dispersions]
        return times, all_vals, all_dispersions



    def get_dataframe(self) -> pd.DataFrame:
        # dir, file = os.path.split(self.config_file)
        # file_name = '{}_data.csv'.format(os.path.splitext(file)[0])
        # full_file_name = os.path.join(dir, file_name)
        df = pd.DataFrame({
            'config': self.config_file,
            'particle_name': self.experimental_and_cell_detail.particle.name,
            'radius_in_m': self.experimental_and_cell_detail.particle.radius,
            'density_in_kg_per_m3': self.experimental_and_cell_detail.particle.density,
            'cell_line_name': self.experimental_and_cell_detail.cell_info.cell_line,
            'time_in_mins': self.times,
            'value': self.associated_per_cell_means,
            'value_std_dev': self.associated_per_cell_means_std_dev
        })
        return df

    def to_csv(self, suffix: str):
        basefile = os.path.splitext(self.config_file)[0]
        filename = '{}_{}.csv'.format(basefile, suffix)
        df = self.get_dataframe()
        df.to_csv(filename)

    def get_experiment_generator(self) -> Generator[FullIncubationExperiment, None, None]:
        return FullIncubationExperiment.incubation_generator_from_ini(self.config_file)

    def output_raw_flow_data(self, folder_name):
        exp_gen = FullIncubationExperiment.incubation_generator_from_ini(self.config_file)
        _, file = os.path.split(self.config_file)
        filename, _ = os.path.splitext(file)
        for ei, exp in enumerate(exp_gen):
            # SO brittle
            metric = self.exp_to_metric_fn(exp)
            for i, cdf in enumerate(exp.blank_controls):
                file_name = os.path.join(folder_name, '{}_{}m_ctrl_r{}.csv'.format(filename, exp.time, i))
                cdf.to_csv(file_name)
            for j, edf in enumerate(exp.experiment_data):
                file_name = os.path.join(folder_name, '{}_{}m_data_r{}.csv'.format(filename, exp.time, j))
                edf.to_csv(file_name)

            ppc = metric.calculate_all_particles_per_cell(exp)
            for i, pa in enumerate(ppc):
                ppc_df = pd.DataFrame(pa)
                ppc_filename = os.path.join(folder_name, '{}_{}m_particles_per_cell_r{}.csv'.format(filename, exp.time, i))
                ppc_df.to_csv(ppc_filename)

    def _get_experimental_and_cell_detail(self) -> ExperimentalAndCellDetail:
        cell_info = CellInfo(cell_line=self.config['cell']['cell_line_name'],
                             total_cells=float(self.config['cell']['total_cells']),
                             surface_area_per_cell=float(self.config['cell']['surface_area_per_cell_in_m2']))

        culture = self.config['culture']
        width = float(culture['width_in_m'])
        height = float(culture['height_in_m'])
        depth = float(culture.get('depth_in_m', '0'))
        if depth == 0:
            cell_culture = CylindricalCellCultureCondition(width=width, height=height,
                                                           temperature=float(culture['temp_in_k']),
                                                           time_in_seconds=self.times[-1] * 60,  #culture condition time is in seconds, config is in minutes
                                                           particle_concentration=0,
                                                           cell_orientation=CellOrientation.bottom,
                                                           flow_speed=0)
        else:
            cell_culture = RectangularCellCultureCondition(width=width, height=height, depth=depth,
                                                           temperature=float(culture['temp_in_k']),
                                                           time_in_seconds=self.times[-1] * 60,  #culture condition time is in seconds, config is in minutes
                                                           particle_concentration=0,
                                                           cell_orientation=CellOrientation.bottom,
                                                           flow_speed=0)



        conc = float(culture['particles_per_cell']) / (width * height)
        cell_culture.particle_concentration = conc
        particle_info = self.config['particle']
        particle = SphericalParticle(name=particle_info['particle_name'], radius=float(particle_info['radius_in_m']),
                                     density=float(particle_info['density_in_kg_per_m3']))

        media = LiquidMedia(viscosity=float(culture['media_viscosity_in_kg_per_m_s']),
                            density=float(culture['media_density_in_kg_per_m3']))

        experimental_detail = ExperimentalAndCellDetail(particle=particle, media=media,
                                                        cell_culture_condition=cell_culture,
                                                        cell_info=cell_info)
        return experimental_detail

    def _assign_times_vals_dispersions(self) -> None:
        particles_per_cell_specified = self.config['model'].get('particles_per_cell_determined', 'False') == 'True'
        particles_transcytosed_specified = self.config['model'].get('particles_per_cell_transcytosed_determined', 'False') == 'True'
        if particles_per_cell_specified:
            self.times, self.all_associated_per_cell_values, self.all_associated_per_cell_dispersions = \
                ConfigInterpreter.times_vals_dispersions_from_pre_specified_ini(self.config, 'particles_per_cell')
        else:
            exp_gen = FullIncubationExperiment.incubation_generator_from_ini(self.config_file)
            for exp in exp_gen:
                metric = self.exp_to_metric_fn(exp)
                dispersion_metric = self.exp_to_dispersion_fn(exp)
                vals = metric.calculate(exp)
                dispersions = dispersion_metric.calculate(exp)
                self.times.append(exp.time)
                self.all_associated_per_cell_values.append(vals)
                self.all_associated_per_cell_dispersions.append(dispersions)
        if particles_transcytosed_specified:
            _, self.all_transcytosed_per_cell_values, self.all_transcytosed_per_cell_dispersions = \
                ConfigInterpreter.times_vals_dispersions_from_pre_specified_ini(self.config, 'particles_transcytosed_per_cell')


