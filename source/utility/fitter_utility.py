import logging
from os.path import splitext

import dolfin.cpp
import fitter.mesh_refinement
import fitter.model_comparer

import scipy.optimize
from fitter import mesh_refinement
from fitter.single_parameter_fitter import new_config_change_density, min_config_subtract_dispersions, \
    max_config_adding_dispersions, ParameterFitter
import matplotlib
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
from utility import config_to_solutions as cts
from utility import flow_utility as fu
from utility.config_interpreter import ConfigInterpreter
import numpy as np
import pandas as pd

dispersion_multiplier = 2
interpolator_fn = cts.interpolate_solutions_to_times
min_density = 1090
max_density = 10001
vary_density_instead_of_dispersion = False
show_plots = True

#these are config_to_solutions
all_oneway_fns = {
    # 'Idealized perfectly absorbing': cts.combined_model(cts.perfect_absorb_idealized_1D_PDE,
    #                                                     cts.perfect_absorb_idealized_well_mixed),
    'Perfectly absorbing': cts.combined_model(cts.perfect_absorb_1D_PDE, cts.perfect_absorb_well_mixed),
    'Linear': cts.combined_model(cts.linear_1D_PDE, cts.linear_well_mixed_ODE),
    'Cell carrying capacity': cts.combined_model(cts.cell_carrying_capacity_1D_PDE,
                                                 cts.cell_carrying_capacity_model_well_mixed_ODE),
    'Surface flux': cts.combined_model(cts.surface_flux_capacity_1D_PDE, cts.surface_flux_capacity_well_mixed_ODE),
    'Surface flux and cell carrying capacity': cts.combined_model(cts.cell_and_surface_capacity_1D_PDE,
                                                                  cts.cell_and_surface_capacity_well_mixed_ODE)
}

all_fns = all_oneway_fns.copy()

dimensionality_tests = {
    'Surface flux 1D': cts.combined_model(cts.surface_flux_capacity_1D_PDE, cts.surface_flux_capacity_well_mixed_ODE),
    'Surface flux 2D': cts.combined_model(cts.surface_flux_capacity_2D_PDE_unscaled,
                                          cts.surface_flux_capacity_well_mixed_ODE)
}
scaling_test = {
    'Suface flux and cell carrying capacity unscaled': cts.combined_model(cts.cell_and_surface_capacity_1D_PDE_unscaled,
                                                                          cts.cell_and_surface_capacity_well_mixed_ODE),
    'Surface flux and cell carrying capacity scaled': cts.combined_model(cts.cell_and_surface_capacity_1D_PDE,
                                                                         cts.cell_and_surface_capacity_well_mixed_ODE)
}
surface_flux_fns = {
    'Surface flux': cts.combined_model(cts.surface_flux_capacity_1D_PDE, cts.surface_flux_capacity_well_mixed_ODE),
    'Surface flux and cell carrying capacity': cts.combined_model(cts.cell_and_surface_capacity_1D_PDE,
                                                                  cts.cell_and_surface_capacity_well_mixed_ODE)
}


def mean_absolute_percentage_error(pred, actual):
    # eliminate zeros, doesn't work properly with MAPE (no defined way to handle)
    nxs = np.array([x for x in pred if x != 0])
    nys = np.array([y for y in actual if y != 0])
    n = len(nys)
    mape = 1 / n * np.sum(abs((nys - nxs) / nys))
    return mape

def get_csv_filename(config_file, model_desc):
    return '{}_{}.csv'.format(splitext(config_file)[0], model_desc)

def get_fig_filename(config_file, model_desc):
    return '{}_{}.png'.format(splitext(config_file)[0], model_desc)

def get_err_filename(config_file, model_desc):
    return '{}_{}.err.txt'.format(splitext(config_file)[0], model_desc)

def plot_and_save_real_and_estimated(model_desc: str, config_to_solutions,
                                     config: ConfigInterpreter,
                                     best_rate: float, min_rate: float, max_rate: float):
    plt.close("all")
    best_model_vals = interpolator_fn(config_to_solutions, config, best_rate)
    min_model_vals = interpolator_fn(config_to_solutions, config, min_rate)
    max_model_vals = interpolator_fn(config_to_solutions, config, max_rate)

    capacity = cts.max_capacity_from_last_derivative_compared_to_max_derivative(config)

    df = pd.DataFrame({
        'Time (mins)': config.times,
        'Particles per cell (model best)': best_model_vals,
        'Particles per cell (model lower)': min_model_vals,
        'Particles per cell (model upper)': max_model_vals,
        'Rate (best)': best_rate,
        'Rate (lower)': min_rate,
        'Rate (upper)': max_rate,
        'Capacity': capacity,
        'Particle name': config.config['particle']['particle_name'],
        'Cell line': config.config['cell']['cell_line_name']
    })

    csv_name = get_csv_filename(config.config_file, model_desc)
    df.to_csv(csv_name)


    plt.errorbar(config.times, config.associated_per_cell_means, yerr=config.all_associated_per_cell_dispersions, fmt='o-', label='True')
    plt.plot(config.times, best_model_vals, 'g-', label='Model (r={:.2e} , cap={:.1f})'.format(best_rate, capacity))
    plt.plot(config.times, min_model_vals, 'g--', label='Model bounds')
    plt.plot(config.times, max_model_vals, 'g--')

    plt.xlabel('Time (min)')
    plt.ylabel('Particles per cell')
    plt.title('{} incubation with {}'.format(config.experimental_and_cell_detail.particle.name,
                                             config.experimental_and_cell_detail.cell_info.cell_line))
    plt.legend(loc='best')
    fig_name = get_fig_filename(config.config_file, model_desc)
    plt.savefig(fig_name)
    if show_plots:
        plt.show(block=False)


def get_config(config_file) -> ConfigInterpreter:
    config = ConfigInterpreter(config_file,
                               fu.get_particles_per_cell,
                               fu.get_standard_error_of_mean_all_samples,
                               fu.remove_outliers_and_control_median_workflow)
    return config


def get_bounded_fitter_dispersion_varied(config: ConfigInterpreter, config_to_solutions, mesh_scaling):
    # todo: add changes from config, other things
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    rate_scale = 1e-9
    minimizer = lambda obj_fn: scipy.optimize.minimize_scalar(obj_fn, method='Brent', tol=1e-4)

    min_config_fn = lambda c: min_config_subtract_dispersions(c, dispersion_multiplier)
    max_config_fn = lambda c: max_config_adding_dispersions(c, dispersion_multiplier)

    fitter = ParameterFitter(solution_interpolator=cts.interpolate_solutions_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=mean_sq,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=min_config_fn,
                             get_max_config_fn=max_config_fn,
                             get_values_fn=lambda c: c.associated_per_cell_means,
                             minimizer_fn=minimizer)

    return fitter


def get_bounded_fitter_dispersion_varied(config: ConfigInterpreter, config_to_solutions, mesh_scaling):
    # todo: add changes from config, other things
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    rate_scale = 1e-9
    minimizer = lambda obj_fn: scipy.optimize.minimize_scalar(obj_fn, method='Brent', tol=1e-4)

    min_config_fn = lambda c: min_config_subtract_dispersions(c, dispersion_multiplier)
    max_config_fn = lambda c: max_config_adding_dispersions(c, dispersion_multiplier)

    fitter = ParameterFitter(solution_interpolator=cts.interpolate_solutions_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=mean_sq,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=min_config_fn,
                             get_max_config_fn=max_config_fn,
                             get_values_fn=lambda c: c.associated_per_cell_means,
                             minimizer_fn=minimizer)

    return fitter





def get_bounded_fitter_density_varied(config: ConfigInterpreter, config_to_solutions, mesh_scaling):
    # todo: add changes from config, other things
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    rate_scale = 1e-9
    minimizer = lambda obj_fn: scipy.optimize.minimize_scalar(obj_fn, method='Brent', tol=1e-4)
    min_config_fn = lambda c: new_config_change_density(c, min_density)
    max_config_fn = lambda c: new_config_change_density(c, max_density)

    fitter = ParameterFitter(solution_interpolator=cts.interpolate_solutions_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=mean_sq,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=min_config_fn,
                             get_max_config_fn=max_config_fn,
                             get_values_fn=lambda c: c.associated_per_cell_means,
                             minimizer_fn=minimizer)
    return fitter


max_uptake_fn = all_oneway_fns['Perfectly absorbing']
percent_close_to_max_uptake = .01


def get_fitter_max_rate(config: ConfigInterpreter, config_to_solutions, mesh_scaling):
    max_rate_mesh_scaling = mesh_refinement.get_high_low_rate_gci_mesh_refinement(config_to_solutions=max_uptake_fn,
                                                                                  config=config,
                                                                                  grid_refinement_factor=1.25)
    pa_vals = cts.interpolate_solutions_to_times(max_uptake_fn, config, 0, max_rate_mesh_scaling)

    # we only care about the values for the "perfectly absorbing" model
    # and we want to be within 1 percent of them (if they were perfect, anything ABOVE the max rate would work)
    #also excluding the first value, which is zero-ish
    close_to_perfectly_absorbing = lambda rate, real, est: abs(percent_close_to_max_uptake -
                                                               mean_absolute_percentage_error(est[1:], pa_vals[1:]) )
    rate_scale = 1e-9
    minimizer = lambda obj_fn: scipy.optimize.minimize_scalar(obj_fn, method='Brent', tol=1e-4)

    fitter = ParameterFitter(solution_interpolator=cts.interpolate_solutions_to_times,
                             rate_scale=rate_scale,
                             mesh_scaling=mesh_scaling,
                             objective_fn=close_to_perfectly_absorbing,
                             config_to_solutions=config_to_solutions,
                             get_min_config_fn=None,
                             get_max_config_fn=None,
                             get_values_fn=lambda c: c.associated_per_cell_means,
                             minimizer_fn=minimizer)
    return fitter


def setup_logging():
    logging.basicConfig(level=logging.ERROR)
    fitter.model_comparer.logger.setLevel(level=logging.INFO)
    fitter.mesh_refinement.logger.setLevel(level=logging.INFO)
    logging.getLogger('FFC').setLevel(logging.ERROR)
    logging.getLogger('UFL').setLevel(logging.ERROR)

    # dolfin.cpp.common.set_log_level(dolfin.cpp.common.ERROR)