from math import pi, ceil
from typing import Callable, List, Tuple

from dosimetry.boundary import LinearFluxCellBoundaryConditionMaker, SurfaceFluxTestingBoundaryConditionMaker, \
    SurfaceCapacityFluxBoundaryConditionMaker
from dosimetry.experimental_detail import ExperimentalDetail, CellOrientation, ExperimentalAndCellDetail, \
    CellCultureCondition
from dosimetry.initial_condition import UniformInitialConditionMaker
from dosimetry.model_maker import ConstantAdvectionDiffusionNoFlux1DModelMaker, \
    ConstantAdvectionDiffusionNoFluxModelMaker, standard_constant_advection_diffusion_greedy_model_builder
from dosimetry.model_maker import standard_constant_advection_diffusion_greedy_model_builder, ModelBuilder
from dosimetry.solver.standard import TimestepLinearModelSolver, AbstractModelSolver, TimestepNonLinearModelSolver, \
    Solution
from dosimetry.solver.three_compartment import LinearInternalizationRate, association_internalization_solver, \
    AssociationInternalizationSolution
from dosimetry.solver.updating_boundary import updating_linear_boundary_solver, SafeRateCell, SafeRate1D, \
    CellCarryingCapacityAssociationRate, CellAndSurfaceCapacityFluxAssociationRate, updating_nonlinear_boundary_solver
from dosimetry.solver.utility import progress_bar, graph
from dosimetry.solver.utility import solution_saver, solution_report
from scipy.interpolate import interp1d

from dosimetry.time_and_mesh import NonLinearCGSpaceAndFunctionMaker, FractionalTimestepMaker, \
    NonLinearSomethingSpaceAndFunctionMaker, FractionalMeshMaker1D
from utility.config_interpreter import ConfigInterpreter
import dosimetry.ode_model as ode

carrying_capacity_heuristic_deriviatve_max_cutoff = 0.01
carrying_capacity_heuristic_proportion_of_max_derivative_cutoff = 0.2
visualize = False
max_error = 999999


def get_adherent_rate_correction(culture_condition: CellCultureCondition, cell_info):
    total_cell_surface = cell_info.surface_area_per_cell * cell_info.total_cells
    rate_correction = total_cell_surface / culture_condition.cell_side_surface_area
    return rate_correction


def get_surface_capacity(config: ConfigInterpreter):
    exp_detail = config.experimental_and_cell_detail
    surface_capacity_scaling = float(config.config['model'].get('surface_capacity_scaling', 1.0))

    # assumes spherical particles and cylindrical cell cultures
    r = exp_detail.particle.radius
    particle_cross_sec_area = pi * r * r

    total_cell_surface = exp_detail.cell_info.surface_area_per_cell * exp_detail.cell_info.total_cells

    surface_capacity = (total_cell_surface / particle_cross_sec_area) * surface_capacity_scaling
    return surface_capacity


def get_max_cell_capacity(config: ConfigInterpreter) -> float:
    if config.config['model']['true_capacity'].strip() == 'from_max_and_derivative_heruistic':
        return max_capacity_from_last_derivative_compared_to_max_derivative(config)
    else:
        return float(config.config['model']['true_capacity'])


def max_capacity_from_max_and_derivative_heruistic_old(config):
    max_capacity = ceil(max(config.val_means))
    times_with_amts = zip(config.times, config.val_means)
    times_with_amts = sorted(times_with_amts, key=lambda a: a[1])

    max_pt = times_with_amts[-1]
    second_max_pt = times_with_amts[-2]
    deriv = (max_pt[1] - second_max_pt[1]) / (max_pt[0] - second_max_pt[0])
    if deriv < carrying_capacity_heuristic_deriviatve_max_cutoff:
        return max(max_capacity, 1)
    else:
        return max(max_capacity * 10, 1)


def max_capacity_from_last_derivative_compared_to_max_derivative(config):
    max_capacity = ceil(max(config.associated_per_cell_means))

    times_with_amts = list(zip(config.times, config.associated_per_cell_means))

    def deriv_fn(pt1, pt2):
        return (pt2[1] - pt1[1]) / (pt2[0] - pt1[0])

    derivs = []

    for i in range(1, len(times_with_amts)):
        deriv = deriv_fn(times_with_amts[i - 1], times_with_amts[i])
        derivs.append(deriv)

    last_deriv = derivs[-1]

    max_deriv = max(derivs)
    deriv_cutoff = carrying_capacity_heuristic_proportion_of_max_derivative_cutoff * max_deriv

    if last_deriv < deriv_cutoff:
        return max(max_capacity, 1)
    else:
        return max(max_capacity * 10, 1)


def get_ODE_scaling_factor(config: ConfigInterpreter):
    condition = config.config['model'].get('condition', 'static_adherent')
    culture = config.experimental_and_cell_detail.cell_culture_condition
    is_adherent = (condition == 'well_mixed_adherent' or condition == 'constant_supply_adherent')
    scaling = 1
    if (is_adherent):
        scaling *= get_adherent_rate_correction(culture, config.experimental_and_cell_detail.cell_info)
        if culture.cell_orientation == CellOrientation.side:
            scaling_factor = 1 / culture.width
        else:
            scaling_factor = 1 / culture.height  # particles on bottom or top
        scaling *= scaling_factor
    else:
        cell_info = config.experimental_and_cell_detail.cell_info
        scaling_factor = cell_info.total_cells * cell_info.surface_area_per_cell / culture.volume
        scaling *= scaling_factor
    return scaling


def ode_solutions_from_rate_function(config: ConfigInterpreter, rate_fn):

    times = [t * 60 for t in config.times]
    culture = config.experimental_and_cell_detail.cell_culture_condition
    initial_amount = culture.particle_concentration * culture.width * culture.height
    scaling = get_ODE_scaling_factor(config)

    # AMOUNT is amount IN SOLUTION
    derivatives_fn = lambda amt, time: rate_fn(scaling, amt)
    ode_model = ode.OdeModel(initial_amount=initial_amount, derivatives_fn=derivatives_fn)
    return ode.get_ode_model_solutions(ode_model, times)

def combined_model(static_model, well_mixed_model):
    def get_combined_model(config: ConfigInterpreter, rate: float, mesh_scaling=0.8):
        condition = config.config['model'].get('condition', 'static_adherent')
        if condition == 'static_adherent':
            return static_model(config, rate, mesh_scaling)
        else:
            return well_mixed_model(config, rate)

    return get_combined_model


##CELL CARRYING CAPACITY

def cell_carrying_capacity_1D_PDE(config: ConfigInterpreter, rate: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(
        exp_detail,
        ConstantAdvectionDiffusionNoFlux1DModelMaker,
        mesh_scaling)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)

    model_solver_maker = TimestepLinearModelSolver
    model_solver_maker = updating_linear_boundary_solver(model_solver_maker)
    model_solver_maker.equation_maker = model_builder.equation_maker

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate

    estimate_max_capacity = max_capacity_from_last_derivative_compared_to_max_derivative(config)

    boundary_conditioner = CellCarryingCapacityAssociationRate(corrected_rate, estimate_max_capacity)
    model_builder.boundary_condition_maker = boundary_conditioner

    model_solver_maker.bc_updater = SafeRate1D(boundary_conditioner)
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def cell_carrying_capacity_1D_PDE_carrying_cap_specified(config: ConfigInterpreter, rate: float,
                                                         carrying_cap: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(
        exp_detail,
        ConstantAdvectionDiffusionNoFlux1DModelMaker,
        mesh_scaling)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)

    model_solver_maker = TimestepLinearModelSolver
    model_solver_maker = updating_linear_boundary_solver(model_solver_maker)
    model_solver_maker.equation_maker = model_builder.equation_maker

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate

    boundary_conditioner = CellCarryingCapacityAssociationRate(corrected_rate, carrying_cap)
    model_builder.boundary_condition_maker = boundary_conditioner

    model_solver_maker.bc_updater = SafeRate1D(boundary_conditioner)
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions

def cell_carrying_capacity_2D_PDE(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    experimental_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(experimental_detail,
                                                                               ConstantAdvectionDiffusionNoFluxModelMaker)
    model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(0)

    pathway = LinearInternalizationRate(0)
    model_solver_maker = TimestepLinearModelSolver

    model_solver_maker = association_internalization_solver(model_solver_maker)
    model_solver_maker.pathway = pathway
    model_solver_maker.associated_start = 0

    model_solver_maker = updating_linear_boundary_solver(model_solver_maker)
    model_solver_maker.equation_maker = model_builder.equation_maker
    corrected_rate = get_adherent_rate_correction(experimental_detail.cell_culture_condition,
                                                  experimental_detail.cell_info) * rate

    estimate_max_capacity = max_capacity_from_last_derivative_compared_to_max_derivative(config)
    model_solver_maker.bc_updater = SafeRateCell(
        CellCarryingCapacityAssociationRate(rate=corrected_rate, capacity=estimate_max_capacity))
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def cell_carrying_capacity_model_well_mixed_ODE(config: ConfigInterpreter, rate: float):
    culture = config.experimental_and_cell_detail.cell_culture_condition
    initial_amount = culture.particle_concentration * culture.width * culture.height
    estimate_max_capacity = max_capacity_from_last_derivative_compared_to_max_derivative(config)

    def carrying_capacity_rate(scaling, current_amt):
        amount_removed = initial_amount - current_amt
        current_rate = -1 * scaling * rate * (estimate_max_capacity - amount_removed) / estimate_max_capacity
        return current_rate * current_amt

    return ode_solutions_from_rate_function(config, carrying_capacity_rate)


##LINEAR

def linear_1D_PDE(config: ConfigInterpreter, rate: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker,
                                                                               mesh_scaling)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(corrected_rate)
    model_solver_maker = TimestepLinearModelSolver

    model_solver_maker.equation_maker = model_builder.equation_maker
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def linear_well_mixed_ODE(config: ConfigInterpreter, rate: float):
    def linear_rate(scaling, current_amt):
        return -1 * scaling * rate * current_amt

    return ode_solutions_from_rate_function(config, linear_rate)


def linear_1D_PDE_timestep_mod(config: ConfigInterpreter, rate: float, timestep_fraction: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(corrected_rate)
    model_solver_maker = TimestepLinearModelSolver

    model_builder.time_maker = FractionalTimestepMaker(timestep_fraction, model_builder.time_maker)

    model_solver_maker.equation_maker = model_builder.equation_maker
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def linear_1D_PDE_time_and_meshstep_mod(config: ConfigInterpreter, rate: float, time_and_mesh_fraction: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker,
                                                                               mesh_scaling=time_and_mesh_fraction)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(corrected_rate)
    model_solver_maker = TimestepLinearModelSolver

    model_solver_maker.equation_maker = model_builder.equation_maker
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


## SURFACE FLUX CAPACITY

def surface_flux_capacity_1D_PDE(config: ConfigInterpreter, rate: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker,
                                                                               mesh_scaling)

    concentration_scaling_factor = 100.0 / exp_detail.cell_culture_condition.particle_concentration

    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration *
        concentration_scaling_factor *
        exp_detail.cell_culture_condition.width)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    surface_capacity = get_surface_capacity(config) * concentration_scaling_factor

    model_builder.space_and_function_maker = NonLinearSomethingSpaceAndFunctionMaker()
    model_builder.boundary_condition_maker = SurfaceCapacityFluxBoundaryConditionMaker(rate=corrected_rate,
                                                                                       surface_capacity=surface_capacity)

    model_solver_maker = TimestepNonLinearModelSolver

    model_solver_maker.equation_maker = model_builder.equation_maker
    solutions = _get_solutions(config, model_solver_maker, model_builder,
                               concentration_scaling_factor=concentration_scaling_factor)
    return solutions


def surface_flux_capacity_1D_PDE_unscaled(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration *
        exp_detail.cell_culture_condition.width)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    surface_capacity = get_surface_capacity(config)

    model_builder.space_and_function_maker = NonLinearSomethingSpaceAndFunctionMaker()
    model_builder.boundary_condition_maker = SurfaceCapacityFluxBoundaryConditionMaker(rate=corrected_rate,
                                                                                       surface_capacity=surface_capacity)
    # model_builder.time_maker = FractionalTimestepMaker(0.1, model_builder.time_maker)

    model_solver_maker = TimestepNonLinearModelSolver

    model_solver_maker.equation_maker = model_builder.equation_maker
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def surface_flux_capacity_2D_PDE_unscaled(config: ConfigInterpreter, rate: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFluxModelMaker,
                                                                               mesh_scaling)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    surface_capacity = get_surface_capacity(config)

    model_builder.space_and_function_maker = NonLinearCGSpaceAndFunctionMaker()
    model_builder.boundary_condition_maker = SurfaceCapacityFluxBoundaryConditionMaker(rate=corrected_rate,
                                                                                       surface_capacity=surface_capacity)
    model_solver_maker = TimestepNonLinearModelSolver

    model_solver_maker.equation_maker = model_builder.equation_maker
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def surface_flux_capacity_well_mixed_ODE(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    sc = get_surface_capacity(config)

    def surface_flux_capacity_rate_function(scaling, current_amt):
        current_flux = -1 * scaling * rate * sc * current_amt / (sc + current_amt)
        return current_flux

    return ode_solutions_from_rate_function(config, surface_flux_capacity_rate_function)


## CELL AND SURFACE CAPACITY

def cell_and_surface_capacity_1D_PDE(config: ConfigInterpreter, rate: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker,
                                                                               mesh_scaling=mesh_scaling)

    concentration_scaling_factor = 100.0 / exp_detail.cell_culture_condition.particle_concentration

    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration *
        exp_detail.cell_culture_condition.width *
        concentration_scaling_factor)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    surface_capacity = get_surface_capacity(config) * concentration_scaling_factor
    cell_capacity = get_max_cell_capacity(config) * concentration_scaling_factor

    model_solver_maker = TimestepNonLinearModelSolver
    model_solver_maker = updating_nonlinear_boundary_solver(model_solver_maker)
    model_builder.space_and_function_maker = NonLinearCGSpaceAndFunctionMaker()

    model_solver_maker.equation_maker = model_builder.equation_maker
    boundary_conditioner = CellAndSurfaceCapacityFluxAssociationRate(rate=corrected_rate,
                                                                     surface_capacity=surface_capacity,
                                                                     cell_capacity=cell_capacity)
    model_builder.boundary_condition_maker = boundary_conditioner
    model_solver_maker.bc_updater = boundary_conditioner
    solutions = _get_solutions(config, model_solver_maker, model_builder,
                               concentration_scaling_factor=concentration_scaling_factor)
    return solutions


def cell_and_surface_capacity_1D_PDE_unscaled(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration *
        exp_detail.cell_culture_condition.width)
    model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(0.0)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    surface_capacity = get_surface_capacity(config)
    cell_capacity = get_max_cell_capacity(config)

    model_solver_maker = TimestepNonLinearModelSolver
    model_solver_maker = updating_nonlinear_boundary_solver(model_solver_maker)
    model_builder.space_and_function_maker = NonLinearCGSpaceAndFunctionMaker()
    model_solver_maker.equation_maker = model_builder.equation_maker
    # model_builder.boundary_condition_maker = CellAndSurfaceCapacityFluxAssociationRate(rate=corrected_rate,
    #                                                                                     surface_capacity=surface_capacity,
    #                                                                                     cell_capacity=cell_capacity)
    model_solver_maker.bc_updater = CellAndSurfaceCapacityFluxAssociationRate(rate=corrected_rate,
                                                                              surface_capacity=surface_capacity,
                                                                              cell_capacity=cell_capacity)
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def cell_and_surface_capacity_2D_PDE(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFluxModelMaker)
    model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(0.0)

    corrected_rate = get_adherent_rate_correction(exp_detail.cell_culture_condition,
                                                  exp_detail.cell_info) * rate
    surface_capacity = get_surface_capacity(config)
    cell_capacity = get_max_cell_capacity(config)

    model_builder.space_and_function_maker = NonLinearCGSpaceAndFunctionMaker()
    model_solver_maker = TimestepNonLinearModelSolver
    model_solver_maker.equation_maker = model_builder.equation_maker
    model_solver_maker = updating_nonlinear_boundary_solver(model_solver_maker)
    model_solver_maker.bc_updater = CellAndSurfaceCapacityFluxAssociationRate(rate=corrected_rate,
                                                                              surface_capacity=surface_capacity,
                                                                              cell_capacity=cell_capacity)
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


def cell_and_surface_capacity_well_mixed_ODE(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    culture = config.experimental_and_cell_detail.cell_culture_condition
    initial_amount = culture.particle_concentration * culture.width * culture.height
    estimate_max_capacity = max_capacity_from_last_derivative_compared_to_max_derivative(config)
    sc = get_surface_capacity(config)

    def cell_and_surface_capacity_function(scaling, current_amt):
        amount_removed = initial_amount - current_amt
        # current_flux = -1 * scaling * rate * sc * current_amt/(sc/2.0 + current_amt) * \
        #                (estimate_max_capacity - amount_removed) / estimate_max_capacity
        current_flux = -1 * scaling * rate * sc * current_amt / (sc + current_amt) * \
                       (estimate_max_capacity - amount_removed) / estimate_max_capacity
        return current_flux

    return ode_solutions_from_rate_function(config, cell_and_surface_capacity_function)


##PERFECTLY ABSORBING (note that rate isn't used)
def perfect_absorb_1D_PDE(config: ConfigInterpreter, rate: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(
        exp_detail, ConstantAdvectionDiffusionNoFlux1DModelMaker, mesh_scaling)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(
        exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)
    # model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(0)

    pathway = LinearInternalizationRate(0)
    model_solver_maker = TimestepLinearModelSolver

    model_solver_maker = association_internalization_solver(model_solver_maker)
    model_solver_maker.pathway = pathway
    model_solver_maker.associated_start = 0

    model_solver_maker.equation_maker = model_builder.equation_maker
    solutions = _get_solutions(config, model_solver_maker, model_builder)
    return solutions


# instantly absorbs everything, no need for ODE
def perfect_absorb_well_mixed(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    culture = config.experimental_and_cell_detail.cell_culture_condition

    initial_amount = culture.particle_concentration * culture.width * culture.height
    completely_gone_solution = AssociationInternalizationSolution(initial_amount, initial_amount, 0.0)

    solutions = [(0, completely_gone_solution),
                 (60 * config.times[-1] + 1, completely_gone_solution)]

    return solutions


##PERFECTLY ABSORBING IDEALIZED (CANT DO ODE)
def perfect_absorb_idealized_1D_PDE(config: ConfigInterpreter, rate: float, mesh_scaling=0.8) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    # NOTE THAT THIS METHOD "CHEATS" AND RESCALES TO THE ACTUAL VALUES
    # it is intended as a perfect, idealized version of a "dosimetry only" control model
    solutions = perfect_absorb_1D_PDE(config, rate, mesh_scaling)
    rescale = max(config.associated_per_cell_means) / max([s[1].absolute_amount_associated for s in solutions])
    new_solutions = [
        (s[0], AssociationInternalizationSolution(s[1].starting_amount, s[1].absolute_amount_associated * rescale,
                                                  s[1].absolute_amount_internalized)) for s in solutions]
    return new_solutions


# instantly absorbs to min of (capacity, available concentration), no need for ODE
def perfect_absorb_idealized_well_mixed(config: ConfigInterpreter, rate: float) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    estimated_max_capacity = max_capacity_from_last_derivative_compared_to_max_derivative(config)

    culture = config.experimental_and_cell_detail.cell_culture_condition
    initial_amount = culture.particle_concentration * culture.width * culture.height

    total_absorbed = min(estimated_max_capacity, initial_amount)
    completely_gone_solution = AssociationInternalizationSolution(initial_amount, total_absorbed, 0.0)

    solutions = [(0, completely_gone_solution),
                 (60 * config.times[-1] + 1, completely_gone_solution)]

    return solutions


##UTILITY FUNCTIONS

def interpolate_solutions_to_times(config_to_solutions,
                                   config: ConfigInterpreter,
                                   rate: float,
                                   mesh_scaling=0.8) -> [float]:
    solutions = config_to_solutions(config, rate, mesh_scaling)
    solved_times = [s[0] / 60 for s in solutions]  # remember, solutions are in seconds, and configs are in minutes
    solved_vals = [s[1].absolute_amount_removed for s in solutions]
    interpolator = interp1d(solved_times, solved_vals)
    interpolated_vals = interpolator(config.times)
    return interpolated_vals


def _get_solutions(config: ConfigInterpreter, model_solver_maker, model_builder, concentration_scaling_factor=1.0):
    model_solver_maker = solution_saver(progress_bar(model_solver_maker))

    if visualize:
        model_solver_maker = graph(model_solver_maker)
        model_solver_maker.max_range = config.experimental_and_cell_detail.cell_culture_condition.particle_concentration * concentration_scaling_factor / 10.0

    model_solver = model_solver_maker()
    model = model_builder.build_model()
    model_solver.solve_model(model)
    for t, s in model_solver.solutions:
        s.absolute_amount_removed /= concentration_scaling_factor
        s.absolute_amount_remaining /= concentration_scaling_factor
        if type(s) is AssociationInternalizationSolution:
            s.starting_amount /= concentration_scaling_factor
            s.absolute_amount_internalized /= concentration_scaling_factor
            s.absolute_amount_associated /= concentration_scaling_factor
    return model_solver.solutions


def get_estimated_max_rate(config: ConfigInterpreter):
    model_condition = config.config['model'].get('condition', 'static_adherent')
    if model_condition in ['well_mixed_adherent', 'well_mixed_suspension']:
        return max_error
    exp_detail = config.experimental_and_cell_detail
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail,
                                                                               ConstantAdvectionDiffusionNoFlux1DModelMaker)
    max_rate = -1.0 * float(model_builder.equation_maker.advection_vector[1] / model_builder.equation_maker.width)
    max_rate /= get_adherent_rate_correction(exp_detail.cell_culture_condition, exp_detail.cell_info)
    max_rate /= 0.5  # Halfway point of particle association
    max_rate *= 10

    return max_rate
