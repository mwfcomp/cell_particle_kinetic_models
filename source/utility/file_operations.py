import os
import os.path
from typing import List

config_col = 'config'


def get_config_file_list(ini_or_lst_file: str, base_folder=None) -> List[str]:
    base, end = os.path.split(ini_or_lst_file)
    ext = os.path.splitext(end)[1]
    if ext.lower() == ".ini":
        if base_folder is not None:
            full_file_name = os.path.join(base_folder, ini_or_lst_file)
        else:
            full_file_name = ini_or_lst_file
        return [full_file_name]
    else:
        with open(ini_or_lst_file) as files:
            raw_filenames = [file.strip() for file in files.readlines()]
            filenames = [_process_filename(base, filename) for filename in raw_filenames]
        return filenames


def _process_filename(base, filename: str):
    if os.path.isabs(filename):
        return filename

    return os.path.join(base, filename)


def remove_config_path(df):
    df[config_col] = df[config_col].apply(lambda c: os.path.split(c)[1])
    return df