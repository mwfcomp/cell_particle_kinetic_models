# from flow_ctyometry.fcs_display import *
from flow_ctyometry.experiment import FullIncubationExperiment
import pandas as pd
from pandas import DataFrame
from flow_ctyometry.transforms import FilterOutliers, FilterNormal, ArcSinHTransform, UndoArcSinHTransform, \
    RemoveControlMedian
from flow_ctyometry.metrics import ControlTransformWorkflow, ControlType as ct, PercentCellAssocCombineControl, \
    MedianSignalPerCell, ParticlesPerCell, MedianAbsoluteDeviationOfParticlesPerCell, StandardErrorOfMeanOfParticlesPerCell, StandardDeviationOfParticlesPerCell
from cachetools import cached, LRUCache
import hashlib


def combine_controls(controls: [DataFrame]):
    control = None
    if len(controls) == 1:
        control = controls[0]
    elif len(controls) > 1:
        control = pd.concat(controls)
    return control


def metric_hash(exp: FullIncubationExperiment):
    h = hashlib.md5()
    for channel in exp.channel_info.all_channels():
        h.update(bytearray(channel, 'utf-8'))
    for ctrl in exp.blank_controls:
        h.update(ctrl.values.tobytes())
    for ctrl in exp.single_signal_controls:
        h.update(ctrl.values.tobytes())
    return h.digest()

def remove_outliers_and_control_median_workflow(channel_info):
    workflow = ControlTransformWorkflow([(ArcSinHTransform(channel_info.all_channels()), ct.no_control),
                                         (FilterOutliers(channel_info.two_normal_channels), ct.blank_control),
                                         (UndoArcSinHTransform(channel_info.all_channels()), ct.no_control),
                                         (RemoveControlMedian(channel_info.detect_channel), ct.blank_control)])
    return workflow

def remove_control_median_workflow(channel_info):
    workflow = ControlTransformWorkflow([(ArcSinHTransform(channel_info.all_channels()), ct.no_control),
                                         (UndoArcSinHTransform(channel_info.all_channels()), ct.no_control),
                                         (RemoveControlMedian(channel_info.detect_channel), ct.blank_control)])
    return workflow

def no_preprocessing_workflow(channel_info):
    return ControlTransformWorkflow([])


flow_filter_workflow = remove_outliers_and_control_median_workflow

_get_percent_association_cache = LRUCache(maxsize=100)
@cached(cache=_get_percent_association_cache, key=metric_hash)
def get_percent_association(exp: FullIncubationExperiment):
    channel_info = exp.channel_info
    blank_controls = exp.blank_controls
    signal_controls = exp.single_signal_controls

    pre_count = ControlTransformWorkflow([(ArcSinHTransform(channel_info.all_channels()), ct.no_control),
                                          (FilterOutliers(channel_info.two_normal_channels), ct.blank_control)])
    post_count = ControlTransformWorkflow([(FilterNormal(normal_channel=channel_info.normal_channel,
                                                         detect_channel=channel_info.detect_channel,
                                                         scale_right=0.05,
                                                         scale_updown=1), ct.blank_control),
                                           (UndoArcSinHTransform(channel_info.all_channels()), ct.no_control)])
    blank_control, signal_control = combine_controls(blank_controls), combine_controls(signal_controls)
    blank_control, signal_control = pre_count.setup_from_controls(blank_control, signal_control)
    post_count.setup_from_controls(blank_control, signal_control)
    return PercentCellAssocCombineControl(pre_count, post_count)


_get_mean_signal_per_cell_cache = LRUCache(maxsize=100)
@cached(cache=_get_mean_signal_per_cell_cache, key=metric_hash)
def get_mean_signal_per_cell(exp: FullIncubationExperiment):
    channel_info = exp.channel_info
    blank_controls = exp.blank_controls
    signal_controls = exp.single_signal_controls
    workflow = flow_filter_workflow(channel_info)
    blank_control, signal_control = combine_controls(blank_controls), combine_controls(signal_controls)
    workflow.setup_from_controls(blank_control, signal_control)
    return MedianSignalPerCell(workflow)


_mad_cache = LRUCache(maxsize=100)
@cached(cache=_mad_cache, key=metric_hash)
def get_median_absolute_deviation_of_signal(exp: FullIncubationExperiment):
    ppc = get_particles_per_cell(exp)
    return MedianAbsoluteDeviationOfParticlesPerCell(ppc)


_sem_all_samples_cache = LRUCache(maxsize=100)
@cached(cache=_mad_cache, key=metric_hash)
def get_standard_error_of_mean_all_samples(exp: FullIncubationExperiment):
    ppc = get_particles_per_cell(exp)
    return StandardErrorOfMeanOfParticlesPerCell(ppc)

_semed_all_samples_cache = LRUCache(maxsize=100)
@cached(cache=_mad_cache, key=metric_hash)
def get_standard_error_of_median_all_samples(exp: FullIncubationExperiment):
    ppc = get_particles_per_cell(exp)
    return StandardErrorOfMeanOfParticlesPerCell(ppc)


_std_dev_all_samples_cache = LRUCache(maxsize=100)
@cached(cache=_mad_cache, key=metric_hash)
def get_standard_deviation_all_samples(exp: FullIncubationExperiment):
    ppc = get_particles_per_cell(exp)
    return StandardDeviationOfParticlesPerCell(ppc)


_get_particles_per_cell_cache = LRUCache(maxsize=100)
@cached(cache=_get_particles_per_cell_cache, key=metric_hash)
def get_particles_per_cell(exp: FullIncubationExperiment):
    channel_info = exp.channel_info
    blank_controls = exp.blank_controls
    signal_controls = exp.single_signal_controls
    workflow = flow_filter_workflow(channel_info)
    blank_control, signal_control = combine_controls(blank_controls), combine_controls(signal_controls)
    workflow.setup_from_controls(blank_control, signal_control)
    signal_workflow = ControlTransformWorkflow([])
    return ParticlesPerCell(workflow, signal_workflow)


_get_no_filters_mean_signal_per_cell_cache = LRUCache(maxsize=100)
@cached(cache=_get_no_filters_mean_signal_per_cell_cache, key=metric_hash)
def get_no_filters_mean_signal_per_cell(exp: FullIncubationExperiment):
    metric = MedianSignalPerCell(ControlTransformWorkflow([]))
    metric.name += '(No filter)'
    return metric


_no_filters_get_particles_per_cell = LRUCache(maxsize=100)
@cached(cache=_no_filters_get_particles_per_cell, key=metric_hash)
def get_no_filters_particles_per_cell(exp: FullIncubationExperiment):
    metric = ParticlesPerCell(ControlTransformWorkflow([]), ControlTransformWorkflow([]))
    metric.name += '(No filter)'
    return metric
