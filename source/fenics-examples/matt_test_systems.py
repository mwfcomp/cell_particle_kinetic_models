from dolfin import *
# import matplotlib.pyplot as plt
import time


height = .0053
width = .0220
mesh = RectangleMesh(Point(0.0, 0.0), Point(width, height), 100, 100)
float_tol = 1e-14
#mesh = UnitSquareMesh(200, 200)

V = FunctionSpace(mesh, 'CG', 1)
u = Function(V)
v = TestFunction(V)
# plot(mesh)
# vel = Expression(('1.0'), element=V.ufl_element())

scaling_factor = 100


############boundary conditions

#dirichlet are easy
u_boundary = Constant(0)
# u_boundary = Expression('0')
def on_boundary_fn(x, on_boundary):
    return on_boundary and x[1] == 0
dirichlet_bc = DirichletBC(V, u_boundary, on_boundary_fn)

#robin are harder
boundary_parts = \
    MeshFunction("size_t", mesh, mesh.topology().dim()-1)

class BottomBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[1]-0.0) < float_tol

#
# class TopBoundary(SubDomain):
#     def inside(self, x, on_boundary):
#         #note: you should be subtracting the max y value of the mesh
#         return on_boundary and abs(x[1]-height) < float_tol
#
gamma_b = BottomBoundary()
gamma_b.mark(boundary_parts, 0)
# gamma_t = TopBoundary()
# gamma_t.mark(boundary_parts, 1)

############boundary conditions end



vel = as_vector((20e-8, -2.1914e-8))#as_vector((0.0, -1.2946e-9))
vel = as_vector((0.0, -10e-8))#as_vector((0.0, -1.2946e-9))
# vel = as_vector((1e-8, 0.0))#as_vector((0.0, -1.2946e-9))
u0_conc = 1.0 * scaling_factor
u0_expr = Expression('pow(x[0]-{0}, 2)+pow(x[1]-{1}, 2) < {2} ? {3} : 0'.format(width/2, height/2,
                                                                                width*height/40, u0_conc), degree=1)
# u0_expr = Constant(u0_conc)
u_1 = interpolate(u0_expr, V)

T = 24*60*60
dt = T/300
# T /= 4

start_amt = assemble(u_1*dx) / scaling_factor

D = Constant(1.7994e-12) #2.249e-12 #Constant(0.3)
# D = Constant(0.3)
diff_term = D*inner(nabla_grad(u), nabla_grad(v))
sed_term = v*(dot(vel, nabla_grad(u)))

unitNormal = FacetNormal(mesh)

# ds = Measure("ds", domain=mesh, subdomain_data=boundary_parts)
r = 1e-9
sc = 2 * scaling_factor
cell_cap = .2e-6 *scaling_factor
# cell_cap = 0.0
a_diff = (u*v + dt*diff_term)*dx
a_diff_sed = (u*v+dt*(diff_term + sed_term))*dx - dt*dot(vel, unitNormal)*u*v*ds
a_diff_sed_flux = a_diff_sed + dt*Constant(r)*u*v*ds
a_diff_sed_surf_cap_flux = a_diff_sed + dt*Constant(r * sc) * u / (sc/2.0 + u) * v * ds
a_diff_sed_cell_cap_flux = a_diff_sed + dt*Constant(max(r * (cell_cap - (start_amt - assemble(u_1*dx))) / cell_cap, 0.0)) * u * v * ds # NEEDS TO BE DONE IN LOOP!
a_diff_sed_surf_and_cell_cap_flux = a_diff_sed + dt*Constant(r * sc) * u / (sc/2.0 + u) * Constant(max( (cell_cap - (start_amt *scaling_factor - assemble(u_1*dx))) / cell_cap, 0.0)) * v * ds # NEEDS TO BE DONE IN LOOP!

a = a_diff_sed_surf_and_cell_cap_flux
L = v*u_1*dx
F = a - L


# Compute solution
# u = Function(V)   # the unknown at a new time level
t = dt

# A = assemble(a)   # assemble only once, before the time stepping
# b = None          # necessary for memory saving assemble call


#plot(u_1)
#time.sleep(10)
max_range = u0_conc
plot(u,  range_min=0., range_max=max_range, title='Time: ' + str(t), elevate=0.0)

total_fluxed=0
ts = []
amts = []
while t <= T:

    print('time =' + str(t))
    # b = assemble(L, tensor=b)
    # dirichlet_bc.apply(A, b)
    # solve(A, u.vector(), b)
    # carrying_cap_rate = Constant(r * (cell_cap - (start_amt - assemble(u* dx))) / cell_cap)
    #

    a = a_diff_sed + dt*Constant(r * sc) * u / (sc/2.0 + u) * Constant(max( (cell_cap - (start_amt *scaling_factor - assemble(u_1*dx))) / cell_cap, 0.0)) * v * ds # NEEDS TO BE DONE IN LOOP!
    # a = a_diff_sed + dt*Constant(max(r * (cell_cap - (start_amt - assemble(u_1*dx))) / cell_cap, 0.0)) * u * v * ds # NEEDS TO BE DONE IN LOOP!
    L = v*u_1*dx
    F = a - L
    # solve(F == 0, u, [])
    solve(F == 0, u, [])

    t += dt
    plot(u,  range_min=0., range_max=max_range, title='Time: ' + str(t))
    print('amount left: {0}'.format(assemble(u_1*dx) / scaling_factor))
    # print('Capacity: {}'.format((cell_cap - (start_amt - assemble(u_1*dx))) / cell_cap))
    ts.append(t)
    amts.append((assemble(u_1*dx) - assemble(u*dx)) / scaling_factor)
    u_1.assign(u)
    print('=============')
    # interactive()

# plt.plot(ts, amts)

end_amt = assemble(u_1*dx) / scaling_factor
relative_amount_removed = 1 - end_amt/start_amt
print('relative amount removed: {0}'.format(relative_amount_removed))
print('total amount removed: {0}'.format(start_amt - end_amt))
# print('total fluxed: {0}'.format(total_fluxed))
# plt.show(block=False)
