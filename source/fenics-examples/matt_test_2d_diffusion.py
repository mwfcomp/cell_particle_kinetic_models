from dolfin import *
from enum import Enum
import time
height = .0053
width = .0053
mesh = RectangleMesh(Point(0.0, 0.0), Point(width, height), 100, 100)
float_tol = 1e-14
#mesh = UnitSquareMesh(200, 200)

V = FunctionSpace(mesh, 'CG', 1)
u = TrialFunction(V)
v = TestFunction(V)
# plot(mesh)
# vel = Expression(('1.0'), element=V.ufl_element())

############boundary conditions

#dirichlet are easy
u_boundary = Constant(0)
# u_boundary = Expression('0')
def on_boundary_fn(x, on_boundary):
    return on_boundary and abs(x[1]-0.0) < DOLFIN_EPS

def on_boundary_fn_all(x, on_boundary):
    return on_boundary and (
        abs(x[1]-height) < DOLFIN_EPS or
        abs(x[1]-0.0) < DOLFIN_EPS or
        abs(x[0]-width) < DOLFIN_EPS or
        abs(x[0]-0.0) < DOLFIN_EPS)

dirichlet_bc = DirichletBC(V, u_boundary, on_boundary_fn)

#robin are harder
boundary_parts = \
    MeshFunction("size_t", mesh, mesh.topology().dim()-1)


class BottomBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[1]-0.0) < DOLFIN_EPS


class NonBottomBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not abs(x[1]-0.0) < DOLFIN_EPS

#
# class TopBoundary(SubDomain):
#     def inside(self, x, on_boundary):
#         #note: you should be subtracting the max y value of the mesh
#         return on_boundary and abs(x[1]-height) < float_tol
#
gamma_b = BottomBoundary()
gamma_b.mark(boundary_parts, 0)
gamma_t = NonBottomBoundary()
gamma_t.mark(boundary_parts, 1)

############boundary conditions end

vel = as_vector((20e-8, -2.1914e-8))#as_vector((0.0, -1.2946e-9))
vel = as_vector((10e-8, 0.0))#as_vector((0.0, -1.2946e-9))
#vel = as_vector((0.0, 0.0))#as_vector((0.0, -1.2946e-9))
# vel = as_vector((1e-8, 0.0))#as_vector((0.0, -1.2946e-9))

u0_expr = Expression('pow(x[0]-{0}, 2)+pow(x[1]-{1}, 2) < {2} ? 1 : 0'.format(width/2, height/2, width*height/40))
# u0_expr = Constant(1.0)
u_1 = interpolate(u0_expr, V)

T = 24*60*60
dt = T/50

D = Constant(5e-12) #2.249e-12 #Constant(0.3)
diff_term = D*inner(nabla_grad(u), nabla_grad(v))
sed_term = v*(dot(vel, nabla_grad(u)))

unitNormal = FacetNormal(mesh)

# ds = Measure("ds", domain=mesh, subdomain_data=boundary_parts)

a_diff = (u*v + dt*diff_term)*dx
# a_diff_sed = (u*v+dt*(diff_term + sed_term))*dx - dt*vel[1]*(u*v*ds(1) - u*v*ds(0))
a_diff_sed = (u*v+dt*(diff_term + sed_term))*dx - dt*dot(vel, unitNormal)*u*v*ds
a = a_diff
L = v*u_1*dx

# Compute solution
u = Function(V)   # the unknown at a new time level
        # total simulation time
t = 0

A = assemble(a)   # assemble only once, before the time stepping
b = None          # necessary for memory saving assemble call


#plot(u_1)
#time.sleep(10)
max_range = 1.0
plot(u,  range_min=0., range_max=max_range, title='Time: ' + str(t), elevate=0.0)
start_amt = assemble(u_1*dx)
total_fluxed = 0

def calculate_flux(u_1, u):
    u_avg = (u_1 + u) / 2
    ds2 = Measure("ds", domain=mesh, subdomain_data=boundary_parts)
    flux = dot((-D*nabla_grad(u_avg)), unitNormal)*ds2(1)
    flux = dt * assemble(flux)
    return flux

while t <= T:

    # print('time =' + str(t))
    b = assemble(L, tensor=b)

    dirichlet_bc.apply(A, b)
    solve(A, u.vector(), b)

    t += dt
    plot(u, range_min=0., range_max=max_range, title='Time: ' + str(t))
    # print('amount left: {0}'.format(assemble(u_1*dx)))
    # flux = dot(as_vector((0.0, 0.0)), unitNormal)*ds(0, subdomain_data=boundary_parts)(mesh)
    #flux = dot((-D*nabla_grad(u)+vel*u), unitNormal)*ds(0, subdomain_data=boundary_parts)(mesh)
    flux = calculate_flux(u_1, u)
    total_fluxed += flux

    print('flux: {0}'.format(flux))
    u_1.assign(u)
    print('=============')
end_amt = assemble(u_1*dx)
relative_amount_removed = 1 - end_amt/start_amt
print('relative amount removed: {0}'.format(relative_amount_removed))
print('total amount removed: {0}'.format(start_amt-end_amt))
print('total fluxed: {0}'.format(total_fluxed))
interactive()



