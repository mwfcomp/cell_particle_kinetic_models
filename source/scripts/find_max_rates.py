import argparse
import logging

import dolfin.cpp.common
import fitter.mesh_refinement
import utility.config_to_solutions as cts
import utility.fitter_utility as fu
from utility.file_operations import get_config_file_list
from utility.fitter_utility import all_oneway_fns
import fitter.model_comparer
from timeit import default_timer as timer

cts.visualize = False

parser = argparse.ArgumentParser(description='Fit a set of experiments to a set of kinetic models')
parser.add_argument('ini_or_lst_file', type=str,
                    help='path to an ini file OR a lst file with one ini config file per line')
parser.add_argument('output_file', type=str, help='file path for the output csv to put results in')
parser.add_argument('--output_file_per_process', action='store_true',
                    help='create a separate output file for each process based on PID, use if running in parallel'
                         ' independent of python')
parser.add_argument('--base_folder', type=str, default=None,
                    help='base folder if specifying ini file')
model_fns = {'Linear': all_oneway_fns['Linear']}

if __name__ == '__main__':
    start = timer()
    logging.basicConfig(level=logging.ERROR)
    fitter.model_comparer.logger.setLevel(level=logging.INFO)
    fitter.mesh_refinement.logger.setLevel(level=logging.INFO)
    logging.getLogger('FFC').setLevel(logging.ERROR)
    logging.getLogger('UFL').setLevel(logging.ERROR)
    dolfin.cpp.common.set_log_level(dolfin.cpp.common.ERROR)

    args = parser.parse_args()
    fitter.model_comparer.fit_max_min = False
    fitter.model_comparer.do_mesh_refinement = True
    cts.visualize = False

    fitter.model_comparer.output_file_per_process = args.output_file_per_process
    config_files = get_config_file_list(args.ini_or_lst_file, base_folder=args.base_folder)

    fitter_fns = {'fit max rate': fu.get_fitter_max_rate}

    configs = [fu.get_config(config_file) for config_file in config_files]
    #only static adherent have a max rate
    configs = [c for c in configs if c.config['model'].get('condition', 'static_adherent') == 'static_adherent']
    fitter.model_comparer.plot_fn = None
    fu.show_plots = False

    fitter.model_comparer.write_model_fits_and_errors(configs=configs,
                                                      output_file_name=args.output_file,
                                                      desc_to_model_fn_dict=model_fns,
                                                      desc_to_fitter_fn_dict=fitter_fns,
                                                      processes=1)
    end = timer()
    print("Elapsed time: {}s".format(end - start))
