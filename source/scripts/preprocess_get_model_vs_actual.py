import argparse
import logging
import os

import dolfin.cpp.common
import fitter.mesh_refinement
import utility.config_to_solutions as cts
import utility.fitter_utility as fu
from utility.file_operations import get_config_file_list
from utility.fitter_utility import all_oneway_fns
import fitter.model_comparer
import pandas as pd

cts.visualize = False
from timeit import default_timer as timer
from utility.file_operations import remove_config_path

model_fns = all_oneway_fns

parser = argparse.ArgumentParser(description='Takes a set of config files and fit rates and re-generates the output of a model into a csv ')
parser.add_argument('ini_or_lst_file', type=str, help='path to an ini file OR a lst file with one ini config file per line')
parser.add_argument('fit_csv', type=str, help='path to an csv file, should contain following columns: config, model_desc, best_rate, max_rate, min_rate, mesh_scaling')
parser.add_argument('--base_folder', type=str, default=None,
                    help='base folder if specifying ini file')
parser.add_argument('--model', type=str, default='Cell carrying capacity',
                    help='base folder if specifying ini file')

def get_fit_entry(fit_df: pd.DataFrame, model, config):
    row = fit_df[(fit_df['model_desc'] == model) & (fit_df['config'] == os.path.split(config)[1])]
    if len(row) > 1:
        raise ValueError
    return row

if __name__ == '__main__':
    start = timer()
    fu.setup_logging()


    args = parser.parse_args()
    output_file = '{}_{}.csv'.format(os.path.splitext(args.fit_csv)[0], 'model_and_actual')

    output_df = pd.DataFrame()
    config_files = get_config_file_list(args.ini_or_lst_file, base_folder=args.base_folder)

    fit_df = pd.DataFrame.from_csv(args.fit_csv)
    remove_config_path(fit_df)


    config_to_solutions = fu.all_oneway_fns[args.model]
    configs = [fu.get_config(config_file) for config_file in config_files]
    for config in configs:
        row = get_fit_entry(fit_df, args.model, config.config_file)
        best_rate = row['best_rate'].values[0]
        min_rate = row['min_rate'].values[0]
        max_rate = row['max_rate'].values[0]
        mesh_scaling = row['mesh_scaling'].values[0]


        best_model_vals = fu.interpolator_fn(config_to_solutions, config, best_rate, mesh_scaling)
        min_model_vals = fu.interpolator_fn(config_to_solutions, config, min_rate, mesh_scaling)
        max_model_vals = fu.interpolator_fn(config_to_solutions, config, max_rate, mesh_scaling)


        odf = pd.DataFrame({'config': config.config_file,
                            'model_desc': args.model,
                            'time_in_mins': config.times,
                            'actual_values': config.associated_per_cell_means,
                            'actual_std_dev': config.associated_per_cell_means_std_dev,
                            'model_best_rate_values': best_model_vals,
                            'model_min_rate_values': min_model_vals,
                            'model_max_rate_values': max_model_vals,
                            'best_rate': best_rate,
                            'min_rate': min_rate,
                            'max_rate': max_rate})
        output_df = output_df.append(odf, ignore_index=True)
        output_df.to_csv(output_file, index=False)


    #     best_model_vals = fu.interpolator_fn(config_to_solutions, config, best_rate, mesh_scaling)
    #     min_model_vals = fu.interpolator_fn(config_to_solutions, config, min_rate, mesh_scaling)
    #     max_model_vals = fu.interpolator_fn(config_to_solutions, config, max_rate, mesh_scaling)


    end = timer()
    print("Elapsed time: {}s".format(end - start))




