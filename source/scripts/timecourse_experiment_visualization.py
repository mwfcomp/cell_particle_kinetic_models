import sys

from flow_ctyometry.fcs_display import *

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('USAGE: <x axis channel> <y axis channel> <list of ini files>')
    config_files = sys.argv[3:]
    channel_info = ChannelInfo(detect_channel=sys.argv[1], normal_channel=sys.argv[2])
    plt.rcParams["figure.figsize"] = [8, 12]
    try:
        create_tight_figs(config_files, [channel_info])
    except KeyError as e:
        print('Encountered an error, are you sure the channel names are correct?')

    plt.show()

