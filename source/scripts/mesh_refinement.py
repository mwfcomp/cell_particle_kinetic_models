import sys
import utility.config_to_solutions as cts
import utility.fitter_utility as fit
import fitter.mesh_refinement
import logging

logging.basicConfig(level=logging.ERROR)
# mesh_refine_logger = logging.getLogger(fitter.mesh_refinement.__name__)
# mesh_refine_logger.setLevel(logging.INFO)
fitter.mesh_refinement.logger.setLevel(logging.INFO)
# logging.getLogger('FFC').setLevel(logging.ERROR)

fitter.mesh_refinement.report = True
config = fit.get_config(sys.argv[1])
mesh_refinment = fitter.mesh_refinement.get_gci_mesh_refinement(
    config_to_solutions=cts.linear_1D_PDE_timestep_mod,
    config=config,
    rate=1e-11,
    gci_threshold=-1,
    grid_refinement_factor=1.25,
    order_of_accuracy=1)

