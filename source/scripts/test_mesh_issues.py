import utility.fitter_utility as fit
import utility.file_operations as filop
import utility.config_to_solutions as cts
import sys
import matplotlib.pyplot as plt


if len(sys.argv) < 2:
    ini_or_lst_file = "/media/sf_Dropbox/scienceData/fcs data/output/debug/sample.ini"
else:
    ini_or_lst_file = sys.argv[1]

configs = filop.get_config_file_list(ini_or_lst_file)
config = fit.get_config(configs[0])
association_rate = 1e-8

mesh_scaling = 0.8
association_rates = [1e-9, 5e-9, 25e-9]
ys = []
for association_rate in [1e-9, 5e-9, 25e-9]:
    sols = cts.cell_carrying_capacity_1D_PDE(config=config, rate=association_rate, mesh_scaling=mesh_scaling)
    ys.append(sols[-1][1].absolute_amount_removed)
    print('Association rate: {}, remaining: {}'.format(mesh_scaling, sols[-1][1].absolute_amount_removed))
    pass
print('-----------------------------------------------')
plt.plot(association_rates, ys, '-o')
plt.xlabel('association rate')
plt.ylabel('absolute amount removed')
plt.show(block=False)
association_rate = 5e-9
mesh_scalings = [5, 1, 1/5]
ys = []
for mesh_scaling in mesh_scalings:
    sols = cts.cell_carrying_capacity_1D_PDE(config=config,
                                             rate=association_rate,
                                             mesh_scaling=mesh_scaling)
    removed = sols[-1][1].absolute_amount_removed
    ys.append(removed)
    print('Mesh scaling: {}, remaining: {}'.format(mesh_scaling, removed))

plt.plot(mesh_scalings, ys, '-o')
plt.xlabel('mesh scaling')
plt.ylabel('absolute amount removed')
plt.title('Assoc_rate = {}'.format(association_rate))

plt.show()





