import sys
from os import path

from flow_ctyometry.experiment import FullIncubationExperiment, ChannelInfo
from flow_ctyometry.fcs_display import FCSPlotter, demo_transforms_with_histograms
from flow_ctyometry.fcs_file_fixer import convert_and_process_sample
from flow_ctyometry.transforms import *

output_path = ''


def get_transforms(ci: ChannelInfo):
    return [ArcSinHTransform(ci.all_channels())]
            # FilterOutOfRange(ci.normal_channel, 0.5, 1000000),
            # FilterOutOfRange(ci.detect_channel, 0.5, 1000000)]


def visualize(detect_channel, normal_channel, samples: [DataFrame]):
    all_channels = samples[0].columns.values
    print('\nChannels: {}\n'.format(all_channels))
    for ch in detect_channel, normal_channel:
        if ch not in all_channels:
            print('FCS file does not have the channel "{}"! Exiting without plotting.'.format(ch))
            exit()
    ci = ChannelInfo(detect_channel=detect_channel, normal_channel=normal_channel)
    plotter = FCSPlotter(x_channel=ci.detect_channel, y_channel=ci.normal_channel)
    plotter.hist_bins = 100
    plotter.fast_and_ugly = True
    transforms = get_transforms(ci)
    plt.figure(figsize=(24, 10))
    demo_transforms_with_histograms(samples, previsualize_transforms=[], transforms=transforms, plotter=plotter)
    #demo_after_transforms(samples, transforms=transforms, plotter=plotter)
    plt.savefig(output_path)
    plt.show()


def set_output_path(file_name):
    global output_path
    name = path.splitext(path.split(file_name)[1])[0]
    dir_name = path.split(file_name)[0]
    fig_name = '{}_{}_{}.png'.format(name, detect_channel, normal_channel)
    output_path = path.join(dir_name, fig_name)


def visualize_from_ini(file_name):
    exp_gen = FullIncubationExperiment.incubation_generator_from_ini(file_name)
    for exp in exp_gen:
        all_data = exp.blank_controls + exp.single_signal_controls + exp.experimental_data
        set_output_path(file_name)
        visualize(exp.channel_info.detect_channel, exp.channel_info.normal_channel, all_data)


def visualize_from_channels_and_fcs(detect_channel, normal_channel, fcs_file_names):
    samples = [convert_and_process_sample(file) for file in fcs_file_names]
    set_output_path(fcs_file_names[0])
    visualize(detect_channel, normal_channel, samples)

if __name__ == '__main__':
    if '--final' in sys.argv:
        sys.argv.remove('--final')


    if len(sys.argv) == 2:
        visualize_from_ini(sys.argv[1])
    elif len(sys.argv) >= 4:
        detect_channel = sys.argv[1]
        normal_channel = sys.argv[2]
        fcs_files = sys.argv[3:]
        print('FCS files: {}'.format(fcs_files))
        visualize_from_channels_and_fcs(detect_channel, normal_channel, fcs_files)
    else:
        print('Usage: '
              '\n {0} detect_channel normal_channel fcs_file(s) '
              '\n OR'
              '\n {0} ini_file_name'.format(sys.argv[0]))
