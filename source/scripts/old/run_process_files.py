from flow_ctyometry.experiment import FullIncubationExperiment
from flow_ctyometry.fcs_display import new_create_images_from_many_experiments
from utility.flow_utility import *

exps = FullIncubationExperiment.incubation_generator_from_ini('/home/x/dosimetry_tool/data/leo_example.ini')

output_dir = '/media/sf_Dropbox/scienceData/fcs data/images/oct-refactor'
channel_info = EnhancedChannelInfo(normal_channel='488 Grn(Peak)', detect_channel='638 Red(Peak)',
                                   two_normal_channels=['SALS(Peak)', 'LALS(Peak)'])

loader = IncubationExperimentsLoader(
        control_folder='/media/sf_Dropbox/scienceData/fcs data/Leo Flow cytometry/controls',
        channel_info=channel_info, default_time_hrs=2)

# folder_exps =loader.get_incubation_generators_from_folder('/media/sf_Dropbox/scienceData/fcs data/Leo Flow cytometry/2015-07-19')
folder_exps = loader.experiment_generators_from_all_folders('/media/sf_Dropbox/scienceData/fcs data/Leo Flow cytometry/')

# for exp_series in folder_exps:
#     print('new series')
#     for exp in exp_series:
#         print('{} {} {}'.format(exp.particle, exp.cell_line, exp.time))
#         pass

new_create_images_from_many_experiments(output_dir, 3, folder_exps, [get_percent_association, get_mean_signal_per_cell])
