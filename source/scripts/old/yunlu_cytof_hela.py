from flow_ctyometry.fcs_display import *



loader = IncubationExperimentsLoader(
        control_folder='/media/sf_Dropbox/scienceData/fcs data/yunlu platinum particles/CyTOF/20160323  Yunlu CyTOF data/controls',
        default_particle='Yunlu Pt particle')

channel_info = ChannelInfo(normal_channel='Ir191Di', detect_channel='Pt195Di')
workflow = cytof_workflow(channel_info)
all_exps= loader.get_incubation_generators_from_folder('/media/sf_Dropbox/scienceData/fcs data/yunlu platinum particles/CyTOF/20160323  Yunlu CyTOF data/')
exps = [exps for exps in all_exps if exps[0].cell_line == 'PC2'][0]
exps = sorted(exps, key=lambda e: e.time_mins)
workflow.setup_from_controls(exps)
# print(exps[0].fcs_data.columns.values)

samples = [e.fcs_data for e in exps]


plotter = FCSPlotter(x_channel=channel_info.detect_channel, y_channel=channel_info.normal_channel)
plotter.fast_and_ugly = False

demo_transforms(samples, workflow.all_transforms()[0:-1], plotter)
plt.show()

