import dolfin as dolfin

from dosimetry.boundary import *
from dosimetry.experimental_detail import *
from dosimetry.initial_condition import *
from dosimetry.model_maker import *
from dosimetry.solver.standard import *
from dosimetry.solver.three_compartment import *
from dosimetry.solver.updating_boundary import *
from dosimetry.time_and_mesh import MaximumTimestepTimeMaker

# experimental_details = load_details_from_file('../data/model_specification.txt')
from dosimetry.solver.utility import *

time_hrs = 24


#WARNING: 1D WILL WORK FOR SQUARES AND CYLINDERS BUT NOT RECTANGULAR BASES
is_1D = True

def get_model(exp_detail):
    if is_1D:
        model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail, model_maker_cls=ConstantAdvectionDiffusionNoFlux1DModelMaker)
        model_builder.initial_condition_maker = UniformInitialConditionMaker(exp_detail.cell_culture_condition.particle_concentration * exp_detail.cell_culture_condition.width)
    else:
        model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail)
        model_builder.initial_condition_maker = UniformInitialConditionMaker(exp_detail.cell_culture_condition.particle_concentration)
    return model_builder.build_model()

def get_model_solver():
    model_solver_maker = TimestepLinearModelSolver
    model_solver = model_solver_maker()
    return model_solver


cell_culture = CylindricalCellCultureCondition(width=.01105, height=.008, temperature=310.15,
                                               time_in_seconds=time_hrs * 60 * 60, particle_concentration=100,
                                               cell_orientation=CellOrientation.bottom,
                                               flow_speed=0)
#6 well plates
#cell_culture = CylindricalCellCultureCondition(width=.0174, height=.00263, temperature=310.15,
#                                               time=time_hrs*60*60, particle_concentration=100,
#                                               cell_orientation=CellOrientation.bottom,
#                                               flow_speed=0)

# matching_cell_culture = CylindricalCellCultureCondition(width=.01156, height=.04,
#                                                         temperature=310.15, time=-1, particle_concentration=200,
#                                                         cell_orientation=CellOrientation.bottom, flow_speed=0)
matching_cell_culture = RectangularCellCultureCondition(width=.02049, depth=.02049, height=.0048,
                                                        temperature=310.15, time_in_seconds=-1, particle_concentration=100,
                                                        cell_orientation=CellOrientation.bottom, flow_speed=0)



# particle = SphericalParticle(name='SiO2', radius=(500e-9)/2, density=1850)
particle = SphericalParticle(name='SiO2', radius=(50e-9)/2, density=1850)
media = LiquidMedia(viscosity=.00101, density=1000)
experimental_detail = ExperimentalDetail(particle=particle, media=media, cell_culture_condition=cell_culture)
# experimental_detail = load_details_from_file('../data/Leo Systems (1 hr).mspec')[0]




# model_solver_maker = solution_report(graph(model_solver_maker))
# model_solver_maker.max_range = experimental_detail.cell_culture_condition.particle_concentration * 2.0

model_solver = get_model_solver()
model = get_model(experimental_detail)

true_solution = model_solver.solve_model(model)
print('True solution: {}'.format(true_solution))

for hour in range(1, time_hrs):
    # matching_cell_culture.particle_concentration = cell_culture.particle_concentration
    matching_cell_culture.time_in_seconds = hour * 60 * 60
    matching_detail = ExperimentalDetail(particle=particle, media=media, cell_culture_condition=matching_cell_culture)
    model_solver = get_model_solver()
    model = get_model(matching_detail)
    match_solution = model_solver.solve_model(model)
    print('{} hrs: {}'.format(hour, match_solution))


















print('Finished')
# dolfin.interactive()


