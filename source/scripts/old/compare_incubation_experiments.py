import sys
from itertools import tee

import utility.flow_utility as util
from flow_ctyometry.experiment import FullIncubationExperiment
from flow_ctyometry.metrics import get_time_means_stds
from matplotlib import pyplot as plt

# def long_running_computation(x: int):
#     asht = ArcSinHTransform(ChannelInfo('test', 'test'))
#     return MedianSignalPerCell(ControlTransformWorkflow([asht]))
#

if __name__ == '__main__':
    metric_fn = util.get_particles_per_cell
    ini_files = sys.argv[1:]
    exp_gen_gen = FullIncubationExperiment.multi_incubation_generators_from_inis(ini_files)
    metric = None
    for exp_gen in exp_gen_gen:
        gen1, gen2 = tee(exp_gen)
        exp = next(gen1)
        metric = metric_fn(exp)
        times, means, stds = get_time_means_stds(metric_fn, gen2)
        plt.errorbar(times, means, yerr=stds, label=exp.particle, fmt='o-')
    plt.xlabel('Time (mins)')
    plt.ylabel(metric.name)
    plt.legend(loc='best', prop={'size': 10})
    plt.show()
