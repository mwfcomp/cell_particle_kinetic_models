import csv
from matplotlib import pyplot as plt
from pandas import *
import numpy as np
# from pandas import DataFrame

# file_name = '/media/sf_Dropbox/scienceData/compare_pde_vs_ode.csv'
file_name = '/home/x/dosimetry_tool/source/scripts/compare_pde_ode.csv'

df = DataFrame.from_csv(file_name, index_col=None)



grouped_by_condition=df.groupby(['width','height','total_time'])
for name, group in grouped_by_condition:
    plt.figure()
    all_series = group.groupby('dt')
    for series_name, series in all_series:
        # group.plot(group['diffusion_coefficient'], group['numerical_solution'])
        relative_error = np.abs(series['analytical_solution'] - series['numerical_solution']) / series['analytical_solution']
        plt.plot(series['diffusion_coefficient'], relative_error, label='dt={}'.format(series_name))
    title = 'Diffusion coefficent vs relative error for w={},h={},t={}'.format(name[0],name[1],name[2])
    plt.title(title)
    plt.legend(loc='upper right')
    plt.gca().set_xscale('log')
    plt.xlabel('D')
    plt.ylabel('Relative error')
    plt.savefig(title)

plt.show()

# def example():



    #
    #
    #
    # all_series = get_all_series(reader,
    #                             start_new_series_fn=get_new_series_fn('dt'),
    #                             x_fn=lambda row: float(row['diffusion_coefficient']),
    #                             y_fn=lambda row: abs(float(row['analytical_solution'])-float(row['numerical_solution'])) / float(row['analytical_solution'])
    #                             )
    # pass


