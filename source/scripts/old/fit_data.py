import os

import dolfin as dolfin
import scipy

from dosimetry.boundary import *
from dosimetry.experimental_detail import *
from dosimetry.initial_condition import *
from dosimetry.model_maker import *
from dosimetry.solver.standard import *
from dosimetry.solver.three_compartment import *
from dosimetry.solver.updating_boundary import *
import scipy.optimize as opt
from scipy.interpolate import interp1d
import numpy as np
from matplotlib import pyplot as plt
from dosimetry.solver.utility import *

evaluations = 0
bounds = np.array([[1.e-10, 10], [33e-7, 10000]])  # rate bounds are related to sedimentation velocity
# bounds = np.array([[1e-4, 10], [33e-4, 10000]])  # rate bounds are related to sedimentation velocity
start_guess = np.array(bounds[1]) / 2.0
save_fig_location = '/media/sf_Dropbox/scienceData/pde_model_fitting'

time = 24 * 60 * 60
true_rate = 1e-8
# true_rate = 2.5e-4

true_capacity = 300.0


def error_fn(ydata, vals):
    error = np.sum(np.abs(ydata - vals)) / len(vals)
    return error


def pde_model_solutions(time, rate, capacity):
    cell_culture = CylindricalCellCultureCondition(width=.022, height=.0053, temperature=310.15,
                                                   time_in_seconds=time, particle_concentration=1e8,
                                                   cell_orientation=CellOrientation.bottom,
                                                   flow_speed=0)
    particle = SphericalParticle(name='Test', radius=(250e-9) / 2, density=2000)
    media = LiquidMedia(viscosity=.00101, density=1000)
    experimental_detail = ExperimentalDetail(particle=particle, media=media, cell_culture_condition=cell_culture)

    model_builder = standard_constant_advection_diffusion_greedy_model_builder(experimental_detail)
    model_builder.initial_condition_maker = initial_condition_fn(
            experimental_detail.cell_culture_condition.particle_concentration)
    # model_builder.time_maker = MaximumTimestepTimeMaker(400, cell_culture.time)
    model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(0.0)
    model = model_builder.build_model()

    pathway = LinearInternalizationRate(0)

    model_solver_maker = TimestepLinearModelSolver

    model_solver_maker = association_internalization_solver(model_solver_maker)
    model_solver_maker.pathway = pathway
    model_solver_maker.associated_start = 0

    model_solver_maker = updating_linear_boundary_solver(model_solver_maker)
    model_solver_maker.equation_maker = model_builder.equation_maker
    model_solver_maker.bc_updater = SafeRateCell(CellCarryingCapacityAssociationRate(rate=rate, capacity=capacity))

    model_solver_maker = solution_saver(progress_bar(model_solver_maker))
    # model_solver_maker = solution_report(graph(model_solver_maker))
    model_solver_maker.max_range = experimental_detail.cell_culture_condition.particle_concentration * 1.0

    model_solver = model_solver_maker()
    model_solver.solve_model(model)

    return model_solver.solutions


def plot_several_pde(time, capacity):
    base_rate = 3.37e-8 / 2
    xdata = np.linspace(1, time, 24 * 5)  # 24 hours
    for rate in np.array([1, 2, 4, 8, 16, 32]) * base_rate:
        y = evaluate_times_pde_model(xdata, rate, capacity)
        plt.plot(xdata, y, label='rate: {:.2e}'.format(rate, capacity))


def xdata_ydata_from_solutions(solutions: [(float, Solution)]) -> ([float], [float]):
    xdata = [s[0] for s in solutions]
    ydata = [s[1].absolute_amount_associated for s in solutions]
    return xdata, ydata


def print_and_plot(method_name, xdata, rate, capacity):
    y_fit = evaluate_times(xdata, rate, capacity)
    plt.plot(xdata, y_fit, label='{} curve'.format(method_name))


# def fit_using_curve_fit(xdata, ydata):
#     popt, pcov = opt.curve_fit(evaluate_times, xdata, ydata, bounds=bounds)
#     print_and_plot('curve_fit', xdata, popt[0], popt[1])

def fit_rate_scalar(xdata, ydata):
    scalar_fn = lambda rate: minimize_fn((rate, true_capacity))
    res = scipy.optimize.minimize_scalar(scalar_fn, bounds=[1.e-10, 33e-7], method='bounded', options={'xatol': 1e-11})
    print_and_plot('Scalar rate', xdata, res.x, true_capacity)
    return res.x, true_capacity


def fit_using_minimize(method, xdata, ydata, options=None):
    if options is None:
        options = {'disp': True}
    else:
        options['disp'] = True
    res = opt.minimize(minimize_fn, (start_guess[0], start_guess[1]),
                       method=method, bounds=bounds, options=options)
    print_and_plot(method, xdata, res.x[0], res.x[1])
    return res.x[0], res.x[1]


def fit_using_simplex(xdata, ydata):
    init_simplex = np.vstack((bounds, start_guess))
    return fit_using_minimize('Nelder-Mead', xdata, ydata, options={'initial_simplex': init_simplex})


def logistic_fn(xs, rate, capacity):
    return capacity / (1 + np.exp(-rate * (xs - 48000)))


def evaluate_times_pde_model(times, rate, capacity):
    global evaluations
    evaluations += 1
    solutions = pde_model_solutions(np.max(times) + 1, rate, capacity)
    xdata, ydata = xdata_ydata_from_solutions(solutions)
    interpolated_sol = interp1d(xdata, ydata, kind='cubic')
    return interpolated_sol(times)


def get_minimize_fn(xdata, ydata):
    def minimize_fn(rate_and_capacity):
        rate, capacity = rate_and_capacity
        if rate < 0 or capacity < 0:
            return 12e12
        vals = evaluate_times(xdata, rate, capacity)
        error = error_fn(ydata, vals)
        print('Built model with rate={} and capacity={}, error={}'.format(rate, capacity, error))
        return error

    return minimize_fn


def legend_title_save_close(title):
    plt.title(title)
    plt.legend(loc='best', fancybox=True, framealpha=0.5)
    plt.savefig(os.path.join(save_fig_location, '{}.png'.format(title)))
    plt.close()


def do_script():
    global initial_condition_fn, evaluate_times, minimize_fn


    evaluate_times = evaluate_times_pde_model
    # evaluate_times = logistic_fn

    initial_conds = [('Uniform', lambda conc: UniformInitialConditionMaker(conc / 10)),
                     ('Clump', lambda conc: MiddleClumpInitialConditionMaker(conc))]

    fit_fns = [('Scalar rate', fit_rate_scalar), ('Simplex', fit_using_simplex),
               ('TNC', lambda xs, ys: fit_using_minimize('TNC', xs, ys, {'stepmx': 1000.0}))]
    # fit_fns = fit_fns[:1]

    rates = [true_rate]
    capacities = [true_capacity]

    for rate in rates:
        for capacity in capacities:
            for name_initial, initial_cond in initial_conds:
                initial_condition_fn = initial_cond
                xdata = np.linspace(1, 24, 24) * 60 * 60  # 24 hours
                y = evaluate_times(xdata, rate, capacity)
                ydata = y + np.mean(y) / 20 * np.random.normal(size=len(xdata))
                minimize_fn = get_minimize_fn(xdata, ydata)
                for fit_name, fit_fn in fit_fns:
                    plt.plot(xdata, y, label='True')
                    plt.plot(xdata, ydata, label='Raw data')
                    fit_rate, fit_cap = fit_fn(xdata, ydata)

                    title = '{} fit to {}, rate,cap=({:.2e},{:.2e})'.format(fit_name, name_initial,
                                                                            rate, capacity)
                    plt.annotate('fit rate,cap=({:.2e},{:.2e}) '.format(fit_rate, fit_cap),
                                 xy=(0, 5), xycoords='figure points', alpha=0.5)
                    # title = 'Simplex fit, true (rate,cap) = ({:.2e},{:.2e}), fit=({:.2e},{:.2e})'.format(
                    #         true_rate, true_capacity, fit_rate, fit_cap)
                    legend_title_save_close(title)

    for capacity in [1000.0, true_capacity]:
        initial_condition_fn = lambda conc: UniformInitialConditionMaker(conc)
        plot_several_pde(time, capacity)
        title = 'Uniform initial condition, cap={}, varying rate'.format(capacity)
        legend_title_save_close(title)

        initial_condition_fn = lambda conc: MiddleClumpInitialConditionMaker(conc)
        plot_several_pde(time, capacity)
        title = 'Middle clump initial condition, cap={}, varying rate'.format(capacity)
        legend_title_save_close(title)
    print('Plotted rate variation')

    print('Evaluations: {}'.format(evaluations))

    print('Finished')
    # dolfin.interactive()


do_script()
