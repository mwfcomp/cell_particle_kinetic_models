import pandas as pd
import sys
import matplotlib.pyplot as plt
import os

x_axis_col= 'Time (mins)'
y_axis_col = 'Particles per cell (median)'
y_axis_std_dev_col = 'Std Deviation'

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: {} <name> <csv file> <name> <csv file>....')
    final_file_name = 'Fig'

    plt.style.use('seaborn-white')
    # plt.gca().xaxis.grid(False)
    # plt.gca().yaxis.grid(True)

    font = {'family': 'sans-serif',
            'weight': 'bold'}

    plt.rc('font', **font)
    # plt.figure(figsize=(, 3))


    for i in range(1, len(sys.argv), 2):
        name = sys.argv[i]
        final_file_name += '_' + name
        file = sys.argv[i+1]
        df = pd.read_csv(file)
        x = df[x_axis_col][:] / 60
        y = df[y_axis_col][:]
        e = df[y_axis_std_dev_col][:]
        plt.errorbar(x, y, e, label=name, linewidth=1.5, elinewidth=1.2, capthick=1.2)
    #
    # line, _, _ = plt.errorbar(sizes * 1e9, ntes * 1e9,
    #                           yerr=[lower_errs * 1e9, upper_errs * 1e9],
    #                           lolims=lolims,
    #                           ecolor=color, elinewidth=1.5, capthick=1.5)
    # plt.setp([line], linewidth=2, color=color)
    #


    final_file_name += '.jpg'
    plt.xlabel('Time (hrs)', **font)
    (xmin, xmax) = plt.xlim()
    plt.xlim(xmin-0.1*xmin, xmax+0.1*xmax)

    plt.ylabel(y_axis_col, **font)
    plt.legend(loc='best')
    filepath = os.path.join(os.path.split(file)[0], final_file_name)
    plt.savefig(filepath)
    plt.show()