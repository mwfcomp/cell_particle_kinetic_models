import sys

from flow_ctyometry.fcs_display import *

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('USAGE: <x axis channel> <y axis channel> <flow rate> <list of ini or fcs files>')
    config_files = sys.argv[4:]
    channel_info = ChannelInfo(detect_channel=sys.argv[1], normal_channel=sys.argv[2])
    flow_rate = float(sys.argv[3])

    if flow_rate == 0:
        flow_rate = None
    plt.rcParams["figure.figsize"] = [13, 8]
    try:
        create_particle_counting_figs(config_files, channel_info=channel_info, flow_rate_in_ul_per_sec=flow_rate)
    except KeyError as e:
        print('Encountered an error, are you sure channel names are correct?')

    plt.show()



