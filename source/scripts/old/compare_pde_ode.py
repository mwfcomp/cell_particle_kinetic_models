from dosimetry.boundary import LinearFluxCellBoundaryConditionMaker
from dosimetry.solver.standard import *
from dosimetry.time_and_mesh import CGSpaceAndFunctionMaker, MaximumTimestepTimeMaker
from dosimetry.experimental_detail import CellOrientation
import csv


total_time = 10000
file_name = 'compare_pde_ode.csv'

with open(file_name,'w') as file:
    writer = csv.writer(file)
    writer.writerow(['diffusion_coefficient', 'dt', 'width',
                     'height', 'total_time', 'analytical_solution', 'numerical_solution'])
    for width, height in [(100, 10), (10, 100), (50, 50)]:
        for max_dt in [200.0, 100.0, 50.0, 25.0, 10.0, 5.0]:
            for diff_coeff in [1.0, 5.0, 10.0, 50.0, 125.0, 625.0, 3125.0, 5000.0, 10000.0]:
                model_maker = ConstantAdvectionDiffusionNoFluxModelMaker(
                        diffusion_coefficient=diff_coeff,
                        sedimentation_velocity=0.0,
                        flow_speed=0.0,
                        width=width,
                        height=height,
                        total_time=total_time)

                particle_concentration = 100.0
                rate = .001
                model_builder = ModelBuilder()
                model_builder.mesh_maker = model_maker
                model_builder.time_maker = model_maker
                model_builder.time_maker = MaximumTimestepTimeMaker(max_dt, model_maker.time)
                model_builder.space_and_function_maker = CGSpaceAndFunctionMaker()
                model_builder.equation_maker = model_maker
                model_builder.boundary_marker = BoundaryMarker(cell_orientation=CellOrientation.bottom)
                model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(rate)
                init_cond = UniformInitialConditionMaker(particle_concentration)
                model_builder.initial_condition_maker = init_cond

                model = model_builder.build_model()
                model_solver_maker = TimestepLinearModelSolver

                model_solver_maker = (model_solver_maker)
                model_solver_maker.max_range = particle_concentration * 1.0

                model_solver = model_solver_maker()

                solution = model_solver.solve_model(model)
                # print('Finished! Final solution: ')
                # print(solution)

                def analytical_solution(start, time):
                    c1 = start
                    k = rate / height
                    current = c1 * exp(-1 * k * time)
                    return current


                analytical_amt = analytical_solution(particle_concentration, total_time) * width * height

                writer.writerow([diff_coeff, model.dt, width, height, total_time,
                                analytical_amt, solution.absolute_amount_remaining])
                print('Loop iteration finished, diff coeff: {}'.format(diff_coeff))
                file.flush()

        # print('Diffusion coefficient: {}'.format(diff_coeff))
        # print('Numerical PDE solution: {} remaining'.format(solution.absolute_amount_remaining))
        # print('Analytical ODE solution: {} remaining'.format(analytical_amt))

# dolfin.interactive()
