import math


def sphere_volume(r):
    # r += 65
    return 4.0/3 * math.pi * r * r * r

def nice_print(val):
    print('{:.2e}'.format(val))


total_14 = 3.8e3 * sphere_volume(14)
total_50 = 5.83e3 * sphere_volume(50)
total_74 = 2.5e3 * sphere_volume(74)


nice_print(total_14)
nice_print(total_50)
nice_print(total_74)
