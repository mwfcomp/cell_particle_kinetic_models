import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
from leven import levenshtein
from sklearn.preprocessing import scale

from utility.flow_utility import *

output_dir = '/media/sf_Dropbox/scienceData/fcs data/images'
gen = leo_gen
ci = leo_channel_info
fi = leo_fluor_intensities

percent_assoc_workflow = get_percent_assoc_workflow(ci)
mean_intensity_workflow = get_avg_intensity_workflow(ci)

metric_maker = MetricFactory(MedianSignalPerCell, ci.detect_channel, mean_intensity_workflow)


# metric_maker = MetricFactory(PercentCellAssoc, ci.detect_channel, percent_assoc_workflow)


def absolute_similarity(fev1, fev2):
    return dist.euclidean(fev1[2], fev2[2])


def graph_shape_similarity(fev1, fev2):
    v1 = scale(fev1[2])
    v2 = scale(fev2[2])
    return dist.euclidean(v1, v2)


def correlation_similarity(fev1, fev2):
    corr_coeff = np.corrcoef(fev1[2], fev2[2])[0][1]
    return 1 - corr_coeff

def name_similarity(fev1, fev2):
    label1 = '{} {}'.format(fev1[1][-1].particle_desc, fev1[1][-1].cell_line)
    label2 = '{} {}'.format(fev2[1][-1].particle_desc, fev2[1][-1].cell_line)
    return levenshtein(label1, label2)


def get_exps_label(fev):
    f = fev[0]
    exp = fev[1][-1]
    cell = exp.cell_line
    particle = exp.particle_desc
    return '{} {} {}'.format(cell, particle, f)


if __name__ == '__main__':
    folder_exps_vals = []
    for folder, exps in leo_gen:
        if len(exps) == 13 and exps[-1].time_mins == 1440:
            for replicate in set([e.replicate for e in exps]):
                rexps = [e for e in exps if e.replicate == replicate or e.is_control()]
                metric = metric_maker.get_metric(rexps)
                vals = metric.calculate_many(rexps)
                fev = (path.split(folder)[1], rexps, vals)
                folder_exps_vals.append(fev)

    all_labels = [get_exps_label(fev) for fev in folder_exps_vals]
    all_vals = [fev[2] for fev in folder_exps_vals]

    for similarity_metric in name_similarity, absolute_similarity, graph_shape_similarity, correlation_similarity:
        dists = [[similarity_metric(f1, f2) for f1 in folder_exps_vals] for f2 in folder_exps_vals]
        avg_dist = np.median(np.ravel(dists))
        # dists = dist.pdist(folder_exps_vals, graph_shape_similarity)
        linked = hier.linkage(dists, method='average')
        for thresh in 0.2, 0.5, 0.6, 0.7, 1.0, 1.5, 2.0, 4.0, 8.0:
            hier.dendrogram(linked, labels=all_labels, orientation='right', color_threshold=thresh * avg_dist)
            plt.title(metric.name)
            filename = '{}_thr_{}.png'.format(similarity_metric.__name__, thresh)
            short_filename = '{}_thr_{}_short.png'.format(similarity_metric.__name__, thresh)
            plt.tight_layout()
            plt.savefig(path.join(output_dir, filename))
            plt.gca().set_xlim([0, avg_dist * 5])
            plt.tight_layout()
            plt.savefig(path.join(output_dir, short_filename))
            plt.close()




# create_images_from_many_experiments(output_dir, 8, gen, metric_makers, ci)

# Generate sample data
# n_samples = 1500
# np.random.seed(0)
# t = 1.5 * np.pi * (1 + 3 * np.random.rand(1, n_samples))
# x = t * np.cos(t)
# y = t * np.sin(t)
#
#
# X = np.concatenate((x, y))
# X += .7 * np.random.randn(2, n_samples)
# X = X.T
#
# # Create a graph capturing local connectivity. Larger number of neighbors
# # will give more homogeneous clusters to the cost of computation
# # time. A very large number of neighbors gives more evenly distributed
# # cluster sizes, but may not impose the local manifold structure of
# # the data
# knn_graph = kneighbors_graph(X, 30, include_self=False)
#
# for connectivity in (None, knn_graph):
#     for n_clusters in (30, 3):
#         plt.figure(figsize=(10, 4))
#         for index, linkage in enumerate(('average', 'complete', 'ward')):
#             plt.subplot(1, 3, index + 1)
#             model = AgglomerativeClustering(linkage=linkage,
#                                             connectivity=connectivity,
#                                             n_clusters=n_clusters)
#             t0 = time.time()
#             model.fit(X)
#             elapsed_time = time.time() - t0
#             plt.scatter(X[:, 0], X[:, 1], c=model.labels_,
#                         cmap=plt.cm.spectral)
#             plt.title('linkage=%s (time %.2fs)' % (linkage, elapsed_time),
#                       fontdict=dict(verticalalignment='top'))
#             plt.axis('equal')
#             plt.axis('off')
#
#             plt.subplots_adjust(bottom=0, top=.89, wspace=0,
#                                 left=0, right=1)
#             plt.suptitle('n_cluster=%i, connectivity=%r' %
#                          (n_clusters, connectivity is not None), size=17)
#
#
# plt.show()

# X = list(range(1, 50))
# X = np.array([X])
# X = X.T
# np.random.shuffle(X)
#
#
# def number_shared_digits(a, b):
#     a_str = str(a)
#     b_str = str(b)
#     i = 0
#     for c in a_str:
#         if c in b_str:
#             i += 1
#     return 100 - i
#
#
# def affinity_shared_digits(vec):
#     return np.array([[number_shared_digits(a, b) for a in vec[:, 0]] for b in vec[:, 0]])
#
#
# s = affinity_shared_digits(X)
# # clusters = 5
# #
# # # for index, linkage in enumerate(('average', 'complete', 'ward')):
# # for index, linkage in enumerate(('average', 'complete')):
# #     # model = cluster.KMeans(n_clusters=3)
# #     model = cluster.AgglomerativeClustering(linkage=linkage, n_clusters=clusters, affinity=affinity_shared_digits)
# #     # AgglomerativeClustering(linkage=linkage, n_clusters=3)
# #     model.fit(X)
# #     labelled = model.fit_predict(X)
# #     xys = list(zip(X, labelled))
# #     labels = set(labelled)
# #     print('*****************************')
# #     for label in labels:
# #         print('Label: {}'.format(label))
# #         xs = sorted([xy[0][0] for xy in xys if xy[1] == label])
# #         print(xs)
#
# dists = dist.pdist(X, number_shared_digits)
# linked = hier.linkage(dists, method='average')
# hier.dendrogram(linked, labels=X, color_threshold=95.5)
# plt.gca().set_ylim([92, 96])
#
# plt.show()
