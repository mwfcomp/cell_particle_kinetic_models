import matplotlib as mpl
from flow_ctyometry.experiment import ChannelInfo
from flow_ctyometry.fcs_display import create_tight_figs, create_particle_counting_figs
from matplotlib import pyplot as plt

font = {'family': 'sans-serif'}

gd_ir_config_files = [
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/237nm_SiO2_GdDOTA_RAW.ini",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/365nm_SiO2_GdDOTA_AF633_with_RAW_cytof_20170622.ini",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/500nm_PMMA_SiO2_GdDOTA_with_RAW_20170623.ini",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/500nm_PMMA_SiO2_GdDOTA_AF647_with_RAW_cytof_20170630.ini",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/AW188_Zwitterionic_MesoSiO2_GdDOTA_AF647_with_RAW_cytof_20170704.ini"
]
gd_ir_channels = [ChannelInfo(detect_channel='Gd158Di', normal_channel='Ir193Di')]

pt_ir_config_files = [
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/365nm_SiO2_GdDOTA_AF633_with_RAW_cytof_20170622.ini",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/500nm_PMMA_SiO2_GdDOTA_with_RAW_20170623.ini",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/500nm_PMMA_SiO2_GdDOTA_AF647_with_RAW_cytof_20170630.ini",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/AW188_Zwitterionic_MesoSiO2_GdDOTA_AF647_with_RAW_cytof_20170704.ini"
]
pt_ir_channels = [ChannelInfo(detect_channel='Pt195Di', normal_channel='Ir193Di')]


cytof_particle_counting_files = [
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/237nm SiO2 Gd/E17_017 NP e-4_0.FCS",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/237nm SiO2 Gd/E17_017 NP e-5_02.FCS",

    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/365nm SiO2 Gd/E17_035_20170609_365nmSiO2_Gd_B_e-4_01.FCS",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/365nm SiO2 Gd/E17_035_20170609_365nmSiO2_Gd_B_e-5_01.FCS",

    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/500nm PMMA Gd/E17_035_20170615_500nmPMMA_Gd_e-3_NP_01.FCS",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/500nm PMMA Gd/E17_035_20170615_500nmPMMA_Gd_e-4_NP_01.FCS",

    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/Zwitter Gd/E17_035_20170609_Zwit_Gd_NP_e-4_01.FCS",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/cytof serial dilutions/Zwitter Gd/E17_035_20170609_Zwit_Gd_NP_e-5_01.FCS"
]
gd_gd_channel_info = ChannelInfo(detect_channel='Gd158Di', normal_channel='Gd160Di')
helios_cytof_flow_rate_in_ul_per_sec = 0.5

flow_fcs_cell_files = [
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/fixing vs non fixing/RAW264.7_6_well_plate.fcs",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/fixing vs non fixing/RAW264.7_6_well_plate_2.fcs",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/fixing vs non fixing/RAW264.7_6_well_plate_3.fcs",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/fixing vs non fixing/RAW_cells_cytof_protocol_1.fcs",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/fixing vs non fixing/RAW_cells_cytof_protocol_2.fcs",
    "/media/sf_Dropbox/scienceData/fcs data/fluor_vs_metal ion/renamed/fixing vs non fixing/RAW_cells_cytof_protocol_3.fcs"
]
flow_channels = ChannelInfo(detect_channel='638 Red(Peak)', normal_channel='488 Grn(Peak)')
#1.5ul per min FOR PARTICLES
#15 ul per min FOR CELLS
apogee_cell_flow_rate_in_ul_per_sec = 0.25
apogee_particle_flow_rate_in_ul_per_sec = .025


def make_mass_cytometry_figures():
    # plt.style.use('seaborn-whitegrid')
    # mpl.rc('lines', linewidth=3)
    mpl.rc('font', **font)
    plt.rcParams["figure.figsize"] = [13, 8]
    create_particle_counting_figs(cytof_particle_counting_files, channel_info=gd_gd_channel_info,
                                  flow_rate_in_ul_per_sec=helios_cytof_flow_rate_in_ul_per_sec)
    create_particle_counting_figs(flow_fcs_cell_files, flow_channels, 0)
    create_particle_counting_figs(gd_ir_config_files, channel_info=gd_gd_channel_info,
                                  flow_rate_in_ul_per_sec=helios_cytof_flow_rate_in_ul_per_sec)
    plt.show(block=False)
    plt.rcParams["figure.figsize"] = [13, 17.333]
    create_tight_figs(gd_ir_config_files, gd_ir_channels)
    create_tight_figs(pt_ir_config_files, pt_ir_channels)


if __name__ == '__main__':
    make_mass_cytometry_figures()
