import dolfin as dolfin

from dosimetry.boundary import *
from dosimetry.experimental_detail import *
from dosimetry.initial_condition import *
from dosimetry.model_maker import *
from dosimetry.solver.standard import *
from dosimetry.solver.three_compartment import *
from dosimetry.solver.updating_boundary import *
from dosimetry.time_and_mesh import MaximumTimestepTimeMaker

# experimental_details = load_details_from_file('../data/model_specification.txt')
from dosimetry.solver.utility import *

cell_culture = CylindricalCellCultureCondition(width=.04, height=.02, temperature=310.15,
                                               time_in_seconds=5 * 24 * 60 * 60, particle_concentration=1e6,
                                               cell_orientation=CellOrientation.bottom,
                                               flow_speed=0)
# cell_culture = CylindricalCellCultureCondition(width=1, height=.0053, temperature=310.15,
#                                                time=24*60*60, particle_concentration=1e8,
#                                                cell_orientation=CellOrientation.bottom,
#                                                flow_speed=0)
particle = SphericalParticle(name='Test', radius=(200e-9)/2, density=1500)
media = LiquidMedia(viscosity=.00101, density=1000)
experimental_detail = ExperimentalDetail(particle=particle, media=media, cell_culture_condition=cell_culture)
# experimental_detail = load_details_from_file('../data/Leo Systems (1 hr).mspec')[0]

is_1D = False

if is_1D:
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(experimental_detail, model_maker_cls=ConstantAdvectionDiffusionNoFlux1DModelMaker)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(experimental_detail.cell_culture_condition.particle_concentration * experimental_detail.cell_culture_condition.width)
else:
    model_builder = standard_constant_advection_diffusion_greedy_model_builder(experimental_detail)
    model_builder.initial_condition_maker = UniformInitialConditionMaker(experimental_detail.cell_culture_condition.particle_concentration)
# model_builder.initial_condition_maker = RaisedUniformInitialConditionMaker(experimental_detail.cell_culture_condition.particle_concentration)
# model_builder.initial_condition_maker = MiddleClumpInitialConditionMaker((experimental_detail.cell_culture_condition.particle_concentration))
# model_builder.time_maker = MaximumTimestepTimeMaker(400, cell_culture.time)
#model_builder.boundary_condition_maker = ConstantFluxCellBoundaryConditionMaker(5e-9)
model_builder.boundary_condition_maker = LinearFluxCellBoundaryConditionMaker(0)
# model_builder.boundary_condition_maker = NoFluxBoundaryConditionMaker()
# model_builder.boundary_condition_maker = MaxGreedyBoundaryConditionMaker()
# model_builder.time_maker = MaximumTimestepTimeMaker(cell_culture.time / 1000, cell_culture.time)
model = model_builder.build_model()
model_solver_maker = TimestepLinearModelSolver

model_solver_maker = association_internalization_solver(model_solver_maker)
pathway = LinearInternalizationRate(0)
model_solver_maker.pathway = pathway
model_solver_maker.associated_start = 0
model_solver_maker = updating_linear_boundary_solver(model_solver_maker)
model_solver_maker.equation_maker = model_builder.equation_maker

max_rate = model_builder.equation_maker.advection_vector[1] / model_builder.equation_maker.width
rate = float(-1.0 * max_rate)

if is_1D:
    model_solver_maker.bc_updater = SafeRate1D(CellCarryingCapacityAssociationRate(rate=rate, capacity=1.0 * 5e3))
else:
    model_solver_maker.bc_updater = SafeRateCell(CellCarryingCapacityAssociationRate(rate=rate, capacity=1.0 * 5e3))


model_solver_maker = solution_report(graph(model_solver_maker))
model_solver_maker.max_range = experimental_detail.cell_culture_condition.particle_concentration* 2.0
model_solver = model_solver_maker()
solution = model_solver.solve_model(model)


print('Finished')
dolfin.interactive()


