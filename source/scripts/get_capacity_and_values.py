import utility.config_to_solutions as cts
from utility.config_interpreter import ConfigInterpreter
from utility.file_operations import get_config_file_list
import sys
import utility.flow_utility as fu
import pandas as pd
import os.path


def get_config(config_file):
    config = ConfigInterpreter(config_file,
                               fu.get_particles_per_cell,
                               fu.get_standard_error_of_mean_all_samples,
                               fu.remove_outliers_and_control_median_workflow)
    return config


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: <output_file>')
        exit()

    config_list_file = sys.argv[1]
    config_files = get_config_file_list(config_list_file)
    cdf = pd.DataFrame()
    tdf = pd.DataFrame()

    for c_file in config_files:
        config = get_config(c_file)
        capacity = cts.max_capacity_from_last_derivative_compared_to_max_derivative(config)
        cdf = cdf.append({
            'config': c_file,
            'capacity': capacity
        }, ignore_index=True)
        c_files = [c_file] * len(config.times)

        rdf = pd.DataFrame({'config': c_files,
                            'time': config.times,
                            'values': config.associated_per_cell_means,
                            'std_dev': config.associated_per_cell_means_std_dev})
        tdf = tdf.append(rdf, ignore_index=True)

    cdf.to_csv('{}_capacity.csv'.format(os.path.splitext(config_list_file)[0]), index=False)
    tdf.to_csv('{}_values_and_std_devs.csv'.format(os.path.splitext(config_list_file)[0]), index=False)
