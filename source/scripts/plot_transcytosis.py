import pandas as pd
import matplotlib.pyplot as plt
import sys

if __name__ == '__main__':

    df = pd.read_csv(sys.argv[1])

    plt.style.use("matt.mplstyle")



    # for style in plt.style.available:
    # plt.style.use(style)
    # ax.plot([t/60 for t in config.times], config.transcytosed_per_cell_means, color='C0', linestyle='-', label='Transcytosed (actual)')
    # ax.plot(best_sol.t/3600, best_sol.y[2], color='C0', linestyle='--', label='Transcytosed (model), r={}'.format(full_rate[1]))
    plt.plot(list(df['Time(h)'][:]), list(df['Transcytosed'][:]))
    # plt.show()
    # plt.title(style)
    plt.savefig("test.png")


    # df = pd.DataFrame(list(zip(best_sol.t/3600, best_sol.y[1], best_sol.y[2])), columns=['Time(h)','Associated','Transcytosed'])
    # df.to_csv(os.path.join(savepath, config.experimental_and_cell_detail.cell_info.cell_line +'.csv'),index=False)
    # plt.show()
    # plt.savefig(os.path.join(savepath, output_name))
