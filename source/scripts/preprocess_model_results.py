import os
import sys
import pandas as pd
import utility.file_operations as fo
from utility.fitter_utility import get_config


if len(sys.argv) != 2:
    print('Usage: <output file LST>')
    exit()

file = sys.argv[1]
config_file_list = fo.get_config_file_list(file)
odf = pd.DataFrame()

# config_file_list = config_file_list[1:3]
cfgs = [get_config(file) for file in config_file_list]
dfs = [cfg.get_dataframe() for cfg in cfgs]
final_df = pd.concat(dfs)
output_filename = '{}_config_details.csv'.format(os.path.splitext(file)[0])
final_df.to_csv(output_filename)
