import sys
from configparser import ConfigParser
from utility.config_interpreter import ConfigInterpreter

from flow_ctyometry.fcs_display import *
from utility.flow_utility import *


def visualize_gating(exp_gen):
    for idx, exp in enumerate(exp_gen):
        if idx == 0:  # only do this for the first one
            ci = exp.channel_info
            pa = get_percent_association(exp)
            pa.setup_from_controls(exp)
            outlier_filter = [w for w in pa.pre_count_workflow.all_transforms() if isinstance(w, FilterOutliers)][0]
            normal_filter = [w for w in pa.post_count_workflow.all_transforms() if isinstance(w, FilterNormal)][0]
            out_plotter = FCSPlotter(x_channel=ci.two_normal_channels[0],
                                     y_channel=ci.two_normal_channels[1])
            detect_plotter = FCSPlotter(x_channel=ci.detect_channel,
                                        y_channel=ci.normal_channel)
            out_plotter.fast_and_ugly = True
            detect_plotter.fast_and_ugly = True
            plt.figure(figsize=(24, 10))
            pre_transform = pa.pre_count_workflow.all_transforms()[0]
            datas = pre_transform.apply_to_many(exp.blank_controls)
            demo_gating(datas, outlier_filter, normal_filter, out_plotter, detect_plotter)
            return


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please enter the path of at least one INI file')
    config_files = sys.argv[1:]
    for config_file in config_files:
        particles_per_cell_config = ConfigInterpreter(config_file,
                                                      get_particles_per_cell,
                                                      get_standard_error_of_mean_all_samples,
                                                      remove_outliers_and_control_median_workflow)

        mean_fluorescent_intensity_config = ConfigInterpreter(config_file,
                                                              get_mean_signal_per_cell,
                                                              get_standard_error_of_mean_all_samples,
                                                              no_preprocessing_workflow)

        percent_assoc_config = ConfigInterpreter(config_file,
                                                 get_percent_association,
                                                 get_standard_error_of_mean_all_samples,
                                                 remove_outliers_and_control_median_workflow)

        particles_per_cell_config.to_csv('particles_per_cell')
        mean_fluorescent_intensity_config.to_csv('mean_fluorescent_intensity')
        percent_assoc_config.to_csv('percent_cell_assoc')

        create_figure_from_experiments(particles_per_cell_config.get_experiment_generator(),
                                       1,
                                       [percent_assoc_config.exp_to_metric_fn,
                                        mean_fluorescent_intensity_config.exp_to_metric_fn,
                                        particles_per_cell_config.exp_to_metric_fn])
        save_image(config_file)
        plt.figure()
    plt.show()



