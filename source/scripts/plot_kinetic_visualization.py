from time import sleep

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint  # dy/dt = func(y, t0, ...)
import dosimetry.ode_model as ode


def get_linear_ode_rate_fn(rate):
    deriv_fn = lambda amt, time: -1 * rate * amt
    return deriv_fn


def get_surface_capacity_ode_rate_fn(rate, sc):
    deriv_fn = lambda amt, time: (-1 * rate * sc / (sc + amt)) * amt
    return deriv_fn


def get_cell_carrying_capacity_ode_rate_fn(rate, cell_cap, initial_amount=1.):
    def get_rate(rate, current_amt):
        amount_removed = initial_amount - current_amt
        rate = -1 * rate * (cell_cap - amount_removed) / cell_cap
        return rate * current_amt

    # fn = get_rate(rate, cell_cap, initial_amount)
    deriv_fn = lambda amt, time: get_rate(rate, amt)
    return deriv_fn


def get_full_carrying_capacity_ode_rate_fn(rate, sc, cell_cap, initial_amount=1.):
    def cell_and_surface_capacity_function(scaling, current_amt):
        amount_removed = initial_amount - current_amt
        current_flux = -1 * scaling * rate * sc / (sc + current_amt) * \
                       (cell_cap - amount_removed) / cell_cap
        return current_flux * current_amt

    deriv_fn = lambda amt, time: cell_and_surface_capacity_function(1, amt)
    return deriv_fn


def plot_both(axs, deriv_fn, time_xs, conc_xs, initial_amount=1.):
    rate_resp = -1 * np.array([deriv_fn(x, 0) for x in conc_xs])

    conc_resp = initial_amount - odeint(deriv_fn, initial_amount, time_xs)

    axs[0].plot(conc_xs, rate_resp)
    axs[1].plot(time_xs, conc_resp)

    axs[0].set_xlim(0, max(conc_xs))
    axs[1].set_xlim(0, max(time_xs))
    # axs[0].set_ylim(0, max_rate*100 )
    axs[1].set_ylim(0, initial_amount)
    for ax in axs:
        ax.set_yticks([])
        ax.set_xticks([])


txs = np.arange(0, 500, .1)
cxs = np.arange(0, 30, .1)

fig, axs = plt.subplots(nrows=4, ncols=2)

max_rate = 0.01
surf_cap = 1
cell_cap = 0.8

for max_rate in [.01]:
    plot_both(axs[0], get_linear_ode_rate_fn(max_rate), txs, cxs)
    for surf_cap in [1]:
        plot_both(axs[1], get_surface_capacity_ode_rate_fn(max_rate, surf_cap), txs, cxs)
        axs[1][0].axvline(surf_cap, linestyle='--')
    # plot_both(axs[1], get_surface_capacity_ode_rate_fn(max_rate, .1), txs, cxs)
    # plot_both(axs[1], get_surface_capacity_ode_rate_fn(max_rate, 1), txs, cxs)
    # plot_both(axs[1], get_surface_capacity_ode_rate_fn(max_rate, 10), txs, cxs)
    plot_both(axs[2], get_cell_carrying_capacity_ode_rate_fn(max_rate, cell_cap), txs, cxs)
    axs[2][1].axhline(cell_cap, linestyle='--')

    plot_both(axs[3], get_full_carrying_capacity_ode_rate_fn(max_rate, surf_cap, cell_cap), txs, cxs)
    axs[3][0].axvline(surf_cap, linestyle='--')
    axs[3][1].axhline(cell_cap, linestyle='--')



axs[3][0].set_xlabel('Concentration')
axs[3][1].set_xlabel('Time')
fig.text(0.11, 0.5, 'Rate', horizontalalignment='center', verticalalignment='center', rotation='vertical')
fig.text(0.53, 0.5, 'Associated Amount', horizontalalignment='center', verticalalignment='center', rotation='vertical')
axs[0][1].text(-700, 0.9, 'e)', weight='bold')
axs[1][1].text(-700, 0.9, 'f)', weight='bold')
axs[2][1].text(-700, 0.9, 'g)', weight='bold')
axs[3][1].text(-700, 0.9, 'h)', weight='bold')

#
# for i, ltr in enumerate(['e)', 'f)', 'g)', 'h)']):
#     fig.text(.05, .83-i*.22, ltr, weight='bold', transform=axs[0].transAxes)



plt.show(block=False)
plt.savefig('kinetic_visual.png')
sleep(50)
exit()
