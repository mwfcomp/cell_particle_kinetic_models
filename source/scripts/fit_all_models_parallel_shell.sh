#!/bin/bash
#usage: script <lst input file> <base folder> <csv output file> <num_processes>
xargs -a "$1" -d '\n' -i -t --max-procs=$4 bash -c "python3 fit_all_models.py \"{}\" \"$3\" --base_folder \"$2\" --output_file_per_process"
