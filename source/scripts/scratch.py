import pandas as pd
from utility.file_operations import remove_config_path
import matplotlib.pyplot as plt
from math import ceil
import matplotlib.patches as mpatches
from collections import OrderedDict

config_col = 'config'
best_rate_col = 'best_rate'
best_rate_error_col = 'best_rate_error'
min_rate_col = 'min_rate'
max_rate_col = 'max_rate'
model_col = 'model_desc'
normalized_error_col = 'normalized_error'
merge_add = "_merge"
excluded_models = ['Idealized perfectly absorbing']
non_parameter_models = ['Idealized perfectly absorbing', 'Perfectly absorbing']
relative_performance_col = 'relative_performance'
particle_name_col = 'particle_name'
cell_line_col = 'cell_line_name'
max_detectable_rate_col = 'max_detectable_rate'
radius_col = 'radius_in_m'
perfectly_absorbing = 'Perfectly absorbing'
cell_carrying_cap = 'Cell carrying capacity'

name_to_display_name = OrderedDict([('Perfectly absorbing', perfectly_absorbing),
                                    ('Cell carrying capacity', cell_carrying_cap),
                                    ('Surface flux and cell carrying capacity', 'Full (surface flux+cell capacity)'),
                                    ('Linear', 'Linear'),
                                    ('Surface flux', 'Surface flux')])


def get_combined_filtered_results(results: pd.DataFrame,
                                  config_details: pd.DataFrame,
                                  max_rates: pd.DataFrame):
    # reset config names to only contain the filename (in case runs on different systems)
    for df in [results, config_details, max_rates]:
        remove_config_path(df)

    config_info = config_details.groupby(by=config_col,
                                         as_index=False).first()  # only care about details, not time info

    max_rates = max_rates.rename(columns={best_rate_col: max_detectable_rate_col})
    cdf = results.merge(config_info, on=config_col, suffixes=('', '_details'))
    cdf = cdf.merge(max_rates, on=config_col, how='left', suffixes=('', '_max_rates'))
    cdf[max_detectable_rate_col] = cdf[max_detectable_rate_col].fillna(
        9999)  # if there's no info in the max rates col, the max rate is infinite
    cdf = cdf[~cdf[model_col].isin(excluded_models)]
    cdf[model_col] = cdf[model_col].map(name_to_display_name)
    return cdf

def plot_all_model_vs_experimental(df: pd.DataFrame, cdf: pd.DataFrame, rows, cols):
    fig, axes = plt.subplots(rows, cols,
                             figsize=(10.8, 1.2 * (rows + 2)))
    axes = axes.flatten()

    model_color = '#1f77b4'

    agdf = list(df.groupby(by=[config_col]))
    agdf.sort(key=lambda c: cdf[cdf.config == c[0]][radius_col].values[0]) #sort by config size

    for i, (gname, gdf) in enumerate(agdf):
        gdf = gdf.sort_values(by='time_in_mins')
        times = gdf['time_in_mins'].values / 60
        actual = gdf['actual_values'].values
        std_dev = gdf['actual_std_dev'].values
        model_best = gdf['model_best_rate_values'].values
        model_min = gdf['model_min_rate_values'].values
        model_max = gdf['model_max_rate_values'].values
        #         axes[i].linewidth = 3.0
        axes[i].errorbar(times, actual, yerr=std_dev, color= '#FF4500')  # '#8c564b'
        # axes[i].plot(times, actual)
        axes[i].plot(times, model_best, color=model_color)
        axes[i].plot(times, model_min, linestyle='--', color=model_color)
        axes[i].plot(times, model_max, linestyle='--', color=model_color)
        axes[i].set_xlim([0, 25])
        ymax = axes[i].get_ylim()[1]
        if ymax < 0.5:
            ymax = ceil(ymax * 10) / 10
        else:
            ymax = ceil(ymax)
        axes[i].set_ylim([0, ymax])
        axes[i].set_xticks([])
        if i >= rows * (cols - 1):
            axes[i].set_xticks([0, 25])
            # axes[i].set_xlabel('Time (h)')
        axes[i].set_yticks([0, ymax])
        axes[i].tick_params(axis='both', which='both', length=0)
        axes[i].spines['top'].set_visible(False)
        axes[i].spines['right'].set_visible(False)
        letter_label = '{})'.format(chr(i + 97))
        axes[i].text(-0.23, 1.2, letter_label, verticalalignment='center', horizontalalignment='center',
                     fontweight='bold', transform=axes[i].transAxes)
        print('{} {}'.format(letter_label, gname), end=' ')
    print('')
    exp_patch = mpatches.Patch(color='#FF4500', label='')
    model_patch = mpatches.Patch(color=model_color, label='')
    plt.tight_layout()
    fig.legend(handles=[exp_patch, model_patch], labels=['Actual', 'Model'], loc='lower center', ncol=2)
    plt.subplots_adjust(bottom=0.1)
    plt.show()

results_file = "/media/sf_Dropbox/scienceData/fcs data/output/run_2018_08_29/combined.csv"
results = remove_config_path(pd.read_csv(results_file))
config_details = remove_config_path(pd.read_csv("/media/sf_Dropbox/scienceData/fcs data/output/run_2018_08_29/all_config_details.csv"))
model_and_actual = remove_config_path(pd.read_csv("/media/sf_Dropbox/scienceData/fcs data/output/run_2018_08_29/combined_model_and_actual.csv"))
max_rates = remove_config_path(pd.read_csv("/media/sf_Dropbox/scienceData/fcs data/output/max_rate.csv"))

full_results = get_combined_filtered_results(results, config_details, max_rates)
nims_only = full_results[~(full_results[config_col].str.contains('fuchs_'))]
fuchs_only = full_results[(full_results[config_col].str.contains('fuchs_'))]

ccc_df = nims_only[nims_only[model_col] == cell_carrying_cap]
cn = remove_config_path(ccc_df)
cn = cn.merge(model_and_actual, on=[config_col, 'time_in_mins'])

nims_only[nims_only.config == 'leo_95nm_coreshell_raw.ini']

plot_all_model_vs_experimental(model_and_actual, config_details, 5, 5)


# plot_all_model_vs_experimental(model_and_actual, 5, 5)




