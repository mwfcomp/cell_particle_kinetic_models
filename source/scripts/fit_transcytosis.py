import argparse
import sys

import utility.config_to_solutions_transcytosis as ctst

from utility.config_to_solutions_transcytosis import \
    transcytosis_cell_carrying_capacity_adherent_constant_concentration_well_mixed_ODE, \
    cell_carrying_capacity_linear_transcytosis_1D_PDE, both_rate_parameter_fitter, \
    transcytosis_only_rate_parameter_fitter, combined_model, linear_static_adherent_PDE, \
    linear_constant_supply_adherent_ODE, get_carrying_capacity_equation, always_1k_carrying_capacity
from utility.file_operations import get_config_file_list
from utility.fitter_utility import get_config
import matplotlib.pyplot as plt
import scipy.optimize
import pandas as pd
from enum import Enum


import os.path

parser = argparse.ArgumentParser(description='Fits a set of experiments to a transcytosis and association models')
parser.add_argument('--a', type=str, default=None, help='LST or INI file to fit ASSOCIATION ONLY model to.')
parser.add_argument('--t', type=str, default=None, help='LST or INI file to fit TRANSCYTOSIS ONLY model to.')
parser.add_argument('--c', type=str, default=None, help='LST or INI file to fit COMBINED TRANSCYTOSIS/ASSOCIATION model to.')

class FitType(Enum):
    Assoc = 1
    Trans = 2
    Both = 3

def fit_stuff(input_file,
              cts,
              fit_type: FitType,
              output_name,
              default_assoc_rate=-1,
              default_trans_rate=-1,
              mesh_scaling=0.8):

    configs = get_config_file_list(ini_or_lst_file=input_file)
    savepath = os.path.split(input_file)[0]

    plt.style.use("matt.mplstyle")
    c = len(configs)
    fit_has_association = (fit_type in [FitType.Assoc, FitType.Both])
    fit_has_transcytosis = (fit_type in [FitType.Trans, FitType.Both])

    if fit_has_association and fit_has_transcytosis:
        r=2
    else:
        r=1

    fw, fh = plt.rcParams['figure.figsize']
    fig, axes = plt.subplots(r, c, sharey='row', squeeze=False, figsize=(fw*c, fh*r), dpi=300)
    # plt.figure(figsize=(6.4*c, 4.8*r), dpi=300)



    #always use limits from first graph
    for i, filename in enumerate(configs):
        print('------------------------------')
        print('Running {}'.format(filename))
        print('------------------------------')
        config = get_config(filename)

        if fit_type == FitType.Both:
            fitter = both_rate_parameter_fitter(config_to_solutions=cts, mesh_scaling=mesh_scaling)
            best_rate, best_rate_err = fitter.get_best_fit_parameter_and_error(config)
            full_rate = best_rate
        elif fit_type == FitType.Trans:
            fitter = transcytosis_only_rate_parameter_fitter(config_to_solutions=cts,
                                                              association_rate=default_assoc_rate,
                                                              mesh_scaling=mesh_scaling)
            best_rate, best_rate_err = fitter.get_best_fit_parameter_and_error(config)
            full_rate = [default_assoc_rate, best_rate]
        else: # fit_type == FitType.Assoc:
            fitter = ctst.association_only_rate_parameter_fitter(config_to_solutions=cts,
                                                                 transcytosis_rate=default_trans_rate,
                                                                 mesh_scaling=mesh_scaling)
            best_rate, best_rate_err = fitter.get_best_fit_parameter_and_error(config)
            full_rate = [best_rate, default_trans_rate]
            pass

        best_sol = cts(config=config,
                       association_rate=full_rate[0],
                       transcytosis_rate=full_rate[1],
                       mesh_scaling=mesh_scaling)

        ax = axes[0,i]
        # ax.set_yscale('symlog')
        if fit_has_association:
            ax.plot([t/60 for t in config.times], config.associated_per_cell_means, color='C1', linestyle='-', marker='o', label='Actual')
            ax.plot(best_sol.t/3600, best_sol.y[1], color='C1', linestyle='--', label='Model, r={:.2e}'.format(full_rate[0]))

            if i==0:
                ax.set_ylabel('Associated particles per cell')
            ax.set_xlabel('Time (h)')
            ax.set_title(config.experimental_and_cell_detail.cell_info.cell_line)
            ax.legend(loc='best')

        if fit_has_transcytosis and fit_has_association:
            ax = axes[1, i]
            # plt.subplot(r,c,i+1+c, sharey='row')

        if fit_has_transcytosis:
            # plt.subplot(r,c,i+1)
            ax.plot([t/60 for t in config.times], config.transcytosed_per_cell_means, color='C0', linestyle='-', marker='o', label='Actual')
            ax.plot(best_sol.t/3600, best_sol.y[2], color='C0', linestyle='--', label='Model, r={:.2e}'.format(full_rate[1]))
            if i==0:
                ax.set_ylabel('Particles per cell transcytosed')
            if not fit_has_association:
                ax.set_title(config.experimental_and_cell_detail.cell_info.cell_line)
            ax.set_xlabel('Time (h)')
            ax.legend(loc='best')

        df = pd.DataFrame(list(zip(best_sol.t/3600, best_sol.y[1], best_sol.y[2])), columns=['Time(h)','Associated','Transcytosed'])
        df.to_csv(os.path.join(savepath, config.experimental_and_cell_detail.cell_info.cell_line +'.csv'),index=False)
        # plt.show()
        plt.savefig(os.path.join(savepath, output_name))

args = parser.parse_args()
print('Version is {}'.format(scipy.__version__))

ctst.get_carrying_capacity_equation = ctst.max_capacity_from_last_derivative_compared_to_max_derivative
only_linear_trans = combined_model(linear_static_adherent_PDE,
                                   linear_constant_supply_adherent_ODE)
assoc_and_trans_cts = combined_model(cell_carrying_capacity_linear_transcytosis_1D_PDE,
                                     transcytosis_cell_carrying_capacity_adherent_constant_concentration_well_mixed_ODE)

mesh_scaling = 0.3
if args.a is not None:
    fit_stuff(input_file=args.a,
              cts=assoc_and_trans_cts,
              fit_type=FitType.Assoc,
              output_name='assoc.png',
              default_trans_rate=0.0,
              mesh_scaling=mesh_scaling)


if args.t is not None:
    fit_stuff(input_file=args.t,
              cts=only_linear_trans,
              fit_type=FitType.Trans,
              output_name='only_linear_transcytosis.png',
              mesh_scaling=mesh_scaling)


if args.c is not None:
    fit_stuff(input_file=args.c,
              cts=assoc_and_trans_cts,
              fit_type=FitType.Both,
              output_name='assoc_and_trans_test_orig_{}_mesh.png'.format(mesh_scaling),
              mesh_scaling=mesh_scaling)


exit()
