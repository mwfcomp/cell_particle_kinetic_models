from dosimetry.solver.three_compartment import AssociationInternalizationSolution
from utility.config_interpreter import ConfigInterpreter
from utility.fitter_utility import get_config
import utility.config_to_solutions as cts
import matplotlib.pyplot as plt

for file in ["/mnt/c/Dropbox/scienceData/fcs data/incubation_ini_files_for_paper/leo_150nm_coreshell_thp1.ini",
             "/mnt/c/Dropbox/scienceData/fcs data/incubation_ini_files_for_paper/leo_214nm_capsule_thp1.ini",
             "/mnt/c/Dropbox/scienceData/fcs data/incubation_ini_files_for_paper/leo_480nm_capsule_thp1.ini",
             "/mnt/c/Dropbox/scienceData/fcs data/incubation_ini_files_for_paper/leo_1032nm_capsule_thp1.ini",
             "/mnt/c/Dropbox/scienceData/fcs data/incubation_ini_files_for_paper/leo_633nm_coreshell_thp1.ini",
             "/mnt/c/Dropbox/scienceData/fcs data/incubation_ini_files_for_paper/leo_282nm_coreshell_thp1.ini"]:

    config = get_config(file)
    #"/media/sf_Dropbox/scienceData/fcs data/incubation_ini_files_for_paper/static_PMA_coreshell.ini"
    config.output_raw_flow_data("/mnt/c/Dropbox//scienceData/stuart_matt_share/for_stuart")


sols = cts.cell_carrying_capacity_1D_PDE_carrying_cap_specified(config=config, rate=9e-7,
                                                                carrying_cap=1000, mesh_scaling=0.4)

xs = [s[0] for s in sols]
ys = [s[1].absolute_amount_removed for s in sols]
plt.plot(xs, ys)
plt.show()

pass


