from dolfin.cpp.common import set_log_level
import utility.config_to_solutions as cts
import utility.config_to_solutions
import utility.flow_utility as fu
from dosimetry.solver.standard import *
from fitter.single_parameter_fitter import ParameterFitter, bounded_objective_fn, \
    min_config_subtract_dispersions, max_config_adding_dispersions
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
from utility.config_interpreter import ConfigInterpreter
import sys
import time

#possibly can delete this in favor of the more flexible "fit_all_models". Stay tuned
utility.config_to_solutions.visualize = False
dispersion_multiplier = 2

interpolator_fn = cts.interpolate_solutions_to_times
# model = cts.combined_model(cts.cell_carrying_capacity_1D_PDE, cts.cell_carrying_capacity_model_well_mixed_ODE)
model = cts.combined_model(cts.perfect_absorb_idealized_1D_PDE, cts.perfect_absorb_idealized_well_mixed)


def get_config(config_file):
    config = ConfigInterpreter(config_file,
                               fu.get_particles_per_cell,
                               fu.get_standard_error_of_mean_all_samples,
                               fu.remove_outliers_and_control_median_workflow)
    return config


def get_fitter(config):
    # todo: add changes from config, other things
    mean_sq = lambda rate, real, est: mean_squared_error(real, est)
    max_rate = cts.get_estimated_max_rate(config)
    fitter = ParameterFitter(solution_interpolator=interpolator_fn,
                             rate_scale=1e-9,
                             objective_fn=bounded_objective_fn(objective_fn=mean_sq,
                                                                     min_rate=0,
                                                                     max_rate=max_rate,
                                                                     max_error=cts.max_error),
                             get_min_config_fn=lambda c: min_config_subtract_dispersions(c,
                                                                                               dispersion_multiplier),
                             get_max_config_fn=lambda c: max_config_adding_dispersions(c, dispersion_multiplier),
                             get_values_fn=lambda c: c.associated_per_cell_means)
    return fitter


def plot_real_and_estimated(config: ConfigInterpreter, config_to_model_fn, best_rate: float, min_rate: float, max_rate: float):
    best_model_vals = interpolator_fn(config_to_model_fn, config, best_rate)
    min_model_vals = interpolator_fn(config_to_model_fn, config, min_rate)
    max_model_vals = interpolator_fn(config_to_model_fn, config, max_rate)

    capacity = cts.max_capacity_from_last_derivative_compared_to_max_derivative(config)

    plt.errorbar(config.times, config.associated_per_cell_means, yerr=config.all_associated_per_cell_dispersions, fmt='o-', label='True')
    plt.plot(config.times, best_model_vals, 'g-', label='Model (CPAA={:.2e} , cap={:.1f})'.format(best_rate, capacity))
    plt.plot(config.times, min_model_vals, 'g--', label='Model bounds')
    plt.plot(config.times, max_model_vals, 'g--')

    plt.xlabel('Time (min)')
    plt.ylabel('Particles per cell')
    plt.title('{} incubation with {}'.format(config.experimental_and_cell_detail.particle.name,
                                             config.experimental_and_cell_detail.cell_info.cell_line))
    plt.legend(loc='best')


if __name__ == '__main__':
    # set_log_level(WARNING)
    if len(sys.argv) >= 2:
        config_files = sys.argv[1:]
        for config_file in config_files:
            try:
                config = get_config(config_file)
                fitter = get_fitter(config)
                start_time = time.time()
                rate, error = fitter.get_best_fit_parameter_and_error(model, config)
                # min_rate, _ = fitter.get_min_fit_parameter_and_error(model, config)
                # max_rate, _ = fitter.get_max_fit_parameter_and_error(model, config)
                end_time = time.time()
                print('Fit rate for {} is: {}, error: {}, calculated in {:.1f}m'.format(config_file, rate, error,
                                                                                        (end_time - start_time) / 60))
                plot_real_and_estimated(config, model, rate, rate, rate)
                # plot_real_and_estimated(config, rate, min_rate, max_rate)

                # test_rate = 4.52e-11
                # fitter._calculate_objective(model, config, test_rate)
                # plot_real_and_estimated(config, test_rate, test_rate/1.1, test_rate * 1.1)

                plt.show(block=False)
                plt.figure()
            except KeyError as e:
                print('Missing parameter {} in file {}'.format(e, config_file))
                continue
        plt.show()
    else:
        print('Usage: {} <config_files>'.format(sys.argv[0]))
        exit()
