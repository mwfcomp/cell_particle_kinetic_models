import argparse

import fitter.model_comparer
import utility.config_to_solutions as cts
import utility.fitter_utility as fu
from utility.file_operations import get_config_file_list
from utility.fitter_utility import all_oneway_fns, setup_logging

cts.visualize = False
from timeit import default_timer as timer

model_fns = all_oneway_fns

parser = argparse.ArgumentParser(description='Fit a set of experiments to a set of kinetic models')
parser.add_argument('ini_or_lst_file', type=str, help='path to an ini file OR a lst file with one ini config file per line')
parser.add_argument('output_file', type=str, help='file path for the output csv to put results in')
parser.add_argument('--vary_density_instead_of_dispersion', action='store_true',
                    help='calculates max/min rate based on varying the density of the particles rather than dispersion of the underlying data')
parser.add_argument('--show_plots', action='store_true',
                    help='displays plots while running')
parser.add_argument('--processes', type=int, default=1,
                    help='number of processes to spawn')
parser.add_argument('--base_folder', type=str, default=None,
                    help='base folder if specifying ini file')
parser.add_argument('--output_file_per_process', action='store_true',
                    help='create a separate output file for each process based on PID, use if running in parallel'
                         ' independent of python')

parser.add_argument('--models', type=str, default='all_oneway', choices=['all_oneway'] + list(all_oneway_fns.keys()))

parser.add_argument('--skip_mesh_refinement', action='store_true',
                    help='skips mesh refinement. Useful for debugging/quick runs')
parser.add_argument('--skip_find_bounds', action='store_true',
                    help='Skips finding upper and lower bounds for model. Useful for debugging/quick runs')
parser.add_argument('--show_solution_progress', action='store_true',
                    help='Shows PDE solving as it happens. Useful for debugging')


def get_model_fns(models_name: str):
    if models_name == 'all_oneway':
        return all_oneway_fns
    else:
        return dict([(models_name, all_oneway_fns[models_name])])


if __name__ == '__main__':
    start = timer()
    setup_logging()


    args = parser.parse_args()
    fitter.model_comparer.fit_max_min = not args.skip_find_bounds
    fitter.model_comparer.do_mesh_refinement = not args.skip_mesh_refinement
    cts.visualize = args.show_solution_progress

    fitter.model_comparer.output_file_per_process = args.output_file_per_process
    config_files = get_config_file_list(args.ini_or_lst_file, base_folder=args.base_folder)

    if args.vary_density_instead_of_dispersion:
        fitter_fns = {'density_varied': fu.get_bounded_fitter_density_varied}
    else:
        fitter_fns = {'get_bounded_fitter': fu.get_bounded_fitter_dispersion_varied}

    configs = [fu.get_config(config_file) for config_file in config_files]

    fitter.model_comparer.plot_fn = fu.plot_and_save_real_and_estimated
    fu.show_plots = args.show_plots

    fitter.model_comparer.write_model_fits_and_errors(configs=configs,
                                                      output_file_name=args.output_file,
                                                      desc_to_model_fn_dict=get_model_fns(args.models),
                                                      desc_to_fitter_fn_dict=fitter_fns,
                                                      processes=args.processes)
    end = timer()
    print("Elapsed time: {}s".format(end - start))




