from multiprocessing import Pool, TimeoutError
import time
import os

print_val=5

def f(x):
    return x*x

def time_fn(x, fn):
    print_val = os.getpid()
    time.sleep(1)
    print('{}_{}_{}'.format(fn(x), print_val, os.getpid()))


if __name__ == '__main__':
    pool = Pool(processes=2)              # start 4 worker processes
    #
    # # print "[0, 1, 4,..., 81]"
    # print(pool.map(f, range(10)))
    #
    # # print same numbers in arbitrary order
    # for i in pool.imap_unordered(f, range(10)):
    #     print(i)
    #
    # # evaluate "f(20)" asynchronously
    # res = pool.apply_async(f, (20,))      # runs in *only* one process
    # print(res.get(timeout=1))              # prints "400"

    # evaluate "os.getpid()" asynchronously
    # res = pool.apply_async(os.getpid, ()) # runs in *only* one process
    # print(res.get(timeout=1))             # prints the PID of that process
    print('Start pool')
    # res1 = pool.apply_async(time_fn, (1, )) # runs in *only* one process
    # res2 = pool.apply_async(time_fn, (2, )) # runs in *only* one process
    # res3 = pool.apply_async(time_fn, (3, )) # runs in *only* one process
    # res3 = pool.apply_async(time_fn, (4, )) # runs in *only* one process
    # time.sleep(10)
    #
    # multiple_results = [pool.apply_async(time_fn, (i, )) for i in range(9)]
    # x = [res.get() for res in multiple_results]
    # print(x)

    def add3(x):
        return x+3

    #

    print('Start')
    reses = []
    for i in range(10):
        print('Starting {}'.format(i))
        reses.append(pool.apply_async(time_fn, args=(i, add3)))
        # res.get()
        # time.sleep(0.1)

    pool.close()
    pool.join()


    # [res.get() for res in reses]
    # print(res.get())

    #
    # launching multiple evaluations asynchronously *may* use more processes
    # multiple_results = [pool.apply_async(os.getpid, ()) for i in range(4)]
    # print([res.get(timeout=1) for res in multiple_results])
    #
    # # make a single worker sleep for 10 secs
    # res = pool.apply_async(time.sleep, (10,))
    # try:
    #     print(res.get(timeout=1))
    # except TimeoutError:
    #     print("We lacked patience and got a multiprocessing.TimeoutError")
    #
    # # launching multiple evaluations asynchronously *may* use more processes
    # multiple_results = [pool.apply_async(os.getpid, ()) for i in range(4)]
    # print([res.get(timeout=1) for res in multiple_results])
    #
    # # make a single worker sleep for 10 secs
    # res = pool.apply_async(time.sleep, (10,))
    # try:
    #     print(res.get(timeout=1))
    # except TimeoutError:
    #     print("We lacked patience and got a multiprocessing.TimeoutError")
