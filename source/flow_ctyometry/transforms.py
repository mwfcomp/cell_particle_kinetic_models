import math

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib.path import Path
from pandas import DataFrame
from scipy.spatial.distance import euclidean
from sklearn import covariance


class AbstractPlottable:
    def plot(self):
        pass


class AbstractFCSTransform:
    def apply(self, fcs_data: DataFrame):
        pass

    def apply_to_many(self, fcs_datas: [DataFrame]):
        new_datas = [self.apply(d) for d in fcs_datas]
        return new_datas


class ControlAbstractFCSTransfrom(AbstractFCSTransform):
    def setup_from_control(self, fcs_data: DataFrame):
        pass


class ChannelsTransform(AbstractFCSTransform):
    def __init__(self, channels, transform_fn):
        self.channels = channels
        self.transform_fn = transform_fn

    def apply(self, fcs_data: DataFrame):
        transformed = fcs_data.copy()
        for channel in self.channels:
            if channel in transformed.columns:
                transformed.loc[:, channel] = self.transform_fn(transformed.loc[:, channel])
        return transformed


class FilterOutliers(ControlAbstractFCSTransfrom):
    def __init__(self, channels=None, outlier_portion=.1):
        self.channels = channels
        self.outlier_portion = outlier_portion
        self.gate_clf = None
        self.short_pts, self.long_pts = None, None

    def setup_from_control(self, control_data):
        self.gate_clf = covariance.EllipticEnvelope(contamination=self.outlier_portion)
        vals = control_data[self.channels][:].values
        self.gate_clf.fit(vals)
        self.short_pts, self.long_pts = self._get_key_ellipse_pts()

    def apply(self, fcs_data: DataFrame):
        inclusive_cols = fcs_data[self.channels].values
        gated = fcs_data[self.gate_clf.predict(inclusive_cols) == 1]
        return gated

    def get_ellipse(self):
        evals, evecs = np.linalg.eig(self.gate_clf.covariance_)
        idx = evals.argsort()
        evals = evals[idx]
        evecs = evecs[idx]

        angle = math.atan(evecs[1][1] / evecs[1][0])

        height = 2 * math.sqrt(abs(self.gate_clf.offset_ * evals[0]))
        width = 2 * math.sqrt(abs(self.gate_clf.offset_ * evals[1]))
        ang_deg = -1 * math.degrees(angle)
        # height set up to be the shorter of the two axis
        ellipse = Ellipse(xy=self.gate_clf.location_, width=width, height=height, alpha=.5, color='red',
                          angle=ang_deg)
        return ellipse

    def _get_key_ellipse_pts(self):
        e = self.get_ellipse()
        a = math.radians(e.angle)
        x_long_dist = math.cos(a) * e.width / 2
        y_long_dist = math.sin(a) * e.width / 2
        long_dists = np.array([x_long_dist, y_long_dist])
        long_pts = np.vstack((e.center - long_dists,
                              e.center + long_dists))

        rev_a = math.radians(90 - e.angle)
        x_short_dist = math.cos(rev_a) * e.height / 2
        y_short_dist = -1 * math.sin(rev_a) * e.height / 2
        short_dists = np.array([x_short_dist, y_short_dist])
        short_pts = np.vstack((e.center - short_dists,
                               e.center + short_dists))

        return short_pts, long_pts


class FilterNormal(FilterOutliers, AbstractPlottable):
    def __init__(self, normal_channel='488 Org(Peak)', detect_channel='638 Red(Peak)',
                 outlier_portion=.1, scale_right=.2, scale_updown=.2):
        channels = [detect_channel, normal_channel]
        super().__init__(channels, outlier_portion)
        self.scale_right = scale_right
        self.scale_updown = scale_updown
        self.gate_pts = None
        self.key_ellipse_pts = None

    def setup_from_control(self, control_data):
        super().setup_from_control(control_data)
        self.gate_pts = self._get_gate_pts2(control_data)

    def apply(self, fcs_data: DataFrame):
        cols = fcs_data[self.channels].values
        included = np.array([self._in_gate(pt) for pt in cols]) == 1
        gated = fcs_data[np.array(included) == 1]
        return gated

    def plot(self):
        ax = plt.gca()
        gate_pts = self.gate_pts
        xlim = max(ax.get_xlim()[1], gate_pts[1][0])
        far_bottom = np.array([xlim, gate_pts[0][1]])
        far_top = np.array([xlim, gate_pts[1][1]])
        all_gate = np.vstack((far_bottom, gate_pts[0], gate_pts[1], far_top))
        plt.plot(all_gate[:, 0], all_gate[:, 1], color='black', linewidth=3)

    def _get_gate_pts(self, control_data):
        xy = control_data[self.channels][:].values

        # xy = sample.data[two_channels[0:2]][:].values

        def lin_space_beyond_limits(ar):
            a_min = ar.min() / 2
            a_max = ar.max() * 2
            return np.linspace(a_min, a_max, 1000)

        xx, yy = np.meshgrid(lin_space_beyond_limits(xy[:, 0]), lin_space_beyond_limits(xy[:, 1]))
        grid_xy = np.c_[xx.ravel(), yy.ravel()]
        z = self.gate_clf.decision_function(grid_xy)
        xy_in = grid_xy[(z > 0) & (z < 0.1)]
        # plt.scatter(xy_in[:, 0], xy_in[:, 1], color='blue')
        loc = self.gate_clf.location_
        z = np.array([euclidean(xy_pt, loc) for xy_pt in xy_in])

        idx = z.argsort()
        xy_in = xy_in[idx]
        top_pt = xy_in[xy_in[:, 1] > loc[1]][-1]
        bottom_pt = xy_in[xy_in[:, 1] < loc[1]][-1]
        right_mid_pt = xy_in[xy_in[:, 0] > loc[0]][0]
        left_mid_pt = xy_in[xy_in[:, 0] < loc[0]][0]
        # key_pts = np.vstack((top_pt, bottom_pt, right_mid_pt, left_mid_pt))
        # slope = stats.linregress(np.vstack((right_mid_pt, top_pt)).T)[0]
        # slope = stats.linregress(np.vstack((bottom_pt, top_pt)).T)
        slope = (top_pt[1] - bottom_pt[1]) / (top_pt[0] - bottom_pt[0]) * 1.0

        # right_most_pt = np.array(sorted(xy_in, key=lambda pt: pt[0])[-1])
        mid_pt = right_mid_pt
        # mid_pt = right_most_pt
        mid_pt[0] *= (1.0 + self.scale_right)
        max_y = xy_in[:, 1].max()
        min_y = xy_in[:, 1].min()
        scale = min_y * self.scale_updown
        max_y += scale
        min_y -= scale

        bottom_gate_pt = np.array([mid_pt[0] - ((mid_pt[1] - min_y) / slope), min_y])
        top_gate_pt = np.array([(max_y - min_y) / slope + bottom_gate_pt[0], max_y])
        gate_pts = np.vstack((bottom_gate_pt, top_gate_pt))
        #
        # plt.plot(key_pts[:, 0], key_pts[:, 1], color='red')
        # plt.plot(gate_pts[:, 0], gate_pts[:, 1], color='red')
        return gate_pts

    def _get_gate_pts2(self, control_data):
        e = self.get_ellipse()
        bottom_ellipse_pt = self.long_pts[0]
        top_ellipse_pt = self.long_pts[1]
        right_mid_pt = self.short_pts[1]
        long_dists = top_ellipse_pt - e.center
        short_dists = right_mid_pt - e.center

        top_pt = top_ellipse_pt + (long_dists * self.scale_updown)
        bottom_pt = bottom_ellipse_pt - (long_dists * self.scale_updown)

        # key_pts = np.vstack((top_pt, bottom_pt, right_mid_pt, left_mid_pt))
        # slope = stats.linregress(np.vstack((right_mid_pt, top_pt)).T)[0]
        # slope = stats.linregress(np.vstack((bottom_pt, top_pt)).T)
        slope = (top_pt[1] - bottom_pt[1]) / (top_pt[0] - bottom_pt[0])

        # right_most_pt = np.array(sorted(xy_in, key=lambda pt: pt[0])[-1])
        mid_pt = right_mid_pt + np.array([np.linalg.norm(short_dists) * self.scale_right, 0])

        bottom_gate_x = mid_pt[0] - (mid_pt[1] - bottom_pt[1]) / slope
        bottom_gate_y = bottom_pt[1]

        top_gate_x = mid_pt[0] + (top_pt[1] - mid_pt[1]) / slope
        top_gate_y = top_pt[1]

        bottom_gate_pt = np.array([bottom_gate_x, bottom_gate_y])
        top_gate_pt = np.array([top_gate_x, top_gate_y])
        gate_pts = np.vstack((bottom_gate_pt, top_gate_pt))
        #
        # plt.plot(key_pts[:, 0], key_pts[:, 1], color='red')
        # plt.plot(gate_pts[:, 0], gate_pts[:, 1], color='red')
        return gate_pts

    def _in_gate(self, pt):
        # gates define a trapezoid extending into infinity on the right, check if an arbitrary point is in this space

        # check if point is in right infinitely sized rectangle part
        ymin = self.gate_pts[:, 1].min()
        ymax = self.gate_pts[:, 1].max()
        rect_x = self.gate_pts[:, 0].max()
        if pt[0] > rect_x and ymin < pt[1] < ymax:
            return True

        # check if point is in left triangle part
        triangle_pts = np.vstack((self.gate_pts, [rect_x, ymin]))
        triangle = Path(triangle_pts)
        return triangle.contains_point(pt)


class LoglikeTransform(ChannelsTransform):
    def __init__(self, channels):
        def transform_fn(x):
            return np.log(x + 1)

        super().__init__(channels, transform_fn)


class UndoLoglikeTransform(ChannelsTransform):
    def __init__(self, channels):
        def transform_fn(x):
            return np.exp(x) - 1

        super().__init__(channels, transform_fn)


class ArcSinHTransform(ChannelsTransform):
    def __init__(self, channels):
        def transform_fn(x):
            return np.arcsinh(x)

        super().__init__(channels, transform_fn)


class UndoArcSinHTransform(ChannelsTransform):
    def __init__(self, channels):
        def transform_fn(x):
            return np.sinh(x)

        super().__init__(channels, transform_fn)


class SinHTransform(ChannelsTransform):
    def __init__(self, channels):
        def transform_fn(x):
            return np.sinh(x)

        super().__init__(channels, transform_fn)


class FilterOutOfRange(AbstractFCSTransform):
    def __init__(self, channel, min_val, max_val):
        self.channel = channel
        self.min_val = min_val
        self.max_val = max_val

    def apply(self, fcs_data: DataFrame):
        below_max = fcs_data[self.channel][:].values < self.max_val
        above_min = fcs_data[self.channel][:].values > self.min_val
        gated = fcs_data[above_min & below_max]
        return gated


class AddToChannels(ChannelsTransform):
    def __init__(self, channels, val):
        self.val = val

        def transform_fn(x):
            return x + self.val

        super().__init__(channels, transform_fn)


class MultiplyChannelsBy(ChannelsTransform):
    def __init__(self, channels, val):
        self.val = val

        def transform_fn(x):
            return x * self.val

        super().__init__(channels, transform_fn)


class RemoveControlMean(AddToChannels, ControlAbstractFCSTransfrom):
    def __init__(self, channel):
        super().__init__([channel], None)

    def setup_from_control(self, control_data: DataFrame):
        self.val = -1 * np.mean(control_data[self.channels][:].values)


class RemoveControlMedian(AddToChannels, ControlAbstractFCSTransfrom):
    def __init__(self, channel):
        super().__init__([channel], None)

    def setup_from_control(self, control_data: DataFrame):
        self.val = -1 * np.median(control_data[self.channels][:].values)


class DivideByControlMedian(MultiplyChannelsBy, ControlAbstractFCSTransfrom):
    def __init__(self, channel):
        super().__init__([channel], None)

    def setup_from_control(self, control_data: DataFrame):
        self.val = 1 / np.median(control_data[self.channels][:].values)
