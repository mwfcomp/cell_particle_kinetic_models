import json
import os
from configparser import ConfigParser
from os import path, listdir

import parse
from pandas import DataFrame

from flow_ctyometry.fcs_file_fixer import convert_and_process_sample
from flow_ctyometry.transforms import AbstractFCSTransform
from itertools import chain
from typing import Generator, Iterable
import math


class ChannelInfo:
    def __init__(self, detect_channel: str, normal_channel: str):
        self.detect_channel = detect_channel
        self.normal_channel = normal_channel

    def all_channels(self):
        return [self.detect_channel, self.normal_channel]


class EnhancedChannelInfo(ChannelInfo):
    def __init__(self, detect_channel: str, normal_channel: str, two_normal_channels: [str]):
        if len(two_normal_channels) != 2:
            raise ValueError('Two Normal channels must be exactly two channels')
        self.two_normal_channels = two_normal_channels
        super().__init__(detect_channel, normal_channel)

    def all_channels(self):
        return super().all_channels() + self.two_normal_channels


def times_vals_from_pre_specified_ini(config: ConfigParser) -> (Iterable[int], Iterable[float]):
    time_pt_sections = [s for s in config.sections() if s.startswith('time_point')]
    times = []
    all_vals = []
    for section in time_pt_sections:
        time = float(config[section]['time_in_mins'])
        vals = json.loads(config[section]['particles_per_cell'])
        times.append(time)
        all_vals.append(vals)
    return times, all_vals


class FullIncubationExperiment:
    def __init__(self, experiment_data: [DataFrame],
                 blank_controls: [DataFrame],
                 single_signal_controls: [DataFrame],
                 time: float, signal_correction: float,
                 total_time_pts: int, channel_info: ChannelInfo,
                 particle: str, cell_line: str, control_hash: str):
        self.experiment_data = experiment_data
        self.blank_controls = blank_controls
        self.single_signal_controls = single_signal_controls
        self.channel_info = channel_info
        self.time = time
        self.total_time_pts = total_time_pts
        self.signal_correction = signal_correction
        self.particle = particle
        self.cell_line = cell_line
        self.control_hash = control_hash

    @staticmethod
    def incubation_experiment_from_file_names(experiment_files: [str], blank_ctrl_files: [str],
                                              signal_ctrl_files: [str],
                                              time: float, signal_correction: float, total_time_pts: int,
                                              channel_info: ChannelInfo, particle: str, cell_line: str):
        experiment_data = [convert_and_process_sample(file) for file in experiment_files]
        blank_ctrl_data = [convert_and_process_sample(file) for file in blank_ctrl_files]
        signal_ctrl_data = [convert_and_process_sample(file) for file in signal_ctrl_files]
        ctrl_hash = "".join(blank_ctrl_files + signal_ctrl_files)

        return FullIncubationExperiment(experiment_data=experiment_data,
                                        blank_controls=blank_ctrl_data,
                                        single_signal_controls=signal_ctrl_data,
                                        time=time,
                                        total_time_pts=total_time_pts,
                                        signal_correction=signal_correction,
                                        channel_info=channel_info,
                                        particle=particle,
                                        cell_line=cell_line,
                                        control_hash=ctrl_hash)

    @staticmethod
    def get_data(channel_info: ChannelInfo, base_folder: str, file_names_or_values_json: str):
        if FullIncubationExperiment.is_file_names(file_names_or_values_json):
            full_file_names = FullIncubationExperiment.full_file_names_from_json(base_folder, file_names_or_values_json)
            frames = [convert_and_process_sample(file) for file in full_file_names]
        else:
            values = json.loads(file_names_or_values_json)
            frames = [FullIncubationExperiment.create_data_frame(channel_info, v) for v in values]
        return frames

    @staticmethod
    def create_data_frame(channel_info, value):
        data_map = {channel_info.detect_channel : [value]}
        for c in channel_info.all_channels():
            if c != channel_info.detect_channel:
                data_map[c] = [0.0]

        frame = DataFrame(data_map)
        return frame

    @staticmethod
    def is_file_names(json_str):
        data_details = json.loads(json_str)
        return type(data_details[0]) is str

    @staticmethod
    def full_file_names_from_json(base_folder: str, file_names_json: str):
        file_names = json.loads(file_names_json)
        return [os.path.join(base_folder, file) for file in file_names]

    @staticmethod
    def get_channel_info(normal_channels_json: str, detect_channels_json: str):
        normal_channels = json.loads(normal_channels_json)
        detect_channels = json.loads(detect_channels_json)
        if len(normal_channels) == 1 and len(detect_channels) == 1:
            return ChannelInfo(normal_channel=normal_channels[0], detect_channel=detect_channels[0])
        elif len(normal_channels) == 3 and len(detect_channels) == 1:
            return EnhancedChannelInfo(normal_channel=normal_channels[0],
                                       two_normal_channels=normal_channels[1:],
                                       detect_channel=detect_channels[0])
        else:
            raise ValueError('Unsupported number of channels - 1 detect channel and 1 or 3 normal channels')

    @staticmethod
    def incubation_generator_from_ini(config_file) -> Generator['FullIncubationExperiment', None, None]:
        config = ConfigParser()
        config.read(config_file)
        time_pt_sections = [s for s in config.sections() if s.startswith('time_point')]

        for section in time_pt_sections:
            section = config[section]
            base_folder = section['base_folder']
            if not base_folder.startswith('/'):
                base_folder = os.path.join(os.path.split(config_file)[0], base_folder)
                section['base_folder'] = base_folder
            channel_info = FullIncubationExperiment.get_channel_info(section['normal_channels'],
                                                                     section['detect_channels'])
            experiments = FullIncubationExperiment.get_data(channel_info, base_folder, section['experiment_data'])
            blank_controls = FullIncubationExperiment.get_data(channel_info, base_folder, section['background_controls'])
            single_signal_controls = FullIncubationExperiment.get_data(channel_info, base_folder,
                                                                       section['particle_signal_controls'])

            if FullIncubationExperiment.needs_pmt_voltage_convert(config):
                get_files = lambda s: FullIncubationExperiment.full_file_names_from_json(base_folder, s)
                FullIncubationExperiment.scale_to_pmt(config, section, get_files(section['experiment_data']), experiments)
                FullIncubationExperiment.scale_to_pmt(config, section, get_files(section['background_controls']), blank_controls)

            yield FullIncubationExperiment(experiment_data=experiments,
                                           blank_controls=blank_controls,
                                           single_signal_controls=single_signal_controls,
                                           time=float(section['time_in_mins']),
                                           total_time_pts=len(time_pt_sections),
                                           signal_correction=float(section['signal_correction']),
                                           channel_info=channel_info,
                                           particle=section['particle_name'],
                                           cell_line=section['cell_line_name'],
                                           control_hash=section['background_controls'] + section[
                                               'particle_signal_controls'])

    @staticmethod
    def needs_pmt_voltage_convert(config: ConfigParser):
        return 'pmt_exponential_response' in config['model']

    @staticmethod
    def scale_to_pmt(config: ConfigParser, section, fcs_files: [str], data: [DataFrame]):
        if len(fcs_files) != len(data):
            raise IndexError('fcs files and data must have same length')
        exp_scale = config['model'].getfloat('pmt_exponential_response')
        channel = FullIncubationExperiment.get_channel_info(section['normal_channels'], section['detect_channels']).detect_channel
        base_folder = section['base_folder']
        # ASSUMPTION: ALL PARTICLE SIGNAL CONTROLS USING SAME PMT VOLTAGE
        signal_control = FullIncubationExperiment.full_file_names_from_json(base_folder, section['particle_signal_controls'])[0]

        base_voltage = FullIncubationExperiment.channel_pmt_voltage(channel, signal_control)

        for idx, frame in enumerate(data):
            scale_voltage = FullIncubationExperiment.channel_pmt_voltage(channel, fcs_files[idx])
            scaling = math.exp(exp_scale * base_voltage) / math.exp(exp_scale * scale_voltage)
            frame.loc[:, channel] = frame.loc[:, channel] * scaling

    @staticmethod
    def channel_pmt_voltage(channel: str, data_file: str):
        channel_start = channel.split('(')[0]
        params = []
        voltages = []
        with open(data_file, encoding='latin-1') as f:
            for line in f:
                if line.startswith('PARAM'):
                    params=line.split(';')
                if line.startswith('PMTVOLTAGES'):
                    voltages = line.split(';')
                    break
        idx = params.index(channel_start)
        return float(voltages[idx])

    @staticmethod
    def get_all_particle_signal_data(config_file):
        exp_gen = FullIncubationExperiment.incubation_generator_from_ini(config_file)
        particle_signals = []
        for exp in exp_gen:
            particle_signals += exp.single_signal_controls
        return particle_signals

    @staticmethod
    def multi_incubation_generators_from_inis(config_files):
        for file in config_files:
            yield FullIncubationExperiment.incubation_generator_from_ini(file)


class IncubationExperimentsLoader:
    def __init__(self, control_folder: str, channel_info: ChannelInfo,
                 control_str='control', default_time_hrs=None, default_particle=None,
                 default_cell_line=None):
        self.control_folder = control_folder
        self.control_str = control_str
        self.default_time_hrs = default_time_hrs
        self.default_particle = default_particle
        self.default_cell_line = default_cell_line
        self.channel_info = channel_info

    def has_default_particle(self):
        return self.default_particle is not None

    def has_default_time(self):
        return self.default_time_hrs is not None

    def has_default_cell_line(self):
        return self.default_cell_line is not None

    # this is ugly, but regexes are uglier...
    def cell_particle_time_replicate_from_file_name(self, filename: str):
        # handle absolute and relative paths
        filename = path.basename(filename)

        # ignore controls
        if self.control_str.lower() in filename.lower():
            return None, None, None, None

        replicate, parse_str = IncubationExperimentsLoader.find_and_process_replicate(filename)
        r = parse.parse('{cell} - {particle}{time:g} hr', parse_str)
        if r is None:
            r = parse.parse('{cell} - {particle}{time:g}hr', parse_str)
        if r is None:
            r = parse.parse('{cell} - {particle}{time:g} h', parse_str)
        if r is None and self.has_default_particle():
            r = parse.parse('{cell} {time:g}h', parse_str)
            if r is not None:
                r.named['particle'] = self.default_particle
        if r is None and self.has_default_particle():
            r = parse.parse('{cell} {time:g}hr', parse_str)
            if r is not None:
                r.named['particle'] = self.default_particle
        if r is None and self.has_default_particle() and self.has_default_cell_line():
            r = parse.parse('{time:g}h', parse_str)
            if r is not None:
                r.named['particle'] = self.default_particle
                r.named['cell'] = self.default_cell_line
        if r is None and self.has_default_time():
            r = parse.parse('{cell} - {particle}', parse_str)
            if r is not None:
                r.named['time'] = self.default_time_hrs
        if r is None and self.has_default_cell_line():
            r = parse.parse('{particle} {time:g}hr', parse_str)
            if r is not None:
                r.named['cell'] = self.default_cell_line

        if r is not None:
            return r['cell'].strip(), r['particle'].strip(), r['time'] * 60, replicate
        return None, None, None, None

    def incubation_file_lists_from_folder(self, folder: str) -> [[str]]:
        def relevant_file(full_path):
            return path.isfile(full_path) and full_path.endswith('fcs')

        series_dict = dict()

        for p in listdir(folder):
            full_path = path.join(folder, p)
            if relevant_file(full_path):
                cell, particle, _, _ = self.cell_particle_time_replicate_from_file_name(p)
                if cell is not None:
                    controls = self.get_control_data_files(cell)
                    if len(controls) != 0:
                        key = (cell.lower(), particle.lower())
                        series_dict.setdefault(key, []).append(full_path)

        return list(series_dict.values())

    def get_time_dict(self, files: [str]):
        time_dict = dict()
        for file in files:
            _, _, time, _ = self.cell_particle_time_replicate_from_file_name(file)
            time_dict.setdefault(time, []).append(file)
        return time_dict

    def get_exp_generator(self, files: [str]):
        time_dict = self.get_time_dict(files)
        cell, particle, _, _ = self.cell_particle_time_replicate_from_file_name(files[0])

        for time in sorted(time_dict.keys()):
            time_files = time_dict[time]
            blank_ctrls = self.get_control_data_files(cell)
            exp = FullIncubationExperiment.incubation_experiment_from_file_names(experiment_files=time_files,
                                                                                 blank_ctrl_files=blank_ctrls,
                                                                                 signal_ctrl_files=[], time=time,
                                                                                 signal_correction=1,
                                                                                 total_time_pts=len(
                                                                                     list(time_dict.keys())),
                                                                                 channel_info=self.channel_info,
                                                                                 particle=particle, cell_line=cell)
            yield exp

    def get_incubation_generators_from_folder(self, folder: str) -> [[FullIncubationExperiment]]:
        file_lists = self.incubation_file_lists_from_folder(folder)
        for files in file_lists:
            yield self.get_exp_generator(files)

    def experiment_generators_from_all_folders(self, base_folder: str):
        all_files = [path.join(base_folder, p) for p in listdir(base_folder)]
        all_folders = reversed(sorted([f for f in all_files if path.isdir(f)]))
        for folder in all_folders:
            print('generating from folder {}'.format(folder))
            for exp_gen in self.get_incubation_generators_from_folder(folder):
                yield exp_gen

    def get_control_data_files(self, cell_line: str) -> str:
        matching_path = None
        for p in listdir(self.control_folder):
            low_path = p.lower()
            if low_path.startswith(cell_line.lower() + ' ') and low_path.endswith('control.fcs'):
                matching_path = p
                break

        if matching_path is None:
            return []
        full_path = path.join(self.control_folder, matching_path)
        return [full_path]

    @staticmethod
    def find_and_process_replicate(filename: str):
        result = parse.search('({:d}).fcs', filename)
        if result is not None:
            replicate = result[0]
            paren_idx = filename.rfind('(')
            new_str = filename[0:paren_idx]
            return replicate, new_str.strip()

        result = parse.search('_{:d}.fcs', filename)
        if result is not None:
            replicate = result[0]
            underscore_idx = filename.rfind('_')
            new_str = filename[0:underscore_idx]
            return replicate, new_str.strip()

        return 0, filename[0:-4].strip()

    @staticmethod
    def mins_from_time_str(time_str: str):
        try:  # sometimes parse fails (it should return None if the parse fails...
            result = parse.parse('{:d}{}', time_str)
        except:
            return -1
        if result is None:
            return -1
        amt, unit = result
        unit = unit.lower()
        if unit in ['m', 'min', 'mins', 'minutes']:
            return amt
        if unit in ['hr', 'hours', 'hour', 'h']:
            return amt * 60
        if unit in ['d', 'days', 'day']:
            return amt * 60 * 24
        return -1
