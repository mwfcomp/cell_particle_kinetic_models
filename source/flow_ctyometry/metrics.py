import numpy as np
import pandas as pd
from flow_ctyometry.experiment import FullIncubationExperiment
from flow_ctyometry.transforms import AbstractFCSTransform, MultiplyChannelsBy
from enum import Enum
import numpy as np
from scipy.stats import sem
from statsmodels import robust
from typing import Generator

class ControlType(Enum):
    no_control = 1
    blank_control = 2
    signal_control = 3


class ControlTransformWorkflow:
    def __init__(self, transforms_and_control_types: [(AbstractFCSTransform, ControlType)]):
        self.transforms_and_controls = transforms_and_control_types

    def all_transforms(self):
        return [transform for (transform, controls) in self.transforms_and_controls]

    def setup_from_controls(self, blank_control, signal_control):
        control_types = [tc[1] for tc in self.transforms_and_controls]
        if blank_control is None and ControlType.blank_control in control_types:
            raise ValueError('No blank control provided, needed for transform!')
        if signal_control is None and ControlType.signal_control in control_types:
            raise ValueError('No signal control provided, needed for transform!')

        # blank_control = pd.concat(exp.blank_controls)
        # signal_control = pd.concat(exp.single_signal_controls)
        for transform, control_type in self.transforms_and_controls:
            if control_type == ControlType.blank_control:
                transform.setup_from_control(blank_control)
            elif control_type == ControlType.signal_control:
                transform.setup_from_control(signal_control)
            if blank_control is not None:
                blank_control = transform.apply(blank_control)
            if signal_control is not None:
                signal_control = transform.apply(signal_control)
        return blank_control, signal_control

    def apply_transforms(self, frames: [pd.DataFrame]):
        for transform in self.all_transforms():
            frames = [transform.apply(f) for f in frames]
        return frames


class AbstractMetric:
    name = 'Metric'

    def calculate(self, exp: FullIncubationExperiment) -> [float]:
        raise NotImplementedError


class PercentCellAssocCombineControl(AbstractMetric):
    name = '% Cell Association'

    def __init__(self, pre_count_workflow: ControlTransformWorkflow, post_count_workflow: ControlTransformWorkflow):
        self.pre_count_workflow = pre_count_workflow
        self.post_count_workflow = post_count_workflow

    def calculate(self, exp: FullIncubationExperiment):
        pre_data = self.pre_count_workflow.apply_transforms(exp.experiment_data)
        pre_total = np.array([p.shape[0] for p in pre_data])
        post_data = self.post_count_workflow.apply_transforms(pre_data)
        post_total = np.array([p.shape[0] for p in post_data])
        return post_total / pre_total * 100


class MedianSignalPerCell(AbstractMetric):
    name = 'Median Signal Per Cell'

    def __init__(self, workflow: ControlTransformWorkflow):
        self.workflow = workflow

    def calculate_all_signal_per_cell(self, exp: FullIncubationExperiment):
        chn = exp.channel_info.detect_channel
        frames = MultiplyChannelsBy([chn], exp.signal_correction).apply_to_many(exp.experiment_data)
        frames = self.workflow.apply_transforms(frames)
        return np.array([frame[chn][:].values for frame in frames])

    def calculate(self, exp: FullIncubationExperiment):
        all_signals = self.calculate_all_signal_per_cell(exp)
        medians = np.array([np.median(f) for f in all_signals])
        return medians


# making a discrete metric for this because we want signal control to go through a
# different workflow
class ParticlesPerCell(MedianSignalPerCell):
    name = 'Particles Per Cell'

    def __init__(self, workflow: ControlTransformWorkflow, signal_workflow: ControlTransformWorkflow):
        self.signal_workflow = signal_workflow
        super().__init__(workflow)

    def calculate_all_particles_per_cell(self, exp: FullIncubationExperiment):
        signals = self.signal_workflow.apply_transforms(exp.single_signal_controls)
        meds = [np.median(sig[exp.channel_info.detect_channel][:]) for sig in signals]
        particle_signal = np.mean(meds)
        all_particles_per_cell = super().calculate_all_signal_per_cell(exp) / particle_signal
        return all_particles_per_cell

    def calculate(self, exp: FullIncubationExperiment):
        all_cells = self.calculate_all_particles_per_cell(exp)
        medians = np.array([np.median(f) for f in all_cells])
        return medians


class MedianAbsoluteDeviationOfParticlesPerCell(AbstractMetric):
    name = 'Median Absolute Deviation'

    def __init__(self, initialized_particles_per_cell: ParticlesPerCell):
        self.particles_per_cell = initialized_particles_per_cell

    def calculate(self, exp: FullIncubationExperiment):
        all_ppc = self.particles_per_cell.calculate_all_particles_per_cell(exp)
        channel_data = np.concatenate(all_ppc)
        mads = robust.mad(channel_data)
        return mads


class StandardErrorOfMeanOfParticlesPerCell(AbstractMetric):
    name = 'Standard Error of Mean'

    def __init__(self, initialized_particles_per_cell: ParticlesPerCell):
        self.particles_per_cell = initialized_particles_per_cell

    def calculate(self, exp: FullIncubationExperiment):
        all_ppc = self.particles_per_cell.calculate_all_particles_per_cell(exp)
        channel_data = np.concatenate(all_ppc)
        sems = sem(channel_data)
        return sems

class StandardErrorOfMedianOfParticlesPerCell(StandardErrorOfMeanOfParticlesPerCell):
    name = 'Standard Error of Median'

    def calculate(self, exp: FullIncubationExperiment):
        return 1.2533 * super().calculate(exp)

class StandardDeviationOfParticlesPerCell(AbstractMetric):
    name = 'Standard Deviation of All Samples'

    def __init__(self, initialized_particles_per_cell: ParticlesPerCell):
        self.particles_per_cell = initialized_particles_per_cell

    def calculate(self, exp: FullIncubationExperiment):
        all_ppc = self.particles_per_cell.calculate_all_particles_per_cell(exp)
        # channel_data = all_ppc[-1]
        all_data = np.concatenate(all_ppc)
        # channel_data = np.concatenate([np.array(frame[chn][:].values) for frame in frames])
        std_devs = np.std(all_data)
        return std_devs




def get_time_means_stds(get_metric_from_exp_fn, exp_gen):
    times, all_vals, _ = get_time_vals_devs(get_metric_from_exp_fn, None, exp_gen)
    means = [np.mean(vals) for vals in all_vals]
    stds = [np.std(vals) for vals in all_vals]
    return times, means, stds
