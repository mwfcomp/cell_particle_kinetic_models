import logging
import os
from itertools import chain
from itertools import tee
from math import ceil
from math import floor
from os import path

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
from flow_ctyometry.experiment import FullIncubationExperiment, ChannelInfo
from flow_ctyometry.fcs_file_fixer import convert_and_process_sample
from flow_ctyometry.metrics import ControlTransformWorkflow
from flow_ctyometry.transforms import AbstractFCSTransform, FilterOutliers, FilterNormal, AbstractPlottable, \
    ArcSinHTransform
from pandas import DataFrame
from scipy.stats import gaussian_kde
from utility.flow_utility import ct


class FCSPlotter:
    def __init__(self, x_channel: str, y_channel: str):
        self.x_channel = x_channel
        self.y_channel = y_channel
        self.xlim = None
        self.ylim = None
        self.fast_and_ugly = True
        self.colormap = plt.cm.get_cmap('gnuplot')
        # self.colormap.set_under('white')
        self.scatter_point_size = 3
        self.gate_color = 'black'
        self.scatter_color = 'blue'
        self.hist_color = 'blueviolet'
        self.hist_alpha = 0.5
        self.hist_fit_line_color = 'black'
        self.hist_bins = 50
        self.set_limit = False

    def get_xy(self, data: DataFrame):
        x = data[self.x_channel][:].values
        y = data[self.y_channel][:].values
        return x, y

    def fix_axis_limits(self):
        ax = plt.gca()
        self.xlim = ax.get_xlim()
        self.ylim = ax.get_ylim()

    def _set_fixed_limits(self):
        if self.xlim is not None:
            ax = plt.gca()
            ax.set_xlim(self.xlim)
            ax.set_ylim(self.ylim)

    def _safe_to_kde_fit(self, vals):
        if vals.shape[0] > 10:  # gaussian kde needs certain amount of points to work, 10 seems safe
            return True
        return False

    def scatter_heatmap(self, data: DataFrame):
        x, y = self.get_xy(data)
        xy = np.vstack([x, y])
        if self.fast_and_ugly and len(x) > 0:
            plt.hexbin(x, y, cmap=self.colormap, mincnt=1)
        else:
            if len(x) < 100:
                z = np.ones(x.shape)
            else:
                z = gaussian_kde(xy)(xy)  # old way of doing it
                bandwidth = 100 / len(x)
                # bandwidth = .001
                # kde = KernelDensity(kernel='epanechnikov').fit(xy.T)
                # kde = KernelDensity(kernel='epanechnikov', bandwidth=bandwidth).fit(xy.T)
                # z = kde.score_samples(xy.T)
                # if self.fast_and_ugly:
                #     plt.hexbin(x, y)
                #     # bandwidth = 100 / len(x)
                #     # kde = KernelDensity(kernel='linear', bandwidth=bandwidth).fit(xy.T)
                #     # z = kde.score_samples(xy.T)
                # else:
            idx = z.argsort()
            x, y, z = x[idx], y[idx], z[idx]
            plt.scatter(x, y, c=z, s=self.scatter_point_size, edgecolor='', cmap=self.colormap)
        plt.xlabel(self.x_channel)
        plt.ylabel(self.y_channel)
        if self.set_limit:
            self._set_fixed_limits()

    def add_top_right_label(self, label):

        ax = plt.gca()
        ax.text(1, 0.98, label, verticalalignment='top', horizontalalignment='right', transform=ax.transAxes)

    def histogram_with_kde_fit(self, data):
        vals = data[self.x_channel][:].values
        if len(vals) > 10:
            xrange = np.linspace(vals.min(), vals.max())
            plt.hist(vals, bins=self.hist_bins, normed=True, color=self.hist_color, alpha=self.hist_alpha)
            if self._safe_to_kde_fit(vals.T):
                kde = gaussian_kde(vals)
                plt.plot(xrange, kde(xrange.T), color='black', alpha=0.8, linewidth=2)
            plt.xlabel(self.x_channel)
            plt.ylabel('Normed count')
            if self.set_limit:
                self._set_fixed_limits()

    def get_run_time(self, data):
        scale = 1000
        for col in ['Time']:
            if col in data.columns.values:
                times = data[col][:].values
                total_time_sec = (np.max(times) - np.min(times)) / scale
                return total_time_sec
        return None

    def make_median_samples_time_label(self, data, flow_rate_in_ul_per_sec=None):
        vals = data[self.x_channel][:].values

        label = 'Samples: {}'.format(len(vals))
        total_time_sec = self.get_run_time(data)
        time_label = ''
        if total_time_sec is not None:
            time_label = 'Time: {:.0f}s'.format(total_time_sec)
        # time_label = max([get_time_label('Time', 1000), get_time_label('TIME', )])
        label += '\n' + time_label
        if flow_rate_in_ul_per_sec is not None and total_time_sec is not None:
            concentration = len(vals) / flow_rate_in_ul_per_sec / total_time_sec
            conc_label = '\nConc:{:.1f}'.format(concentration) + r'$\frac{p}{ul}$'
            label += conc_label
        label += '\n' + r'$\tilde{x}$' + ': {:.1f}'.format(np.median(vals))
        self.add_top_right_label(label)

    def plot_classifier_contour(self, outlier_filter: FilterOutliers):
        clf = outlier_filter.gate_clf
        ax = plt.gca()
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()

        xx, yy = np.meshgrid(np.linspace(xlim[0], xlim[1], 1000), np.linspace(ylim[0], ylim[1], 1000))
        z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
        z = z.reshape(xx.shape)
        plt.contour(xx, yy, z, levels=[0], linewidths=2, colors=self.gate_color)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

    def plot_included_ellipse(self, outlier_filter: FilterOutliers):
        ellipse = outlier_filter.get_ellipse()
        ax = plt.gca()
        ax.add_artist(ellipse)
        plt.plot(outlier_filter.long_pts[:, 0], outlier_filter.long_pts[:, 1], color='black')
        plt.plot(outlier_filter.short_pts[:, 0], outlier_filter.short_pts[:, 1], color='grey')

    def plot_time_vs_metric(self, exps: [FullIncubationExperiment], metric):
        times, avgs, stds = [], [], []
        for exp in exps:
            times.append(exp.time)
            vals = metric.calculate(exp)
            avgs.append(np.mean(vals))
            stds.append(np.std(vals))
        plt.errorbar(times, avgs, yerr=stds)
        plt.xlabel('Time (mins)')
        plt.ylabel(metric.name)


def only_show_limits(axs):
    for ax in axs:
        ax.set_xticks([ax.get_xticks()[1], ax.get_xticks()[-1]])
        ax.set_yticks([ax.get_yticks()[1], ax.get_yticks()[-1]])
        ax.set_xlabel('')
        ax.set_ylabel('')


def set_axes_limits_to_max_spread(axs):
    xlims = list(chain.from_iterable([ax.get_xlim() for ax in axs]))
    ylims = list(chain.from_iterable([ax.get_ylim() for ax in axs]))
    max_xlim = (min(xlims), max(xlims))
    max_ylim = (min(ylims), max(ylims))
    for ax in axs:
        ax.set_xlim(max_xlim)
        ax.set_ylim(max_ylim)


def create_figure_from_experiments(exp_gen, min_time_pts: int,
                                   get_metric_fns) -> FullIncubationExperiment:
    fig = plt.figure(figsize=(24, 10))
    gs = gridspec.GridSpec(3, 1)
    bottom_row = gridspec.GridSpecFromSubplotSpec(1, len(get_metric_fns), subplot_spec=gs[1:])
    times, avgs, stds = [], [], []
    current_exp_num = 0
    top_axs = []
    exp = None
    plotter = None
    top_row = None
    raw_data_workflow = ControlTransformWorkflow([])
    blank_controls = []
    signal_controls = []
    plottable_transform = None

    def add_if_not_present(new_controls: [DataFrame], control_list: [DataFrame]):
        hasher = lambda df: hash(df.values.tobytes())
        list_hashes = [hasher(df) for df in control_list]
        for control in new_controls:
            ctrl_hash = hasher(control)
            if ctrl_hash not in list_hashes:
                control_list.append(control)

    def add_to_plot_col(plot_col, data: [DataFrame], top_label: str):
        for i, d in enumerate(data):
            ax = plt.subplot(plot_col[i])
            fig.add_subplot(ax)
            plotter.scatter_heatmap(d)
            top_axs.append(ax)
            if i == 0:
                plotter.add_top_right_label(top_label)

    def add_to_exp_row(exp: FullIncubationExperiment):
        nonlocal plotter, current_exp_num, top_row
        top_row = gridspec.GridSpecFromSubplotSpec(1, exp.total_time_pts + 2, subplot_spec=gs[0])
        col = gridspec.GridSpecFromSubplotSpec(len(exp.experiment_data), 1, subplot_spec=top_row[current_exp_num + 2])
        data = raw_data_workflow.apply_transforms(exp.experiment_data)
        add_to_plot_col(col, data, exp.time)
        current_exp_num += 1

    def plot_controls():
        blank_col = gridspec.GridSpecFromSubplotSpec(len(blank_controls), 1, subplot_spec=top_row[0])
        blank_data = raw_data_workflow.apply_transforms(blank_controls)
        add_to_plot_col(blank_col, blank_data, 'blank')
        signal_col = gridspec.GridSpecFromSubplotSpec(len(signal_controls), 1, subplot_spec=top_row[1])
        transforms = raw_data_workflow.all_transforms()
        if len(transforms) > 0:
            trans1 = transforms[0]
            signal_data = trans1.apply_to_many(signal_controls)
            add_to_plot_col(signal_col, signal_data, 'signal')
        else:
            add_to_plot_col(signal_col, signal_controls, 'signal')

    for _ in get_metric_fns:
        avgs.append([])
        stds.append([])

    for idx, exp in enumerate(exp_gen):
        if exp.total_time_pts < min_time_pts:
            return None
        if idx == 0:
            channel_info = exp.channel_info
            plotter = FCSPlotter(x_channel=channel_info.detect_channel, y_channel=channel_info.normal_channel)
            plotter.fast_and_ugly = True
            plotter.set_limit = True
            plt.suptitle('{} incubation with {}'.format(exp.cell_line, exp.particle))
        times.append(exp.time)

        metrics = []
        for get_metric in get_metric_fns:
            try:
                metric = get_metric(exp)
                metrics.append(metric)
            except Exception as e:
                logging.warning('Issue with metric function {}'.format(get_metric.__name__))
                continue
        # metrics = [get_metric(exp) for get_metric in get_metric_fns]
        for i, metric in enumerate(metrics):
            vals = metric.calculate(exp)
            avgs[i].append(np.mean(vals))
            stds[i].append(np.std(vals))
        if hasattr(metrics[0], 'pre_count_workflow'):
            raw_data_workflow = metrics[0].pre_count_workflow
            if hasattr(metrics[0], 'post_count_workflow'):
                for transform in metrics[0].post_count_workflow.all_transforms():
                    if isinstance(transform, AbstractPlottable):
                        plottable_transform = transform
        elif hasattr(metrics[0], 'workflow'):
            raw_data_workflow = metrics[0].workflow
        add_if_not_present(exp.blank_controls, blank_controls)
        add_if_not_present(exp.single_signal_controls, signal_controls)
        add_to_exp_row(exp)

    if top_row is not None:
        plot_controls()
        set_axes_limits_to_max_spread(top_axs)
        only_show_limits(top_axs)

        if plottable_transform is not None:
            for ax in top_axs:
                plt.sca(ax)
                plottable_transform.plot()

        for i, metric in enumerate(metrics):
            fig.add_subplot(plt.subplot(bottom_row[i]))
            plt.errorbar(times, avgs[i], yerr=stds[i])
            plt.xlabel('Time (mins)')
            plt.ylabel(metric.name)
    return exp


def create_tight_plots_from_experiments(exp_gen, workflow: ControlTransformWorkflow, channel_info=None,
                                        ignore_zero=False):
    if ignore_zero:
        next(exp_gen)
    exp_gen, exp_gen2 = tee(exp_gen)
    # find rows and cols
    cols = 0
    rows = 0
    for rows, exp in enumerate(exp_gen2):
        num_exps = len(exp.experiment_data)
        if num_exps > cols:
            cols = num_exps
    _, ax_arr = plt.subplots(rows + 1, cols, sharex=True, sharey=True)
    plotter = None

    for row, exp in enumerate(exp_gen):
        if channel_info is None:
            channel_info = exp.channel_info
        if plotter is None:
            plotter = FCSPlotter(x_channel=channel_info.detect_channel, y_channel=channel_info.normal_channel)
            plotter.fast_and_ugly = True
        all_medians = [np.median(exps[channel_info.detect_channel][:]) for exps in exp.experiment_data]
        exp_trans = workflow.apply_transforms(exp.experiment_data)

        for col, d in enumerate(exp_trans):
            ax = ax_arr[row, col]
            plt.sca(ax)
            plotter.scatter_heatmap(d)
            label = '{:.1f}h\n{}{}:{:.1f}'.format(exp.time/60, '', r'$\tilde{x}$', all_medians[col])
            ax.text(1, 0.1, label, verticalalignment='bottom', horizontalalignment='right', transform=ax.transAxes)
            ax.set_xlabel('')
            ax.set_ylabel('')
            if not (col == 0 and row == rows):
                plt.setp(ax.get_xticklabels(), visible=False)
                plt.setp(ax.get_yticklabels(), visible=False)

    ax = ax_arr[0, 0]
    disp_xticks = [0, floor(ax.get_xlim()[1])]
    ax.set_xticks(disp_xticks)
    ax.set_xticklabels(disp_xticks)
    disp_yticks = [0, floor(ax.get_ylim()[1])]
    ax.set_yticks(disp_yticks)
    ax.set_yticklabels(disp_yticks)

    # plt.sca(ax)
    # plt.setp(plt.setp(ax.get_xticklabels(), visible=True))

    plt.figtext(0.05, 0.5, channel_info.normal_channel, rotation='vertical', va='center')
    plt.figtext(0.5, 0.05, channel_info.detect_channel, ha='center', va='top')
    plt.subplots_adjust(wspace=0, hspace=0)
    print('Median displayed is BEFORE transforming/workflow, data shown is AFTER')


def create_particle_counting_image(sample: DataFrame, transform: AbstractFCSTransform, channel_info: ChannelInfo,
                                   flow_rate_in_ul_per_sec=None):
    median = np.median(sample[channel_info.detect_channel][:])
    sample_trans = transform.apply(sample)
    plotter = FCSPlotter(channel_info.detect_channel, channel_info.normal_channel)
    plotter.fast_and_ugly = True
    plt.subplot(1, 2, 1)
    plotter.scatter_heatmap(sample_trans)
    plt.subplot(1, 2, 2)
    plotter.histogram_with_kde_fit(sample_trans)
    plotter.make_median_samples_time_label(sample, flow_rate_in_ul_per_sec=flow_rate_in_ul_per_sec)
    # label = 'Samples:{}'.format(sample.count)
    #
    # label = '{}{}:{:.1f}'.format('', r'$\tilde{x}$', median)
    # ax.text(1, 0.1, label, verticalalignment='bottom', horizontalalignment='right', transform=ax.transAxes)


# plt.text(0)



class NextPlot:
    def __init__(self, rows, cols):
        self.plt_num = 1
        self.rows = rows
        self.cols = cols

    def next_plot(self):
        plt.subplot(self.rows, self.cols, self.plt_num)
        self.plt_num += 1


def gauss(x, a, x0, sigma):
    return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))


def demo_gating(samples: [DataFrame], f_out: FilterOutliers, f_norm: FilterNormal,
                out_plotter: FCSPlotter, detect_plotter: FCSPlotter):
    p = NextPlot(len(samples), 5)
    out_plotter.set_limit = True
    detect_plotter.set_limit = True

    for idx, sample in enumerate(samples):
        p.next_plot()

        detect_plotter.scatter_heatmap(sample)
        if idx == 0:
            plt.gca().set_xlim([0, 10])
            plt.gca().set_ylim([0, 10])
            detect_plotter.fix_axis_limits()

        p.next_plot()
        out_plotter.scatter_heatmap(sample)
        out_plotter.plot_included_ellipse(f_out)
        if idx == 0:
            plt.gca().set_xlim([0, 10])
            plt.gca().set_ylim([0, 10])
            out_plotter.fix_axis_limits()

        p.next_plot()
        sample = f_out.apply(sample)
        out_plotter.scatter_heatmap(sample)
        out_plotter.plot_classifier_contour(f_out)
        out_plotter.plot_included_ellipse(f_out)

        p.next_plot()
        detect_plotter.scatter_heatmap(sample)
        detect_plotter.plot_included_ellipse(f_norm)
        detect_plotter.plot_classifier_contour(f_norm)
        f_norm.plot()

        p.next_plot()
        sample = f_norm.apply(sample)
        detect_plotter.scatter_heatmap(sample)
        f_norm.plot()


def demo_transforms(samples: [DataFrame], transforms: [AbstractFCSTransform], plotter: FCSPlotter):
    p = NextPlot(len(samples), len(transforms) + 1)
    for idx, sample in enumerate(samples):
        p.next_plot()
        plotter.scatter_heatmap(sample)

        for transform in transforms:
            p.next_plot()
            sample = transform.apply(sample)
            plotter.scatter_heatmap(sample)
            proportion_remaining = sample.shape[0] / samples[idx].shape[0]
            lbl = '{0:.2f}'.format(proportion_remaining)
            plotter.add_top_right_label(lbl)


def demo_transforms_with_histograms(samples: [DataFrame], previsualize_transforms: [AbstractFCSTransform],
                                    transforms: [AbstractFCSTransform], plotter: FCSPlotter):
    p = NextPlot(len(samples), 2 * (1 + len(transforms)))
    for idx, sample in enumerate(samples):
        p.next_plot()
        for transform in previsualize_transforms:
            sample = transform.apply(sample)
        plotter.scatter_heatmap(sample)
        p.next_plot()
        plotter.histogram_with_kde_fit(sample)
        plotter.make_median_samples_time_label(sample)
    for transform in transforms:
        sample = transform.apply(sample)
        p.next_plot()
        plotter.scatter_heatmap(sample)
        plotter.make_median_samples_time_label(sample)
        p.next_plot()
        plotter.histogram_with_kde_fit(sample)


def demo_after_transforms(samples: [DataFrame], transforms: [AbstractFCSTransform], plotter: FCSPlotter, cols=3):
    fig, ax = plt.subplots(ceil(len(samples) / cols), cols, sharex=True, sharey=True)
    # fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)
    if len(samples) * cols > 1:
        axs = ax.ravel()
    else:
        axs = [ax]
    for idx, sample in enumerate(samples):
        plt.sca(axs[idx])
        for transform in transforms:
            sample = transform.apply(sample)
        plotter.scatter_heatmap(sample)


def new_create_images_from_many_experiments(output_dir: str, min_time_pts: int,
                                            exps_gen_gen: [[FullIncubationExperiment]], make_metric_fns):
    for idx, exp_gen in enumerate(exps_gen_gen):
        try:
            exp = create_figure_from_experiments(exp_gen, min_time_pts, make_metric_fns)
            if exp is not None:
                fig_name = '{}_{}_{}.png'.format(exp.particle, exp.cell_line, idx)
                print('  Graphing {}'.format(fig_name))
                filename = path.join(output_dir, fig_name)
                plt.savefig(filename, bbox_inches='tight')
            plt.close()
        except Exception as e:
            print('Issue with experiment set {}:'.format(idx))
            print('  {}'.format(e))
        continue


def create_same_date_images_from_many_experiments(output_dir: str, min_exps: int, exps_gen,
                                                  metric_factory, channel_info: ChannelInfo):
    plotter = FCSPlotter(x_channel=channel_info.detect_channel, y_channel=channel_info.normal_channel)

    def plot_days_exps(folder, all_exps):
        if len(all_exps) > 0:
            try:
                fig_name = '{}_img.png'.format(path.split(folder)[1])
                p = NextPlot(1, len(all_exps))
                plt.figure(figsize=(5 * len(all_exps), 5))
                for exps in all_exps:
                    p.next_plot()
                    metric = metric_factory.get_metric(exps)
                    plotter.plot_time_vs_metric(exps, metric)
                    label = '{} in {}'.format(exps[-1].cell_line, exps[-1].particle_desc)
                    plotter.add_top_right_label(label)
                filename = path.join(output_dir, fig_name)
                plt.savefig(filename, bbox_inches='tight')
            except Exception as e:
                print('Issue with folder {}:'.format(folder))
                print('  {}'.format(e))

    all_exps = []
    last_folder = ''
    for folder, exps in exps_gen:
        if len(exps) >= min_exps:
            if folder == last_folder:
                all_exps.append(exps)
            else:
                plot_days_exps(last_folder, all_exps)
                last_folder = folder
                all_exps = [exps]


def save_image(config_file):
    filepath = os.path.splitext(config_file)[0] + '.png'
    plt.savefig(filepath)


def save_fig(config_file, ending):
    base_name = os.path.splitext(config_file)[0] + '.png'
    plt.savefig('{}_{}.png'.format(base_name, ending))


def create_tight_figs(config_files, cis):
    for ci in cis:
        for config_file in config_files:
            exp_gen = FullIncubationExperiment.incubation_generator_from_ini(config_file)
            workflow = ControlTransformWorkflow([(ArcSinHTransform(ci.all_channels()), ct.no_control)])
            workflow.setup_from_controls(None, None)
            create_tight_plots_from_experiments(exp_gen, workflow, channel_info=ci, ignore_zero=False)
            save_fig(config_file, '{}_{}'.format(ci.detect_channel, ci.normal_channel))
            plt.show(block=False)


def create_particle_counting_figs(config_files, channel_info, flow_rate_in_ul_per_sec=None):
    # workflow = ControlTransformWorkflow([(ArcSinHTransform(channel_info.all_channels()), ct.no_control)])
    # workflow.setup_from_controls(None, None)
    transform = ArcSinHTransform(channel_info.all_channels())
    for config_file in config_files:
        if config_file.lower().endswith('.ini'):
            samples = FullIncubationExperiment.get_all_particle_signal_data(config_file)
            for i, sample in enumerate(samples):
                plt.figure()
                create_particle_counting_image(sample, transform, channel_info, flow_rate_in_ul_per_sec=flow_rate_in_ul_per_sec)
                save_fig(config_file, '{}_particle_count_{}'.format(channel_info.detect_channel, i))
        elif config_file.lower().endswith('.fcs'):
            plt.figure()
            sample = convert_and_process_sample(config_file)
            create_particle_counting_image(sample, transform, channel_info, flow_rate_in_ul_per_sec=flow_rate_in_ul_per_sec)
            save_fig(config_file, '{}_particle_count_{}'.format(channel_info.detect_channel, 0))
        else:
            print('Unrecognized file type: {}'.format(config_file))
