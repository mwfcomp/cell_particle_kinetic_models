import unittest
from unittest.mock import patch, MagicMock

from flow_ctyometry.fcs_display import *


class IncubationExperimentTests(unittest.TestCase):
    def test_something(self):
        pass
        # self.assertEqual(1, 2)

    def test_parse_filename_examples(self):
        pass


class IncubationExperimentLoaderTests(unittest.TestCase):
    def setUp(self):
        self.loader = IncubationExperimentsLoader('/fake_control_file', channel_info=ChannelInfo('test', 'test'))

    def test_parse_filename(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HeLa - 60 nm 0.5 hr.fcs')
        expected = ('HeLa', '60 nm', 30, 0)
        self.assertEqual(parsed, expected)

    def test_parse_filename2(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HeLa - 60 nm 0.5 hr (1).fcs')
        expected = ('HeLa', '60 nm', 30, 1)
        self.assertEqual(parsed, expected)

    def test_parse_filename3(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('RAW - 60 nm CS 1 h (1).fcs')
        expected = ('RAW', '60 nm CS', 60, 1)
        self.assertEqual(parsed, expected)

    def test_parse_filename4(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('E16_032 - HeLa Cis 24hr.fcs')
        expected = ('E16_032', 'HeLa Cis', 1440, 0)
        self.assertEqual(parsed, expected)

    def test_parse_with_default_time(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HeLa - 110 nm Capsule K-free (1).fcs')
        self.assertEqual(parsed, (None, None, None, None))

        self.loader.default_time_hrs = 2
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HeLa - 110 nm Capsule K-free (1).fcs')
        expected = ('HeLa', '110 nm Capsule K-free', self.loader.default_time_hrs * 60, 1)
        self.assertEqual(parsed, expected)

    def test_parse_with_default_particle(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HELA 1hr_0.fcs')
        self.assertEqual(parsed, (None, None, None, None))

        self.loader.default_particle = 'test particle'
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HELA 1hr_0.fcs')
        expected = ('HELA', self.loader.default_particle, 60, 0)
        self.assertEqual(parsed, expected)

    def test_parse_filename_with_default_particle(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HELA 3hr.fcs')
        self.assertEqual(parsed, (None, None, None, None))

        self.loader.default_particle = 'test particle'
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HELA 3hr.fcs')
        expected = ('HELA', self.loader.default_particle, 180, 0)
        self.assertEqual(parsed, expected)

    def test_parse_fails(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('control.fcs')
        self.assertEqual(parsed, (None, None, None, None))

    def test_parse_fails1(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name('HeLa - Control.fcs')
        self.assertEqual(parsed, (None, None, None, None))

    def test_parse_fails2(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name(
                'Ahaogieaoruer.fcs')
        self.assertEqual(parsed, (None, None, None, None))

    def test_parse_fails3(self):
        parsed = self.loader.cell_particle_time_replicate_from_file_name(
                '3048 A (110 nm PMA(SH) - AF488 Core-Shell) (1).fcs')
        self.assertEqual(parsed, (None, None, None, None))

    @staticmethod
    def mock_init_from_files(experiment_files: [str], blank_ctrl_files: [str],
                             signal_ctrl_files: [str],
                             time: float, signal_correction: float, total_time_pts: int,
                             channel_info: ChannelInfo, particle: str, cell_line: str):
        return FullIncubationExperiment(experiment_data=[],
                                        blank_controls=[],
                                        single_signal_controls=[],
                                        time=time,
                                        total_time_pts=total_time_pts,
                                        signal_correction=signal_correction,
                                        channel_info=channel_info,
                                        particle=particle,
                                        cell_line=cell_line,
                                        control_hash="")

    mock_isfile = MagicMock(return_value=True)

    # a fairly brittle test - refactoring in flow cytometry will break
    # IF REFACTORED, WILL NEED TO CHANGE PATCH LOCATION
    @patch('flow_ctyometry.experiment.path.isfile', mock_isfile)
    @patch.object(FullIncubationExperiment, 'incubation_experiment_from_file_names', mock_init_from_files)
    def test_series_from_multi_folder(self):
        mock_control_files = ['HELA - Control.fcs', 'THP-1 - control.fcs']
        mock_real_files = ["THP-1 - 235 nm CS 16 hr (1).fcs",
                           "09-Jul-2015.wsp",
                           "hela - 110 nm Capsule 1 hr (1).fcs",
                           "HeLa - 110 nm Capsule 2 hr.fcs",
                           "HeLa - 110 nm Capsule 4 hr (1).fcs",
                           "HeLa - 110 nm Capsule 4 hr.fcs",
                           "HeLa - 110 nm Capsule 8 hr (1).fcs",
                           "HeLa - 110 nm Capsule 8 hr.fcs",
                           "HeLa - 110 nm Capsule 16 hr.fcs",
                           "WRONGCELL - 110 nm Capsule 16 hr.fcs",
                           '3048 A (110 nm PMA(SH) - AF488 Core-Shell) (1).fcs',
                           "thp-1 - 235 nm CS 24 hr.fcs"]
        path.isfile('test')

        def fake_listdir(folder):
            if folder == self.loader.control_folder:
                return mock_control_files
            return mock_real_files

        with patch('flow_ctyometry.experiment.listdir', fake_listdir):
            all_series = list(self.loader.get_incubation_generators_from_folder('fake_test_folder'))
            self.assertEqual(len(all_series), 2)

            all_series = [list(all_series[0]), list(all_series[1])]
            all_series.sort(key=lambda s: s[0].cell_line.lower())
            for exp in all_series[0]:
                self.assertEqual(exp.total_time_pts, 5)
            for exp in all_series[1]:
                self.assertEqual(exp.total_time_pts, 2)


class TransformTests(unittest.TestCase):
    def setUp(self):
        sample_control_data = None
        sample_signal_data = None

    def test_loglike_transform_only_affects_specified_columns(self):
        pass

    def test_unloglike_transform_undoes_loglike_transform(self):
        pass

    def test_filter_outliers_removes_outlier_portion(self):
        pass

    def test_filter_normal_takes_correct_points(self):
        pass


class AbstractCountMetricTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_particles_per_cell_calculates_expected(self):
        pass

    def test_min_signal_estimate_particles_per_cell_estimate_expected(self):
        pass
