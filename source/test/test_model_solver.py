import unittest
from copy import deepcopy

from dosimetry.boundary import *
from dosimetry.experimental_detail import *
from dosimetry.initial_condition import *
from dosimetry.solver.standard import *


class ModelBuilderTests(unittest.TestCase):
    def setUp(self):
        self.core_shell = SphericalParticle(name='Core shell', radius=(250e-9) / 2, density=1650)
        self.media = LiquidMedia(viscosity=.00101, density=1000)
        self.bottom_detail = ExperimentalDetail(self.core_shell, self.media,
                                                CylindricalCellCultureCondition(width=.022, height=.0053,
                                                                                temperature=310.15,
                                                                                time_in_seconds=24 * 60 * 60,
                                                                                particle_concentration=1,
                                                                                cell_orientation=CellOrientation.bottom,
                                                                                flow_speed=0))

        self.inverted_detail = ExperimentalDetail(self.core_shell, self.media,
                                                  CylindricalCellCultureCondition(width=.022, height=.002,
                                                                                  temperature=310.15,
                                                                                  time_in_seconds=24 * 60 * 60,
                                                                                  particle_concentration=1,
                                                                                  cell_orientation=CellOrientation.top,
                                                                                  flow_speed=0))

        self.vertical_detail = ExperimentalDetail(self.core_shell, self.media,
                                                  RectangularCellCultureCondition(width=.007, height=.016, depth=.014,
                                                                                  temperature=310.15, time_in_seconds=24 * 60 * 60,
                                                                                  particle_concentration=1,
                                                                                  cell_orientation=CellOrientation.vertical,
                                                                                  flow_speed=0))

        self.model_builder = standard_constant_advection_diffusion_greedy_model_builder(self.bottom_detail)

        self.model_solver = TimestepLinearModelSolver()
        # self.model_solver.visualize = True
        self.low_flux_bc = LinearFluxCellBoundaryConditionMaker(1e-12)
        self.med_flux_bc = LinearFluxCellBoundaryConditionMaker(5e-9)
        self.high_flux_bc = LinearFluxCellBoundaryConditionMaker(10)

    def _assert_amount_removed_is_expected(self, expected_val, check_relative_amount=False):
        model = self.model_builder.build_model()
        solution = self.model_solver.solve_model(model)
        if check_relative_amount:
            self.assertAlmostEqual(solution.relative_amount_removed, expected_val, 2)
        else:
            self.assertAlmostEqual(solution.absolute_amount_removed, expected_val, 4)

            # amt_normalized_to_bottom = solution.relative_amount_removed * \
            #                            condition.volume / self.bottom_condition.volume
            # self.assertAlmostEqual(amt_normalized_to_bottom, expected_val, places=2)

    def _assert_boundary_conditions_equal(self,
                                          exp_detail: ExperimentalDetail,
                                          bc1: AbstractBoundaryConditionMaker,
                                          bc2: AbstractBoundaryConditionMaker):
        self.model_builder = standard_constant_advection_diffusion_greedy_model_builder(exp_detail)
        self.model_builder.boundary_condition_maker = bc1
        sol1 = self.model_solver.solve_model(self.model_builder.build_model())

        self.model_builder.boundary_condition_maker = bc2
        sol2 = self.model_solver.solve_model(self.model_builder.build_model())

        self.assertAlmostEqual(sol1.relative_amount_removed, sol2.relative_amount_removed, places=2)

    def test_solution_calculations(self):
        sol = Solution(100.0, 90.0)
        self.assertAlmostEqual(sol.absolute_amount_removed, 10.0)
        self.assertAlmostEqual(sol.relative_amount_removed, 0.1)
        self.assertAlmostEqual(sol.absolute_amount_remaining, 90.0)
        self.assertAlmostEqual(sol.relative_amount_remaining, 0.9)

    def test_long_no_flux_nothing_removed(self):
        self.model_builder.boundary_maker = NoFluxBoundaryConditionMaker()
        self._assert_amount_removed_is_expected(0.0)

    def test_long_known_system_bottom(self):
        self._assert_amount_removed_is_expected(0.37, True)

    def test_long_known_system_inverted(self):
        expected_amt = .015 / (
            self.inverted_detail.cell_culture_condition.volume / self.bottom_detail.cell_culture_condition.volume)
        self.model_builder = standard_constant_advection_diffusion_greedy_model_builder(self.inverted_detail)
        self._assert_amount_removed_is_expected(expected_amt, True)

    def test_long_known_system_vertical(self):
        expected_amt = .048 / (
            self.vertical_detail.cell_culture_condition.volume / self.bottom_detail.cell_culture_condition.volume)
        self.model_builder = standard_constant_advection_diffusion_greedy_model_builder(self.vertical_detail)
        self._assert_amount_removed_is_expected(expected_amt, True)

    def test_double_concentration_equals_double_absolute_amount_removed(self):
        exp_detail_2 = deepcopy(self.bottom_detail)
        exp_detail_2.cell_culture_condition.particle_concentration *= 2
        model_builder2 = standard_constant_advection_diffusion_greedy_model_builder(exp_detail_2)
        sol2 = self.model_solver.solve_model(model_builder2.build_model())
        self._assert_amount_removed_is_expected(sol2.relative_amount_removed, True)
        self._assert_amount_removed_is_expected(sol2.absolute_amount_removed / 2)

    def test_long_high_flux_bottom_like_max_greedy(self):
        self._assert_boundary_conditions_equal(self.bottom_detail, self.high_flux_bc,
                                               MaxGreedyBoundaryConditionMaker())

    def test_long_high_flux_vertical_like_max_greedy(self):
        self._assert_boundary_conditions_equal(self.vertical_detail, self.high_flux_bc,
                                               MaxGreedyBoundaryConditionMaker())

    def test_long_low_flux_like_no_flux(self):
        self._assert_boundary_conditions_equal(self.bottom_detail, self.low_flux_bc,
                                               NoFluxBoundaryConditionMaker())

    def test_long_medium_flux_in_between_no_flux_and_high_flux(self):
        self.model_builder.boundary_condition_maker = self.low_flux_bc
        sol_low = self.model_solver.solve_model(self.model_builder.build_model())

        self.model_builder.boundary_condition_maker = self.med_flux_bc
        sol_med = self.model_solver.solve_model(self.model_builder.build_model())

        self.model_builder.boundary_condition_maker = self.high_flux_bc
        sol_high = self.model_solver.solve_model(self.model_builder.build_model())

        self.assertLess(sol_low.absolute_amount_removed, sol_med.absolute_amount_removed)
        self.assertLess(sol_med.absolute_amount_removed, sol_high.absolute_amount_removed)

    def test_long_amount_removed_greedy_more_when_longer(self):
        sol_normal = self.model_solver.solve_model(self.model_builder.build_model())
        self.model_builder.time_maker.time /= 2
        sol_half = self.model_solver.solve_model(self.model_builder.build_model())
        self.assertLess(sol_half.absolute_amount_removed, sol_normal.absolute_amount_removed)

    def test_long_amount_removed_flux_more_when_longer(self):
        self.model_builder.boundary_condition_maker = self.med_flux_bc
        sol_normal = self.model_solver.solve_model(self.model_builder.build_model())
        self.model_builder.time_maker.time /= 2
        sol_half = self.model_solver.solve_model(self.model_builder.build_model())
        self.assertLess(sol_half.absolute_amount_removed, sol_normal.absolute_amount_removed)

    def test_long_clump_isnt_removed_never_encounters_boundary(self):
        self.model_builder.initial_condition_maker = MiddleClumpInitialConditionMaker(1.0)
        self._assert_amount_removed_is_expected(0.0)

    def test_long_clump_fully_removed_long_duration(self):
        self.model_builder.time_maker.time = 24 * 60 * 300
        self.model_builder.initial_condition_maker = MiddleClumpInitialConditionMaker(1.0)
        self._assert_amount_removed_is_expected(1.0, True)

    def test_long_nothing_removed_floating_particle(self):
        self.core_shell.density = 900
        self._assert_amount_removed_is_expected(0.0)

    def disabled_test_long_heavy_particles_work(self):
        # This test is incredibly long running, disabling for now. It should run, though
        self.core_shell.density *= 5
        self.model_builder = standard_constant_advection_diffusion_greedy_model_builder(self.bottom_detail)
        self.model_solver = TimestepLinearModelSolver()
        self._assert_amount_removed_is_expected(1,True)

    # def test_long_relative_amount_removed_linear_flux_independent_of_concentration(self):
