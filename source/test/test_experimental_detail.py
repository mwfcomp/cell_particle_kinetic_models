import unittest

from dosimetry.experimental_detail import *
from dosimetry.experimental_detail import _load_details_from_lines


class ExperimentalDetailTests(unittest.TestCase):
    def assertAlmostEqual(self, a, b):
        super().assertAlmostEqual(a, b, places=4)

    def test_load_details_from_lines(self):
        test_lines = ['~CellCultureCondition\n',
                      'width=1\n', 'height=2\n', 'volume=3\n',
                      'temperature=4\n', 'time=5\n', 'cell_orientation=CellOrientation:bottom\n',
                      'particle_concentration=1\n',
                      'flow_speed=6\n',
                      '~LiquidMedia\n', 'viscosity=7\n', 'density=8\n',
                      '~SphericalParticle\n', "name='test particle'\n", 'radius=9\n', 'density=10\n',
                      '~SphericalParticle\n', "name='test particle2'\n", 'radius=11\n', 'density=12\n',
                      '~CellCultureCondition\n',
                      'width=13\n', 'height=14\n', 'volume=15\n',
                      'temperature=16\n', 'time=17\n', 'cell_orientation=CellOrientation:top\n',
                      'particle_concentration=1\n',
                      'flow_speed=18\n',
                      '~LiquidMedia\n', 'viscosity=19\n', 'density=20\n',
                      '~SphericalParticle\n', "name='test particle3'\n", 'radius=21\n', 'density=22\n']
        details = _load_details_from_lines(test_lines)
        culture_condition = CellCultureCondition(width=1, height=2, volume=3, temperature=4, time_in_seconds=5,
                                                 cell_orientation=CellOrientation.bottom, particle_concentration=1,
                                                 flow_speed=6)
        media = LiquidMedia(viscosity=7, density=8)
        particle0 = SphericalParticle('test particle', radius=9, density=10)
        particle1 = SphericalParticle('test particle2', radius=11, density=12)
        detail0 = ExperimentalDetail(cell_culture_condition=culture_condition, media=media, particle=particle0)
        detail1 = ExperimentalDetail(cell_culture_condition=culture_condition, media=media, particle=particle1)
        detail2 = ExperimentalDetail(cell_culture_condition=
                                     CellCultureCondition(width=13, height=14, volume=15, temperature=16, time_in_seconds=17,
                                                          cell_orientation=CellOrientation.top,
                                                          particle_concentration=1,
                                                          flow_speed=18),
                                     media=LiquidMedia(density=20, viscosity=19),
                                     particle=SphericalParticle(name='test particle3', radius=21, density=22))
        self.assertEqual(details[0], detail0)
        self.assertEqual(details[1], detail1)
        self.assertEqual(details[2], detail2)

    def test_spherical_particle_calculations_correct(self):
        particle = SphericalParticle(name='test', radius=5, density=100)
        self.assertAlmostEqual(particle.volume, 523.59877)
        self.assertAlmostEqual(particle.surface_area, 314.15926)
        self.assertAlmostEqual(particle.mass, 52359.87755)

    def test_cylinder_culture_conditions_volume_correct(self):
        culture = CylindricalCellCultureCondition(width=5, height=10, temperature=0, time_in_seconds=0,
                                                  cell_orientation=CellOrientation.bottom, particle_concentration=1,
                                                  flow_speed=1)
        self.assertAlmostEqual(culture.volume, 196.34954)

    def test_rectangular_culture_conditions_volume_correct(self):
        culture = RectangularCellCultureCondition(width=5, height=10, depth=2, temperature=0, time_in_seconds=0,
                                                  cell_orientation=CellOrientation.bottom, particle_concentration=1,
                                                  flow_speed=1)
        self.assertAlmostEqual(culture.volume, 100)
