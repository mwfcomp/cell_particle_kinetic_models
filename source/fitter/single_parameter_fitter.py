from copy import deepcopy
from typing import Callable, List
import scipy.optimize
from utility.config_interpreter import ConfigInterpreter
import numpy as np


class ParameterFitter:
    def __init__(self, solution_interpolator,
                 rate_scale: float,
                 mesh_scaling: float,
                 # allow scaling of the rate prior to objective function to make solving easier for minimizer function
                 objective_fn: Callable[[float, List[float], List[float]], float],
                 config_to_solutions,
                 get_min_config_fn: Callable[[ConfigInterpreter], ConfigInterpreter],
                 get_max_config_fn: Callable[[ConfigInterpreter], ConfigInterpreter],
                 get_values_fn: Callable[[ConfigInterpreter], List[float]],
                 minimizer_fn: Callable[[Callable[[float], float]], scipy.optimize.OptimizeResult] = scipy.optimize.minimize_scalar):
        self.solution_interpolator = solution_interpolator
        self.rate_scale = rate_scale
        self.mesh_scaling = mesh_scaling
        self.objective_fn = objective_fn
        self.config_to_solutions = config_to_solutions
        self.minimizer_fn = minimizer_fn
        self.report_status = True
        self.get_min_config_fn = get_min_config_fn
        self.get_max_config_fn = get_max_config_fn
        self.get_values_fn = get_values_fn

    def get_best_fit_parameter_and_error(self, config: ConfigInterpreter):
        fn_to_minimize = lambda rate: self._calculate_objective(config, rate * self.rate_scale)
        optimize_result = self.minimizer_fn(fn_to_minimize)
        best_rate = optimize_result.x * self.rate_scale
        return best_rate, optimize_result.fun

    def get_max_fit_parameter_and_error(self, config: ConfigInterpreter):
        max_config = self.get_max_config_fn(config)
        return self.get_best_fit_parameter_and_error(max_config)

    def get_min_fit_parameter_and_error(self, config: ConfigInterpreter):
        min_config = self.get_min_config_fn(config)
        return self.get_best_fit_parameter_and_error(min_config)

    def _calculate_objective(self, config: ConfigInterpreter, rate):
        real_values = self.get_values_fn(config)
        model_values = self.solution_interpolator(self.config_to_solutions, config, rate, self.mesh_scaling)
        objective_val = self.objective_fn(rate, real_values, model_values)
        if self.report_status:
            print('Fit rate: {}, error: {}'.format(rate, objective_val))
        return objective_val




def bounded_objective_fn(objective_fn: Callable[[float, List[float], List[float]], float],
                         min_rate: float,
                         max_rate: float,
                         max_error: float) -> Callable[[float, List[float], List[float]], float]:
    def bounded(rate, estimated, actual):
        if rate < min_rate or rate > max_rate:
            return max_error
        else:
            return objective_fn(rate, estimated, actual)

    return bounded


def min_config_subtract_dispersions(config: ConfigInterpreter, dispersion_multiplier: float) -> ConfigInterpreter:
    new_config = deepcopy(config)
    new_vals = [m - i * dispersion_multiplier for m, i in zip(config.associated_per_cell_means, config.all_associated_per_cell_dispersions)]
    new_config.associated_per_cell_means = new_vals
    return new_config


def max_config_adding_dispersions(config: ConfigInterpreter, dispersion_multiplier: float) -> ConfigInterpreter:
    new_config = deepcopy(config)
    new_vals = [m + i * dispersion_multiplier for m, i in zip(config.associated_per_cell_means, config.all_associated_per_cell_dispersions)]
    new_config.associated_per_cell_means = new_vals
    return new_config


def new_config_change_density(config: ConfigInterpreter, density: float) -> ConfigInterpreter:
    new_config = deepcopy(config)
    new_config.experimental_and_cell_detail.particle.density = density
    return new_config



