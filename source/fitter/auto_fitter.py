from configparser import ConfigParser
from utility.fitter_utility import *

import pandas as pd
import os
monitored_folder = '.'
import matplotlib.pyplot as plt
import traceback as tb

model_desc = 'model'
max_detectable_model_desc = 'MAX'

def process_all_in_folder():
    config_files = get_all_unprocessed_inis()
    for config_file in config_files:
        try:
            process_config_file(config_file)
            plt.close()
        except Exception as e:
            print('Issue with file: {}'.format(config_file))
            print('---{}'.format(e))
            tb.print_exc()
            err_path = get_err_filename(config_file, model_desc)
            open(err_path, 'a').close() #make error file which will prevent future processing
            continue

def file_processed(full_file_path):
    csv_path = get_csv_filename(full_file_path, model_desc)
    err_path = get_err_filename(full_file_path, model_desc)
    return os.path.isfile(csv_path) or os.path.isfile(err_path)

def get_all_unprocessed_inis():
    all_files = [os.path.join(monitored_folder, fn) for fn in next(os.walk(monitored_folder))[2]]
    all_inis = [f for f in all_files if f.endswith('.ini')]
    unprocessed_inis = [f for f in all_inis if not file_processed(f)]
    return unprocessed_inis

def base_output_file_name(config_file):
    output_file_name = '{}'.format(os.path.splitext(config_file)[0])
    return output_file_name

def get_figure_path(config_file):
    return base_output_file_name(config_file) + '.png'

def get_csv_info_path(config_file):
    return base_output_file_name(config_file) + '.csv'


def process_config_file(config_file):
    config = get_config(config_file)
    config_to_solutions = all_oneway_fns['Cell carrying capacity']
    mesh_scaling = mesh_refinement.get_high_low_rate_gci_mesh_refinement(config_to_solutions=config_to_solutions,
                                                                         config=config,
                                                                         grid_refinement_factor=1.25)
    fitter = get_bounded_fitter_dispersion_varied(config,
                                                  config_to_solutions=config_to_solutions,
                                                  mesh_scaling=mesh_scaling)

    #only static adherent have a max detectable rate
    is_static_adherent = config.config['model'].get('condition', 'static_adherent') == 'static_adherent'
    if is_static_adherent:
        max_detectable_rate_fitter = get_fitter_max_rate(config,
                                                     config_to_solutions=config_to_solutions,
                                                     mesh_scaling=mesh_scaling)
        max_detectable_rate, _ = max_detectable_rate_fitter.get_best_fit_parameter_and_error(config)
        plot_and_save_real_and_estimated(model_desc=max_detectable_model_desc,
                                         config_to_solutions=config_to_solutions,
                                         config=config,
                                         best_rate=max_detectable_rate,
                                         min_rate=max_detectable_rate,
                                         max_rate=max_detectable_rate)

    #only static adherent have a max detectable rate
    # configs = [c for c in configs if c.config['model'].get('condition', 'static_adherent') == 'static_adherent']

    best_rate, best_rate_error = fitter.get_best_fit_parameter_and_error(config)
    min_rate, min_rate_error = fitter.get_min_fit_parameter_and_error(config)
    max_rate, max_rate_error = fitter.get_max_fit_parameter_and_error(config)
    plot_and_save_real_and_estimated(model_desc=model_desc,
                                     config_to_solutions=config_to_solutions,
                                     config=config,
                                     best_rate=best_rate, min_rate=min_rate, max_rate=max_rate)

    config.get_dataframe().to_csv(get_csv_info_path(config.config_file))



    # create_and_save_csv_info(times, true_amts, true_devs, model_config, rates, true_capacity, csv_file)



    # model_config = ConfigParser()
    # model_config.read(config_file)
    # setup_workflow(model_config)
    # times, true_amts, true_devs = times_amts_devs_from_config(config_file, model_config)
    # rates = find_rates(model_config, times, true_amts, true_devs)
    # true_capacity = get_true_capacity(model_config, times, true_amts)
    # csv_file = get_csv_info_path(config_file)
    # create_and_save_csv_info(times, true_amts, true_devs, model_config, rates, true_capacity, csv_file)
    # create_and_save_plot(csv_file, get_figure_path(config_file))
    # return rates[1]
