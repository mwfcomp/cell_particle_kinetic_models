import traceback
from typing import List, Callable, Dict
import time

import sys
from pandas.errors import EmptyDataError
from fitter.single_parameter_fitter import ParameterFitter
import fitter.mesh_refinement as mesh_refinement
from utility.config_interpreter import ConfigInterpreter
import pandas as pd
import pathos.multiprocessing
import multiprocessing
import os.path
import os
import logging

logger = logging.getLogger(__name__)
fit_max_min = True
write_lock = multiprocessing.Lock()
plot_lock = multiprocessing.Lock()
plot_fn = lambda model_desc, config_to_solutions, config, best_rate, min_rate, max_rate: None
grid_refinement_factor=1.25
output_file_per_process=False
do_mesh_refinement = True

def write_model_fits_and_errors(configs: List[ConfigInterpreter],
                                output_file_name: str,
                                desc_to_model_fn_dict,
                                desc_to_fitter_fn_dict: Dict[str, Callable[[ConfigInterpreter], ParameterFitter]],
                                processes=1):
    pool = pathos.multiprocessing.Pool(processes=processes)
    for config in configs:
        logger.info('*CONFIG: {}'.format(config.config_file))
        for model_desc, model_fn in desc_to_model_fn_dict.items():
            logger.info('**MODEL: {}'.format(model_desc))
            if do_mesh_refinement:
                mesh_scaling = mesh_refinement.get_high_low_rate_gci_mesh_refinement(config_to_solutions=model_fn,
                                                                                     config=config,
                                                                                     grid_refinement_factor=grid_refinement_factor)
            else:
                mesh_scaling = 1 / grid_refinement_factor
            for fitter_desc, fitter_fn in desc_to_fitter_fn_dict.items():
                logger.info('***FITTER: {}'.format(fitter_desc))
                fitter = fitter_fn(config, model_fn, mesh_scaling)
                if processes > 1:
                    pool.apply_async(write_single_model_fit, args=(output_file_name,
                                                                   fitter_desc, fitter,
                                                                   model_desc, config))
                else:
                    write_single_model_fit(output_file_name,
                                           fitter_desc, fitter,
                                           model_desc, config)
    pool.close()
    pool.join()


def write_single_model_fit(output_file_name: str,
                           fitter_desc: str, fitter: ParameterFitter,
                           model_desc: str, config: ConfigInterpreter):
    try:
        best_rate, best_rate_error = fitter.get_best_fit_parameter_and_error(config)
        if fit_max_min:
            min_rate, min_rate_error = fitter.get_min_fit_parameter_and_error(config)
            max_rate, max_rate_error = fitter.get_max_fit_parameter_and_error(config)
        else:
            min_rate, min_rate_error = best_rate, best_rate_error
            max_rate, max_rate_error = best_rate, best_rate_error

        append_to_output_dataframe(output_file_name, config, fitter_desc, model_desc,
                                   best_rate, best_rate_error, max_rate, max_rate_error, min_rate, min_rate_error,
                                   fitter.mesh_scaling)

        if plot_fn is not None:
            with plot_lock:
                plot_fn(model_desc, fitter.config_to_solutions, config, best_rate, min_rate, max_rate)


    except Exception as e:
        logger.error('ERROR with {} using {} fitter:'.format(config.config_file, fitter_desc))
        traceback.print_exc()


def append_to_output_dataframe(output_file_name_base, config, fitter_desc, model_desc,
                               best_rate, best_rate_error, max_rate, max_rate_error, min_rate, min_rate_error,
                               mesh_scaling):
    if output_file_per_process:
        path, ext = os.path.splitext(output_file_name_base)
        output_file_name = '{}_{}{}'.format(path, os.getpid(), ext)
    else:
        output_file_name = output_file_name_base

    with write_lock:
        try:
            if not os.path.isfile(output_file_name):
                with open(output_file_name, 'w'):
                    pass
            fit_dataframe = pd.read_csv(output_file_name)
        except EmptyDataError:
            fit_dataframe = pd.DataFrame()
        timestr = time.strftime("%Y-%m-%d_%H:%M:%S")
        fit_dataframe = fit_dataframe.append({
            'time': timestr,
            'config': config.config_file,
            'fitter_desc': fitter_desc,
            'model_desc': model_desc,
            'best_rate': best_rate,
            'max_rate': max_rate,
            'min_rate': min_rate,
            'best_rate_error': best_rate_error,
            'min_rate_error': min_rate_error,
            'max_rate_error': max_rate_error,
            'mesh_scaling': mesh_scaling}, ignore_index=True)
        fit_dataframe.to_csv(output_file_name, index=False,
                             columns=['time', 'config', 'model_desc', 'fitter_desc',
                                      'best_rate', 'max_rate', 'min_rate', 'best_rate_error', 'min_rate_error',
                                      'max_rate_error', 'mesh_scaling'])
