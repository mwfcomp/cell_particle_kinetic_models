from utility.config_interpreter import ConfigInterpreter
import matplotlib.pyplot as plt
import logging

logger = logging.getLogger(__name__)

report = False


# iteratively refines mesh until GCI is under threshold
def get_gci_mesh_refinement(config_to_solutions,
                            config: ConfigInterpreter,
                            rate=100.,
                            gci_threshold=0.01,
                            grid_refinement_factor=2,
                            order_of_accuracy=1,
                            safety_factor=3):
    gcis = []
    diffs = []
    amts = []
    mesh_scaling = 1.0 * grid_refinement_factor
    while len(gcis) == 0 or gcis[-1] > gci_threshold:
        mesh_scaling /= grid_refinement_factor
        sols = config_to_solutions(config, rate, mesh_scaling)
        finer_sol = sols[-1][1].absolute_amount_removed
        if len(amts) > 0:
            gci = get_grid_convergence_index(fine_sol=finer_sol,
                                             coarse_sol=amts[-1],
                                             grid_refinement_factor=grid_refinement_factor,
                                             order_of_accuracy=order_of_accuracy,
                                             safety_factor=safety_factor)
            gcis.append(gci)
            diffs.append(finer_sol - amts[-1])
        if report and len(gcis) > 0:
            plt.close()
            steps = range(1, len(gcis) + 1)
            plt.plot(steps, gcis)
            plt.xlabel('Number of refinements')
            plt.ylabel('GCI')
            plt.show(block=False)
            print("Mesh refinement: {}, GCIs: {}, diffs: {}, amounts: {}".format(mesh_scaling, gcis, diffs, amts))
        amts.append(finer_sol)
    logger.info("Mesh refinement under gci threshold of {} : {}".format(gci_threshold, mesh_scaling))
    return mesh_scaling  # undo extra refinement


def get_high_low_rate_gci_mesh_refinement(config_to_solutions,
                                          config: ConfigInterpreter,
                                          low_rate=1e-11,
                                          high_rate=100,
                                          gci_threshold=0.01,
                                          grid_refinement_factor=2,
                                          order_of_accuracy=1,
                                          safety_factor=3):
    logger.info("Doing mesh refinement for low rate of {}, high_rate of {}".format(low_rate, high_rate))
    low_refinement = get_gci_mesh_refinement(config_to_solutions=config_to_solutions,
                                             config=config,
                                             rate=low_rate,
                                             gci_threshold=gci_threshold,
                                             grid_refinement_factor=grid_refinement_factor,
                                             order_of_accuracy=order_of_accuracy,
                                             safety_factor=safety_factor)
    high_refinement = get_gci_mesh_refinement(config_to_solutions=config_to_solutions,
                                              config=config,
                                              rate=high_rate,
                                              gci_threshold=gci_threshold,
                                              grid_refinement_factor=grid_refinement_factor,
                                              order_of_accuracy=order_of_accuracy,
                                              safety_factor=safety_factor)

    return min(low_refinement, high_refinement)



# calculates grid convergence index suggested by roache
def get_grid_convergence_index(fine_sol: float,
                               coarse_sol: float,
                               grid_refinement_factor: float,
                               order_of_accuracy: float,
                               safety_factor: float = 3):
    f1 = fine_sol
    f2 = coarse_sol
    r = grid_refinement_factor
    p = order_of_accuracy
    Fs = safety_factor

    return Fs / (r ** p - 1) * abs((f2 - f1) / f1)
