from math import ceil

from dolfin import *
from dolfin.cpp.generation import IntervalMesh, RectangleMesh
from scipy import constants

from dosimetry.time_and_mesh import CGSpaceAndFunctionMaker, SpecifyMinimumTimestepMaker
from .experimental_detail import ExperimentalDetail
from .model import *
from .initial_condition import UniformInitialConditionMaker
from .boundary import BoundaryMarker, MaxGreedyBoundaryConditionMaker


class ConstantAdvectionDiffusionNoFluxModelMaker(AbstractMeshMaker, AbstractTimeMaker, AbstractEquationMaker):
    def __init__(self, diffusion_coefficient, sedimentation_velocity, flow_speed, width, height, total_time,
                 mesh_scaling=0.8):
        self.diffusion_coefficient = diffusion_coefficient
        self.sedimentation_velocity = sedimentation_velocity
        self.width = width
        self.height = height
        self.time = total_time
        self.mesh_scaling = mesh_scaling
        if self.sedimentation_velocity == 0:
            self.hy = self.height / 50  # completely arbitrary
        else:
            self.hy = 2.0 * self.diffusion_coefficient / self.sedimentation_velocity  # stability criteria for h for advection-diffusion equation
        # self.hy *= mesh_scaling # allows scaling of h (smaller = more refined mesh)
        if flow_speed == 0:
            self.hx = self.width / 20  # completely arbitrary
        else:
            self.hx = 2.0 * self.diffusion_coefficient / flow_speed
        # self.hx *= mesh_scaling
        self.advection_vector = as_vector((flow_speed, -1.0 * self.sedimentation_velocity))

    def setup_mesh(self, model: Model):
        # cell_culture = self.experimental_detail.cell_culture_condition
        max_pt = Point(self.width, self.height)
        mesh_pts_x = max(int(self.width / self.hx), 100) / self.mesh_scaling
        self.hx = self.width / mesh_pts_x
        mesh_pts_y = max(int(self.height / self.hy), 100) / self.mesh_scaling
        self.hy = self.height / mesh_pts_y
        mesh = RectangleMesh(Point(0.0, 0.0), max_pt, mesh_pts_x, mesh_pts_y)
        model.mesh = mesh
        model.width = self.width
        model.height = self.height

    def setup_time(self, model: Model):
        h = min(self.hx, self.hy)
        if self.sedimentation_velocity == 0:
            dt = h * h / 4.1  # dt stability criteria for diffusion equation
        else:
            dt = h * h / (2 * self.diffusion_coefficient)  # dt stability criteria for advection-diffusion equation

        dt *= 0.8  # to be safe
        timestep_maker = SpecifyMinimumTimestepMaker(dt, self.time)
        timestep_maker.setup_time(model)

    def setup_equations(self, model: Model):
        u = model.trial_function
        v = model.test_function

        D = Constant(self.diffusion_coefficient)
        diff_term = D * inner(nabla_grad(u), nabla_grad(v))

        sed_term = v * (dot(self.advection_vector, nabla_grad(u)))
        a_diff_sed = (u * v + model.dt * (diff_term + sed_term)) * dx

        unitNormal = FacetNormal(model.mesh)

        if dot(self.advection_vector, unitNormal) != 0:
            no_flux_term = model.dt * dot(self.advection_vector, unitNormal) * u * v * ds
            model.a = a_diff_sed - no_flux_term
        else:
            model.a = a_diff_sed
        model.L = v * model.u_init * dx


class ConstantAdvectionDiffusionNoFlux1DModelMaker(ConstantAdvectionDiffusionNoFluxModelMaker):
    def setup_mesh(self, model: Model):
        nx = ceil(self.height / self.hy)
        mesh_pts = int(max(nx, 100) / self.mesh_scaling)
        self.hy = self.height / mesh_pts
        # print('hy: {}'.format(self.hy))
        mesh = IntervalMesh(mesh_pts, 0, self.height)
        model.mesh = mesh
        model.height = self.height
        model.width = self.width

    def setup_equations(self, model: Model):
        u = model.trial_function
        v = model.test_function
        D = Constant(self.diffusion_coefficient)
        s = self.advection_vector[1]
        # s = 0

        diff_term = D * (u.dx(0) * v.dx(0))

        sed_term = v * (s * u.dx(0))
        a_diff_sed = (u * v + model.dt * (diff_term + sed_term)) * dx

        unitNormal = Expression('pow(x[0]-0.0, 2) < {0} ? -1.0 : 1.0'.format(DOLFIN_EPS), degree=1)

        if s != 0:
            no_flux_term = model.dt * s * unitNormal * u * v * ds
            model.a = a_diff_sed - no_flux_term
        else:
            model.a = a_diff_sed
        model.L = v * model.u_init * dx


def standard_constant_advection_diffusion_greedy_model_builder(experimental_detail: ExperimentalDetail,
                                                               model_maker_cls=ConstantAdvectionDiffusionNoFluxModelMaker,
                                                               mesh_scaling=0.8):
    model_maker = model_maker_cls(
        diffusion_coefficient=stokes_einstein_equation(experimental_detail),
        sedimentation_velocity=stokes_law(experimental_detail),
        flow_speed=experimental_detail.cell_culture_condition.flow_speed,
        width=experimental_detail.cell_culture_condition.width,
        height=experimental_detail.cell_culture_condition.height,
        total_time=experimental_detail.cell_culture_condition.time_in_seconds + 1, # simulation should run for slightly longer than experiment to allow interpolation
        mesh_scaling=mesh_scaling)
    initial_condition_maker = UniformInitialConditionMaker(
        experimental_detail.cell_culture_condition.particle_concentration)
    initial_condition_maker.initial_concentration = experimental_detail.cell_culture_condition.particle_concentration
    boundary_marker = BoundaryMarker(experimental_detail.cell_culture_condition.cell_orientation)
    boundary_condition_maker = MaxGreedyBoundaryConditionMaker()
    model_builder = ModelBuilder()
    model_builder.mesh_maker = model_maker
    model_builder.time_maker = model_maker
    model_builder.space_and_function_maker = CGSpaceAndFunctionMaker()
    model_builder.equation_maker = model_maker
    model_builder.boundary_marker = boundary_marker
    model_builder.boundary_condition_maker = boundary_condition_maker
    model_builder.initial_condition_maker = initial_condition_maker
    return model_builder


def stokes_einstein_equation(experimental_detail: ExperimentalDetail):
    n = experimental_detail.media.viscosity
    r = experimental_detail.particle.radius
    t = experimental_detail.cell_culture_condition.temperature
    kb = constants.k
    diffusion_coefficient = (kb * t) / (6 * pi * n * r)
    return diffusion_coefficient


def stokes_law(experimental_detail: ExperimentalDetail):
    r = experimental_detail.particle.radius
    d = experimental_detail.particle.density
    md = experimental_detail.media.density
    n = experimental_detail.media.viscosity
    g = constants.g
    sedimentation_velocity = (2 / 9) * ((d - md) / n) * g * r * r
    return sedimentation_velocity


