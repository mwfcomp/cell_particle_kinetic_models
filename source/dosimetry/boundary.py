from .model import AbstractBoundaryConditionMaker, AbstractBoundaryMarker, Model
from .experimental_detail import CellOrientation
from dolfin import *


class BoundaryMarker(AbstractBoundaryMarker):
    def __init__(self, cell_orientation):
        self.cell_orientation = cell_orientation

    def mark_boundaries(self, model: Model):
        co = self.cell_orientation
        x_axis = model.mesh.topology().dim() - 2
        y_axis = model.mesh.topology().dim() - 1

        if co == CellOrientation.bottom:
            cell_dist_fn = lambda x: abs(x[y_axis] - 0.0)
        elif co == CellOrientation.top:
            cell_dist_fn = lambda x: abs(x[y_axis] - model.height)
        elif co == CellOrientation.side:
            cell_dist_fn = lambda x: abs(x[x_axis] - 0.0)
        else:
            raise NotImplementedError('Unknown cell orientation')

        class CellBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and cell_dist_fn(x) < DOLFIN_EPS

        class NonCellBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return on_boundary and not (cell_dist_fn(x) < DOLFIN_EPS)

        boundary_parts = \
            MeshFunction("size_t", model.mesh, model.mesh.topology().dim() - 1)

        boundary_parts.set_all(self.NONCELL_BOUNDARY)
        cell_part = CellBoundary()
        cell_part.mark(boundary_parts, self.CELL_BOUNDARY)
        # non_cell_part = NonCellBoundary()
        # non_cell_part.mark(boundary_parts, 1)
        model.marked_boundaries = boundary_parts


class NoFluxBoundaryConditionMaker(AbstractBoundaryConditionMaker):
    def setup_boundaries(self, model: Model):
        return


class MaxGreedyBoundaryConditionMaker(AbstractBoundaryConditionMaker):
    def setup_boundaries(self, model: Model):
        u_boundary = Constant(0.0)
        bc = DirichletBC(model.function_space, u_boundary,
                         model.marked_boundaries, AbstractBoundaryMarker.CELL_BOUNDARY)
        model.dirichlet_bcs.append(bc)


class FluxFunctionCellBoundaryConditionMaker(AbstractBoundaryConditionMaker):
    def __init__(self, flux_function):
        self.flux_function = flux_function

    def setup_boundaries(self, model: Model):
        ds2 = Measure("ds", domain=model.mesh, subdomain_data=model.marked_boundaries)
        u = model.trial_function
        v = model.test_function
        boundary_flux_term = model.dt * self.flux_function(u) * v * ds2(AbstractBoundaryMarker.CELL_BOUNDARY)
        model.a += boundary_flux_term


class SurfaceCapacityFluxBoundaryConditionMaker(FluxFunctionCellBoundaryConditionMaker):
    def __init__(self, rate, surface_capacity):
        self.rate = rate
        self.surface_capacity = surface_capacity
        super().__init__(lambda u: self._flux_function(u))

    def _flux_function(self, u):
        return Constant(self.rate * self.surface_capacity) * u / (self.surface_capacity + u)


class SurfaceFluxTestingBoundaryConditionMaker(AbstractBoundaryConditionMaker):
    def __init__(self, rate, surface_capacity):
        self.rate = rate
        self.surface_capacity = surface_capacity

    def setup_boundaries(self, model: Model):
        ds2 = Measure("ds", domain=model.mesh, subdomain_data=model.marked_boundaries)
        u = model.trial_function
        u2 = Function(model.function_space)
        v = model.test_function
        fun_part = (u - 2.0 * u)
        boundary_flux_term = model.dt * fun_part * v * ds2(AbstractBoundaryMarker.CELL_BOUNDARY)

        # boundary_flux_term = model.dt * Constant(self.rate * self.surface_capacity) * u / (self.surface_capacity/2.0 + u) * v * ds2(AbstractBoundaryMarker.CELL_BOUNDARY)

        model.a += boundary_flux_term


class LinearFluxCellBoundaryConditionMaker(FluxFunctionCellBoundaryConditionMaker):
    def __init__(self, flux_rate):
        def flux_fn(u):
            return Constant(flux_rate) * u
        super().__init__(flux_fn)


class ConstantFluxCellBoundaryConditionMaker:
    def __init__(self, flux_amt):
        self.flux_amt = Constant(flux_amt)

    def setup_boundaries(self, model: Model):
        ds2 = Measure("ds", domain=model.mesh, subdomain_data=model.marked_boundaries)
        v = model.test_function
        boundary_flux_term = model.dt * self.flux_amt * v * ds2(AbstractBoundaryMarker.CELL_BOUNDARY)
        model.L -= boundary_flux_term
