from math import ceil
from dolfin import *
from dolfin.cpp.generation import IntervalMesh
from dosimetry.model import AbstractSpaceAndFunctionMaker, Model, AbstractTimeMaker, AbstractMeshMaker


class CGSpaceAndFunctionMaker(AbstractSpaceAndFunctionMaker):
    def __init__(self):
        pass

    def setup_space_and_functions(self, model: Model):
        model.function_space = FunctionSpace(model.mesh, 'CG', 1)
        model.trial_function = TrialFunction(model.function_space)
        model.test_function = TestFunction(model.function_space)


class NonLinearCGSpaceAndFunctionMaker(AbstractSpaceAndFunctionMaker):
    def __init__(self):
        pass

    def setup_space_and_functions(self, model: Model):
        model.function_space = FunctionSpace(model.mesh, 'CG', 1)
        model.trial_function = Function(model.function_space)
        model.test_function = TestFunction(model.function_space)


class NonLinearSomethingSpaceAndFunctionMaker(AbstractSpaceAndFunctionMaker):
    def __init__(self):
        pass

    def setup_space_and_functions(self, model: Model):
        model.function_space = FunctionSpace(model.mesh, 'P', 1)
        model.trial_function = Function(model.function_space)
        model.test_function = TestFunction(model.function_space)



class SpecifyMinimumTimestepMaker(AbstractTimeMaker):
    def __init__(self, minimum_timestep, time):
        self.minimum_timestep = minimum_timestep
        self.time = time

    def setup_time(self, model: Model):
        steps = self.time / self.minimum_timestep
        steps = max(20.0, steps)  # always do something
        steps = ceil(steps)  # insure an integer number of steps
        model.dt = self.time / steps
        model.total_time = self.time


class FractionalTimestepMaker(AbstractTimeMaker):
    def __init__(self, timestep_fraction, timestep_maker: AbstractTimeMaker):
        self.timestep_fraction = timestep_fraction
        self.timestep_maker = timestep_maker

    def setup_time(self, model: Model):
        self.timestep_maker.setup_time(model)
        steps = model.total_time / model.dt
        steps /= self.timestep_fraction
        steps = ceil(steps)
        new_dt = model.total_time / steps
        new_timestep_maker = SpecifyMinimumTimestepMaker(new_dt, model.total_time)
        new_timestep_maker.setup_time(model)


class MaximumTimestepTimeMaker(AbstractTimeMaker):
    def __init__(self, max_dt, total_time):
        self.max_dt = max_dt
        self.total_time = total_time

    def setup_time(self, model: Model):
        steps = self.total_time / self.max_dt
        steps = ceil(steps)  # insure an integer number of steps
        model.dt = self.total_time / steps
        model.total_time = self.total_time


class FractionalMeshMaker1D(AbstractMeshMaker):
    def __init__(self, mesh_fraction, mesh_maker: AbstractMeshMaker):
        self.mesh_fraction = mesh_fraction
        self.mesh_maker = mesh_maker

    def setup_mesh(self, model: Model):
        self.mesh_maker.setup_mesh(model)
        old_hy = model.height / len(model.mesh.cells())
        hy = old_hy * self.mesh_fraction
        nx = ceil(model.height / hy)
        model.mesh = IntervalMesh(max(int(nx), 100), 0, model.height)
