from typing import List, Tuple

from scipy.integrate import odeint
from dosimetry.experimental_detail import CellCultureCondition, CellOrientation, ExperimentalAndCellDetail
import numpy as np
from dosimetry.solver.standard import Solution
from dosimetry.solver.three_compartment import AssociationInternalizationSolution
from dosimetry.solver.updating_boundary import AbstractRateCalculator


class OdeModel:
    def __init__(self, initial_amount, derivatives_fn):
        self.initial_amount = initial_amount
        self.derivatives_fn = derivatives_fn


def solve_ode_model(model: OdeModel, time_pts: [int]):
    amts = odeint(model.derivatives_fn, model.initial_amount, time_pts)
    amts = np.reshape(amts, (len(amts),))
    return model.initial_amount - amts


def get_ode_model_solutions(model: OdeModel, time_pts: List[int]) -> List[
    Tuple[float, AssociationInternalizationSolution]]:
    associated_amounts = solve_ode_model(model, time_pts)
    solutions = [AssociationInternalizationSolution(starting_amount=model.initial_amount,
                                                    associated_amount=amt,
                                                    internalized_amount=0) for amt in associated_amounts]
    return list(zip(time_pts, solutions))


# carrying capacity, as change to amount IN SOLUTION
# rate is governed by amount OUT OF SOLUTION
#current amount is amount IN SOLUTION, amount removed is amount OUT OF SOLUTION
def get_carrying_capacity_fn(max_rate: float, capacity: float, initial_amount: float):
    def get_rate(current_amt):
        amount_removed = initial_amount - current_amt
        rate = -1 * max_rate * (capacity - amount_removed) / capacity
        return rate * current_amt

    return get_rate


def get_well_mixed_adherent_scaling_factor(culture):
    if culture.cell_orientation == CellOrientation.side:
        scaling_factor = 1 / culture.width
    else:
        scaling_factor = 1 / culture.height  # particles on bottom or top
    return scaling_factor


def get_well_mixed_suspension_scaling_factor(culture, cell_info):
    cell_surface = cell_info.total_cells * cell_info.surface_area_per_cell
    scaling_factor = cell_surface / culture.volume
    return scaling_factor


def well_mixed_adherent_carrying_capacity_model(culture: CellCultureCondition, rate, capacity):
    scaling_factor = get_well_mixed_adherent_scaling_factor(culture)
    return carrying_capacity_model(culture, scaling_factor * rate, capacity)


def well_mixed_suspension_carrying_capacity_model(culture, cell_info, rate, capacity):
    scaling_factor = get_well_mixed_suspension_scaling_factor(culture, cell_info)
    return carrying_capacity_model(culture, scaling_factor * rate, capacity)


def carrying_capacity_model(culture, rate, capacity):
    initial_amount = culture.particle_concentration * culture.width * culture.height
    cap_fn = get_carrying_capacity_fn(rate, capacity, initial_amount)
    derivatives_fn = lambda amt, time: cap_fn(amt)
    return OdeModel(initial_amount=initial_amount, derivatives_fn=derivatives_fn)


def linear_model(culture, rate):
    initial_amount = culture.particle_concentration * culture.width * culture.height
    derivative_fn = lambda amt_in_solution, time: -1 * rate * amt_in_solution
    return OdeModel(initial_amount=initial_amount, derivatives_fn=derivative_fn)
