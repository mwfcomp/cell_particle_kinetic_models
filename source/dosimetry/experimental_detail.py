from math import pi
from enum import Enum

numeric_type = float


class PropertyComparisonClass:
    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class SphericalParticle(PropertyComparisonClass):
    def __init__(self, name: str, radius: numeric_type, density: numeric_type):
        self.name = name
        self.radius = numeric_type(radius)
        self.density = numeric_type(density)
        self.volume = pow(self.radius, 3) * 4 / 3 * pi
        self.surface_area = 4 * pi * self.radius * self.radius
        self.mass = self.volume * self.density


class LiquidMedia(PropertyComparisonClass):
    def __init__(self, viscosity: numeric_type, density: numeric_type):
        self.viscosity = viscosity
        self.density = density


class CellOrientation(Enum):
    bottom = standard = 1
    top = inverted = 2
    side = vertical = 3


class CellCultureCondition(PropertyComparisonClass):
    def __init__(self, width: numeric_type, height: numeric_type, cell_side_surface_area: numeric_type,
                 volume: numeric_type, temperature: numeric_type, time_in_seconds: numeric_type,
                 particle_concentration: numeric_type, cell_orientation: CellOrientation = CellOrientation.bottom,
                 flow_speed: numeric_type = 0):
        self.width = width
        self.height = height
        self.cell_side_surface_area = cell_side_surface_area
        self.volume = volume
        self.temperature = temperature
        self.time_in_seconds = time_in_seconds
        self.particle_concentration = particle_concentration
        self.cell_orientation = cell_orientation
        self.flow_speed = flow_speed


class CylindricalCellCultureCondition(CellCultureCondition):
    def __init__(self, width, height, temperature, time_in_seconds, cell_orientation,
                 particle_concentration, flow_speed):
        volume = pi * width / 2 * width / 2 * height
        if cell_orientation != CellOrientation.side:
            cell_side_surface_area = volume / height
        else:
            cell_side_surface_area = volume / width
        super().__init__(width, height, cell_side_surface_area, volume, temperature, time_in_seconds,
                         particle_concentration, cell_orientation, flow_speed)


class RectangularCellCultureCondition(CellCultureCondition):
    def __init__(self, width, height, depth, temperature, time_in_seconds,
                 particle_concentration, cell_orientation, flow_speed):
        if cell_orientation != CellOrientation.side:
            cell_side_surface_area = width * depth
        else:
            cell_side_surface_area = width * height
        volume = width * height * depth
        super().__init__(width, height, cell_side_surface_area, volume, temperature, time_in_seconds,
                         particle_concentration, cell_orientation, flow_speed)


class CellInfo(PropertyComparisonClass):
    def __init__(self, cell_line: str, total_cells: float, surface_area_per_cell: float):
        self.cell_line = cell_line
        self.total_cells = total_cells
        self.surface_area_per_cell = surface_area_per_cell


class ExperimentalDetail(PropertyComparisonClass):
    def __init__(self, particle: SphericalParticle, media: LiquidMedia, cell_culture_condition: CellCultureCondition):
        self.particle = particle
        self.media = media
        self.cell_culture_condition = cell_culture_condition


class ExperimentalAndCellDetail(ExperimentalDetail):
    def __init__(self, particle: SphericalParticle, media: LiquidMedia, cell_culture_condition: CellCultureCondition,
                 cell_info: CellInfo):
        super().__init__(particle, media, cell_culture_condition)
        self.cell_info = cell_info

# standard_cell_media = LiquidMedia(viscosity=.00101, density=1000)

# corning_6_well_standard_plate_condition = \
#     StaticCylinderCellCutureCondition(width=0.0348, height=.0053, cell_orientation=CellOrientation.bottom)
#
# corning_12_well_standard_plate_condition= \
#     StaticCylinderCellCutureCondition(width=0.0221, height=.0053, cell_orientation=CellOrientation.bottom)
#
# corning_24_well_standard_plate_condition = \
#     StaticCylinderCellCutureCondition(width=0.0156, height=.0053, cell_orientation=CellOrientation.bottom)
#
# corning_48_well_standard_plate_condition = \
#     StaticCylinderCellCutureCondition(width=0.011, height=.0053, cell_orientation=CellOrientation.bottom)
#
# corning_96_well_standard_plate_condition = \
#     StaticCylinderCellCutureCondition(width=0.0064, height=.0053, cell_orientation=CellOrientation.bottom)
