class Model:
    def __init__(self):
        self.mesh = None
        self.width = None
        self.height = None

        self.dt = None
        self.total_time = None
        self.function_space = None
        self.trial_function = None
        self.test_function = None

        self.marked_boundaries = None
        self.u_init = None
        self.a = None
        self.L = None
        self.dirichlet_bcs = []


class ModelBuilder:
    def __init__(self):
        self.mesh_maker = AbstractMeshMaker()
        self.space_and_function_maker = AbstractSpaceAndFunctionMaker()
        self.time_maker = AbstractTimeMaker()
        self.initial_condition_maker = AbstractInitialConditionMaker()
        self.equation_maker = AbstractEquationMaker()
        self.boundary_marker = AbstractBoundaryMarker()
        self.boundary_condition_maker = AbstractBoundaryConditionMaker()

    def build_model(self) -> Model:
        model = Model()
        self.mesh_maker.setup_mesh(model)
        self.space_and_function_maker.setup_space_and_functions(model)
        self.time_maker.setup_time(model)
        self.initial_condition_maker.setup_initial_conditions(model)
        self.boundary_marker.mark_boundaries(model)
        self.equation_maker.setup_equations(model)
        self.boundary_condition_maker.setup_boundaries(model)
        return model


class AbstractMeshMaker:
    def setup_mesh(self, model: Model) -> None:
        raise NotImplementedError


class AbstractSpaceAndFunctionMaker:
    def setup_space_and_functions(self, model: Model) -> None:
        raise NotImplementedError


class AbstractTimeMaker:
    def setup_time(self, model: Model) -> None:
        raise NotImplementedError


class AbstractInitialConditionMaker:
    def setup_initial_conditions(self, model: Model) -> None:
        raise NotImplementedError


class AbstractEquationMaker:
    def setup_equations(self, model: Model) -> None:
        raise NotImplementedError


class AbstractBoundaryMarker:
    CELL_BOUNDARY = 0
    NONCELL_BOUNDARY = 1

    def mark_boundaries(self, model: Model) -> None:
        raise NotImplementedError


class AbstractBoundaryConditionMaker:
    def setup_boundaries(self, model: Model) -> None:
        raise NotImplementedError
