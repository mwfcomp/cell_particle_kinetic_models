from .model import AbstractInitialConditionMaker, Model
from dolfin import *


class UniformInitialConditionMaker(AbstractInitialConditionMaker):
    def __init__(self, initial_concentration):
        self.initial_concentration = initial_concentration

    def setup_initial_conditions(self, model: Model):
        u_init = Constant(self.initial_concentration)
        model.u_init = interpolate(u_init, model.function_space)


class MiddleClumpInitialConditionMaker(AbstractInitialConditionMaker):
    def __init__(self, clump_concentration: float):
        self.relative_width = 1 / 40
        self.relative_x = 1 / 2
        self.relative_y = 1 / 2
        self.clump_concentration = clump_concentration

    def setup_initial_conditions(self, model: Model):
        width = model.width * self.relative_x
        height = model.height * self.relative_y
        diameter = model.width * model.height * self.relative_width
        is_1D = model.mesh.topology().dim() == 1
        if is_1D:
            u_init = Expression('pow(x[0]-{0}, 2) < {1} ? {2} : 0.0'.format(height, diameter, self.clump_concentration),
                                degree=1)
        else:
            u_init = Expression('pow(x[0]-{0}, 2)+pow(x[1]-{1}, 2) < {2} ? {3} : 0.0'.
                                format(width, height, diameter, self.clump_concentration), degree=1)
        model.u_init = interpolate(u_init, model.function_space)


class SpindlyTowerInitialConditionMaker(AbstractInitialConditionMaker):
    def __init__(self, concentration: float):
        self.concentration = concentration
        self.relative_tower_width = 1 / 30
        self.relative_tower_height = 1 / 4
        self.relative_x = 1 / 2

    def setup_initial_conditions(self, model: Model):
        width_start = model.width * (self.relative_x - self.relative_tower_width)
        width_end = model.width * (self.relative_x + self.relative_tower_width)
        height = model.height * self.relative_tower_height
        # diameter = model.width * model.height * self.relative_width
        u_init = Expression('(x[1] > {} || (x[0] > {} && x[0] < {})) ? {} : 0.0'.format(height,
                                                                                        width_start,
                                                                                        width_end,
                                                                                        self.concentration))
        model.u_init = interpolate(u_init, model.function_space)


class RaisedUniformInitialConditionMaker(AbstractInitialConditionMaker):
    def __init__(self, concentration: float):
        self.concentration = concentration
        self.relative_height = 1 / 4

    def setup_initial_conditions(self, model: Model):
        height = model.height * self.relative_height
        # diameter = model.width * model.height * self.relative_width
        u_init = Expression('(x[1] > {}) ? {} : 0.0'.format(height, self.concentration))
        model.u_init = interpolate(u_init, model.function_space)
