from math import sqrt

from dolfin import assemble, dx

from dosimetry.solver.standard import Solution


class AssociationInternalizationSolution(Solution):
    def __init__(self, starting_amount, associated_amount, internalized_amount):
        removed_amount = associated_amount + internalized_amount
        self.starting_amount = starting_amount
        self.absolute_amount_associated = associated_amount
        self.absolute_amount_internalized = internalized_amount
        self.relative_amount_associated = associated_amount / starting_amount
        self.relative_amount_internalized = internalized_amount / starting_amount
        super().__init__(starting_amount, starting_amount - removed_amount)

    def __str__(self):
        return "Remaining :{:.2f}({:.2e}), Associated: {:.2f}({:.2e}), Internalized: {:.2f}({:.2e})".format(
                self.relative_amount_remaining,
                self.absolute_amount_remaining,
                self.relative_amount_associated,
                self.absolute_amount_associated,
                self.relative_amount_internalized,
                self.absolute_amount_internalized)



class AbstractInternalizationRate:
    def get_associated_amount(self, u_remaining, u_last_remaining, u_last_associated, dt):
        raise NotImplementedError

    def get_internalization_rate(self, associated_amount):
        raise NotImplementedError


def association_internalization_solver(solver, pathway=None):
    class AssociationInternalizationSolver(solver):
        nonlocal pathway
        pathway = pathway
        associated_start = 0
        internalized_start = 0

        # def __init__(self, *args, **kwargs):
        #     self.associated = 0
        #     self.internalized = 0
        #     super().__init__(*args, **kwargs)

        def init_solver_loop(self, model):
            super().init_solver_loop(model)
            self.associated = self.associated_start
            self.internalized = self.internalized_start

        def solve_timestep(self, model):
            super().solve_timestep(model)
            last_remaining = assemble(self._u_1 * dx)
            remaining = assemble(self._u * dx)

            self.associated = self.pathway.get_associated_amount(remaining, last_remaining, self.associated, model.dt)
            self.internalized = model.dt * self.pathway.get_internalization_rate(self.associated) + self.internalized

        def get_solution(self):
            end_amt = assemble(self._u * dx)
            total_start = self._start_amount + self.associated_start + self.internalized_start
            total_end = end_amt + self.associated + self.internalized
            unaccounted_for = total_start - total_end
            if abs(unaccounted_for) > total_start / 1000000:
                raise ValueError('Unaccounted for mass in system!')

            solution = AssociationInternalizationSolution(self._start_amount, self.associated, self.internalized)
            return solution

    return AssociationInternalizationSolver


class LinearInternalizationRate(AbstractInternalizationRate):
    def __init__(self, rate):
        self.rate = rate

    def get_associated_amount(self, u_remaining, u_last_remaining, u_last_associated, dt):
        return (u_last_remaining - u_remaining + u_last_associated) / (1 + dt * self.rate)
        # for constant flux: return (u_last_remaining - u_remaining + u_last_associated) - (dt * self.rate)

    def get_internalization_rate(self, associated_amount):
        return self.rate * associated_amount


class MichaelisMentenInternalizationRate(AbstractInternalizationRate):
    def __init__(self, rate_max, half_max_conc):
        self.rate_max = rate_max
        self.half_max_conc = half_max_conc

    def get_associated_amount(self, u_remaining, u_last_remaining, u_last_associated, dt):
        # TODO:  I think this is wrong / has a bug, Check
        b = self.half_max_conc + dt * self.rate_max + u_remaining - u_last_remaining - u_last_associated
        c = self.half_max_conc * (u_remaining - u_last_remaining - u_last_associated)
        plus = (-b + sqrt(b * b - 4 * c)) / 2.0
        minus = (-b - sqrt(b * b - 4 * c)) / 2.0
        if plus > 0 and minus > 0:
            raise ValueError("Can't determine internalization rate")
        if plus < 0 and minus < 0:
            raise ValueError("Negative internalization rate")
        return max(plus, minus)

    def get_internalization_rate(self, associated_amount):
        return self.rate_max * associated_amount / (self.half_max_conc + associated_amount)
