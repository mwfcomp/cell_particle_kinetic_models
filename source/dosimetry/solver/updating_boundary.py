from dolfin import assemble, parameters, Form
from dolfin.cpp.function import Constant
from ufl import form

from dosimetry.boundary import ConstantFluxCellBoundaryConditionMaker, FluxFunctionCellBoundaryConditionMaker
from dosimetry.model import Model, AbstractEquationMaker, AbstractBoundaryConditionMaker
from dosimetry.solver.standard import Solution
from dosimetry.solver.three_compartment import AssociationInternalizationSolution
from dosimetry.boundary import LinearFluxCellBoundaryConditionMaker
from dosimetry.experimental_detail import CellCultureCondition, CellInfo, SphericalParticle

parameters['form_compiler']['optimize'] = True
parameters['form_compiler']['cpp_optimize'] = True


class AbstractSolutionDependentBoundaryConditionMaker:
    def update_boundaries(self, solution: Solution, model: Model):
        raise NotImplementedError


# WARNING! Need to set timestep appropriately or can get bad behavior
def updating_linear_boundary_solver(solver, equation_maker: AbstractEquationMaker = None,
                                    bc_updater: AbstractSolutionDependentBoundaryConditionMaker = None):
    class UpdatingLinearBoundaryTimestepModelSolver(solver):
        nonlocal equation_maker, bc_updater
        equation_maker = equation_maker
        bc_updater = bc_updater

        def solve_timestep(self, model):
            super().solve_timestep(model)
            self.current_solution = self.get_solution()

        def loop_update(self, model):
            self.equation_maker.setup_equations(model)
            self.bc_updater.update_boundaries(self.current_solution, model)
            self.A = assemble(model.a)
            super().loop_update(model)

    return UpdatingLinearBoundaryTimestepModelSolver


# WARNING! Need to set timestep appropriately or can get bad behavior
def updating_nonlinear_boundary_solver(solver, equation_maker: AbstractEquationMaker = None,
                                       bc_updater: AbstractSolutionDependentBoundaryConditionMaker = None):
    class UpdatingNonlinearBoundaryTimestepModelSolver(solver):
        nonlocal equation_maker, bc_updater
        equation_maker = equation_maker
        bc_updater = bc_updater

        def solve_timestep(self, model):
            super().solve_timestep(model)
            self.current_solution = self.get_solution()

        def loop_update(self, model):
            self.equation_maker.setup_equations(model)
            self.bc_updater.update_boundaries(self.current_solution, model)
            super().loop_update(model)

    return UpdatingNonlinearBoundaryTimestepModelSolver


class AbstractRateCalculator:
    def calculate_rate(self, solution):
        raise NotImplementedError


class CellCarryingCapacityAssociationRate(AbstractSolutionDependentBoundaryConditionMaker, AbstractBoundaryConditionMaker, AbstractRateCalculator):
    def __init__(self, rate, capacity):
        self.rate = rate
        self.capacity = capacity

    def calculate_rate(self, solution):
        n = solution.absolute_amount_removed
        # Warning - this does not have checks in place for incorrect rates, that are too high, negative, etc
        return self.rate * (self.capacity - n) / self.capacity

    def update_boundaries(self, solution: AssociationInternalizationSolution, model: Model):
        rate = self.calculate_rate(solution)
        bc_maker = LinearFluxCellBoundaryConditionMaker(rate)
        bc_maker.setup_boundaries(model)

    def setup_boundaries(self, model: Model):
        none_associated_solution = AssociationInternalizationSolution(100.0, 0, 0) #100 is arbitrary
        self.update_boundaries(none_associated_solution, model)



class CellAndSurfaceCapacityFluxAssociationRate(AbstractSolutionDependentBoundaryConditionMaker, AbstractBoundaryConditionMaker):
    def __init__(self, rate, cell_capacity, surface_capacity):
        self.rate = rate
        self.cell_capacity = cell_capacity
        self.surface_capacity = surface_capacity

    def _get_flux(self, solution, u):
        n = solution.absolute_amount_removed
        cc = self.cell_capacity
        sc = self.surface_capacity
        # flux = Constant(self.rate * sc * (cc - n) / cc) * u / (sc/2.0 + u)
        flux = Constant(self.rate * sc * (cc - n) / cc) * u / (sc + u)
        return flux

    def update_boundaries(self, solution: AssociationInternalizationSolution, model: Model):
        flux_fn = lambda u: self._get_flux(solution, u)
        bc_maker = FluxFunctionCellBoundaryConditionMaker(flux_fn)
        bc_maker.setup_boundaries(model)

    def setup_boundaries(self, model: Model):
        none_associated_solution = AssociationInternalizationSolution(100.0, 0, 0) #100 is arbitrary
        self.update_boundaries(none_associated_solution, model)


class ZeroOverCapacityRateCell(CellCarryingCapacityAssociationRate, AbstractRateCalculator):
    def __init__(self, wrapped_rate: CellCarryingCapacityAssociationRate):
        self.wrapped_rate = wrapped_rate

    def calculate_rate(self, solution):
        rate = self.wrapped_rate.calculate_rate(solution)
        if rate < 0.0:
            return 0.0
        return rate

    def update_boundaries(self, solution: AssociationInternalizationSolution, model: Model):
        return self.wrapped_rate.update_boundaries(solution, model)


class SafeRateCell(CellCarryingCapacityAssociationRate, AbstractRateCalculator):
    def __init__(self, wrapped_rate: CellCarryingCapacityAssociationRate):
        self.wrapped_rate = wrapped_rate

    def calculate_rate(self, solution):
        return self.wrapped_rate.calculate_rate(solution)

    def get_rate_correction(self, model, amount_over):
        rate = -1.0 * amount_over / model.width / model.dt
        return rate

    def update_boundaries(self, solution: AssociationInternalizationSolution, model: Model):
        rate = self.calculate_rate(solution)
        if rate >= 0.0:
            self.wrapped_rate.update_boundaries(solution, model)
        else:
            amount_over = solution.absolute_amount_removed - self.wrapped_rate.capacity
            rate = self.get_rate_correction(model, amount_over)
            bc_maker = ConstantFluxCellBoundaryConditionMaker(rate)
            bc_maker.setup_boundaries(model)


class SafeRate1D(SafeRateCell):
    def get_rate_correction(self, model, amount_over):
        rate = -1.0 * amount_over / model.dt
        return rate
