from dosimetry.model_maker import *


class Solution:
    def __init__(self, starting_amount, ending_amount):
        # todo: add mesh solutions? timepoints?
        self.absolute_amount_removed = starting_amount - ending_amount
        self.absolute_amount_remaining = ending_amount
        self.relative_amount_removed = self.absolute_amount_removed / starting_amount
        self.relative_amount_remaining = self.absolute_amount_remaining / starting_amount

    def __str__(self):
        return "Remaining :{:.2f}({:.2e}), Removed: {:.2f}({:.2e})".format(
            self.relative_amount_remaining,
            self.absolute_amount_remaining,
            self.relative_amount_removed,
            self.absolute_amount_removed)


class AbstractModelSolver:
    def solve_model(self, model: Model) -> Solution:
        raise NotImplementedError


class TimestepLinearModelSolver:
    def __init__(self):
        self._u = None
        self.A = None
        self._b = None
        self._current_time = None
        self._u_1 = None
        self._start_amount = 0
        self.solver_parameters = {}

    def solve_model(self, model: Model) -> Solution:
        self.init_solver_loop(model)
        while self.loop_condition(model):
            self.solve_timestep(model)
            self.loop_update(model)
        self.end_loop(model)
        return self.get_solution()

    def init_solver_loop(self, model):
        self._u = Function(model.function_space)
        # time of simulation
        self.A = assemble(model.a)
        self._b = None  # necessary for memory saving assemble call
        self._current_time = 0
        self._u_1 = model.u_init
        self._start_amount = assemble(self._u_1 * dx)
        self._u.assign(self._u_1)

    def loop_condition(self, model):
        return self._current_time < model.total_time - model.dt / 1000  # fudge factor for float operations

    def solve_timestep(self, model):
        self._current_time += model.dt
        self._b = assemble(model.L, tensor=self._b)

        for bc in model.dirichlet_bcs:
            bc.apply(self.A, self._b)
        solve(self.A, self._u.vector(), self._b, 'lu')

    def loop_update(self, model):
        self._u_1.assign(self._u)

    def end_loop(self, model):
        return

    def get_solution(self):
        end_amt = assemble(self._u * dx)
        solution = Solution(self._start_amount, end_amt)
        return solution


class TimestepNonLinearModelSolver(TimestepLinearModelSolver):
    def init_solver_loop(self, model):
        self._u = model.trial_function
        # time of simulation
        self._b = None  # necessary for memory saving assemble call
        self._current_time = 0
        self._u_1 = model.u_init
        self._start_amount = assemble(self._u_1 * dx)
        self._u.assign(self._u_1)


    def solve_timestep(self, model):
        self._current_time += model.dt
        F = model.a - model.L
        solve(F == 0, model.trial_function, model.dirichlet_bcs, form_compiler_parameters={"optimize": True})
        # solve(F == 0, model.trial_function, model.dirichlet_bcs, solver_parameters={"newton_solver": {"report": True}},
        #       form_compiler_parameters={"optimize": True})



def calculate_and_display_flux(model: Model, u):
    n = FacetNormal(model.mesh)
    flux = dot(nabla_grad(u), n) * ds
    total_flux = assemble(flux)
    print('Total flux: {}'.format(total_flux))
