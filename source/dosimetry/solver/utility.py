from dolfin import plot, assemble, dx
from tqdm import tqdm

from dosimetry.solver.standard import Solution


def graph(solver):
    class Graph(solver):
        max_range = 1.0

        def init_solver_loop(self, model):
            super().init_solver_loop(model)
            plot(self._u, range_min=0., range_max=self.max_range, title='Time: {}'.format(self._current_time),
                 elevate=0.0)

        def loop_update(self, model):
            super().loop_update(model)
            current_amt = assemble(self._u * dx)
            current_sol = Solution(self._start_amount, current_amt)
            title = 'Time: {0:.0f}, Amount removed: {1:.4f}'.format(self._current_time,
                                                                    current_sol.relative_amount_removed)
            plot(self._u, range_min=0., range_max=self.max_range, title=title)

    return Graph


def solution_report(solver):
    class SolutionReport(solver):
        def solve_timestep(self, model):
            super().solve_timestep(model)
            current_solution = self.get_solution()
            print(current_solution)

    return SolutionReport


def progress_bar(solver):
    class ProgressBar(solver):
        bar = None

        def init_solver_loop(self, model):
            super().init_solver_loop(model)
            self.bar = tqdm(total=int(model.total_time / model.dt))

        def solve_timestep(self, model):
            super().solve_timestep(model)
            self.bar.update(1)

        def end_loop(self, model):
            super().end_loop(model)
            self.bar.close()

    return ProgressBar


def solution_saver(solver):
    class SolutionSaver(solver):

        def init_solver_loop(self, model):
            super().init_solver_loop(model)
            self.solutions = [(self._current_time, self.get_solution())]

        def loop_update(self, model):
            self.solutions += [(self._current_time, self.get_solution())]
            super().loop_update(model)

            # def end_loop(self, model):
            # self.solutions += [(self._current_time, self.get_solution())]
            # super().end_loop(model)

    return SolutionSaver