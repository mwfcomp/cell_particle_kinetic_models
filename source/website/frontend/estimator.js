$(function() {
    $('.tabular.menu .item').tab();
    $('.add_timepoint').click(add_time_point);
    $('.remove_timepoint').click(remove_time_point);
    $('.cell_experiment .results').append($('.templates .results').clone(true));
    $('.cell_experiment').append($('.templates .calculate').clone(true));
    update_form_validation();

    $('form .add_timepoint').trigger('click');
    $('.ui .time input').last().attr('value', 0)
    $('.ui .particles input').last().attr('value', 0)
    for (var i = 1; i < 6; i++) {
        $('form .add_timepoint').trigger('click');
        $('.ui .time input').last().attr('value', 2**(i-1)*60);
        $('.ui .particles input').last().attr('value', 10*i);
    }
    $('.label').popup();
//    add_form_errors();
});

add_form_errors = function() {
    error_param = URI.parseQuery(URI.parse(document.URL).query).errors;
    error_fields = error_param.split(',');
};

$.fn.form.settings.rules.in_range = function(value, range) {
    parts = range.split('..', 2);
    min = parseFloat(parts[0]);
    max = parseFloat(parts[1]);
    val = parseFloat(value);
    if (isNaN(min) || isNaN(max) || isNaN(val)) {
        return false;
    }
    return (min <= val) && (val <= max)
};

$.fn.form.settings.rules.greater_than_other_field = function(value, identifier) {
    lesser_val = $('[name="' + identifier +'"]').val();
    lesser_val = parseFloat(lesser_val);
    val = parseFloat(value);
    if (isNaN(lesser_val)) {
        return true;
    }
    if (isNaN(val)) {
        return false;
    }
    return lesser_val < val;
};

$.fn.form.settings.rules.single_or_comma_separated_vals = function(value, unused) {
    value = value.replace(' ', '');
    parts = value.split(',');
    if (parts.length < 1) {
        return false;
    }

    for(var i=0; i<parts.length; i++) {
        val = parseFloat(parts[i]);
        if (isNaN(val) || parts[0].startsWith('.')) {
            return false;
        }
    }
    return true;
};

update_form_validation = function () {
    field_validation = {
        particle_name : {
            identifier: 'particle_name',
            rules : [{type: 'empty', prompt: 'Please provide a particle name'}]
        },
        cell_line_name : {
            identifier: 'cell_line_name',
            rules : [{type: 'empty', prompt: 'Please provide a cell line name'}]
        },
        particle_radius : {
            identifier: 'particle_radius',
            rules : [{type: 'in_range[1e-9..1e-5]', prompt: 'Particle size must be between 1e-9 and 1e-5'}]
        },
        particle_density : {
            identifier: 'particle_density',
            rules : [{type: 'greater_than_other_field[media_density]', prompt: 'Particle density must be greater than media density '}]
        },
        culture_width : {
            identifier: 'culture_width',
            rules : [{type: 'in_range[1e-5..0.1]', prompt: 'Plate width must be between 10 um and 10cm'}]
        },
        culture_height : {
            identifier: 'culture_height',
            rules : [{type: 'in_range[1e-5..0.1]', prompt: 'Media height must be between 10 um and 10cm'}]
        },
        temperature : {
            identifier : 'temperature',
            rules : [{type: 'in_range[1..500]', prompt: 'Temperature must be between 1K and 500K'}]
        },
        media_viscosity : {
            identifier : 'media_viscosity',
            rules : [{type: 'in_range[1e-4..0.1]', prompt: 'Media viscosity must be between 1e-4 and 0.1'}]
        },
        media_density : {
            identifier : 'media_density',
            rules : [{type: 'in_range[100..10000]', prompt: 'Media density must be between 100 and 10000'}]
        },
        particle_concentration : {
            identifier : 'particle_concentration',
            rules : [{type: 'in_range[1..1e7]', prompt: 'Initial concentration must be between 1 and 1e7 particles per cell'}]
        },
        total_cells : {
            identifier : 'total_cells',
            rules : [{type: 'in_range[1..1e7]', prompt: 'Total cells must be between 1 and 1e7'}]
        },
        cell_surface_area : {
            identifier : 'cell_surface_area',
            rules : [{type: 'in_range[1e-10..2e-2]', prompt: 'Cell surface area must be between 1e-10 and 2e-2'}]
        },
        max_capacity : {
            identifier : 'max_capacity',
            rules : [{type: 'in_range[0..1e10]', prompt: 'Max capacity must be between 0 and 1e10 particles'}]
        }
    }
    //Technically this adds it to many more than just the "needed" ones - counts ALL timepoints, not just points on form
    //no big deal though
    time_pts = ('.timepoint').length;
    for(i=0; i< time_pts+1; i++) {
        time_loop_id = 'time' + i.toString();
        particle_loop_id = 'particle' + i.toString();
        field_validation[time_loop_id] = {
            identifier: time_loop_id,
            rules : [{type: 'in_range[0..1e10]', prompt: 'Please provide an appropriate time value for time ' + i.toString() }]};
        field_validation[particle_loop_id] = {
            identifier: particle_loop_id,
            rules : [{type: 'single_or_comma_separated_vals', prompt: 'Particles per cell ' + i.toString() + ' must be a single value, or a comma separated list of values. All values must start with a decimal digit'}]};
    }

    //add validation to all forms, may need to change at some point
    $('.form').form({
        fields: field_validation
    });
};

add_time_point = function() {
    time_pt_template = $('.templates .timepoint').clone();
    id = $(this).parent().children('.timepoint').length;
    time_input = time_pt_template.find('.time input').first();
    time_id = 'time' + id.toString();
    time_input.attr('name', time_id);
    particle_input = time_pt_template.find('.particles input').first();
    particle_id = 'particle' + id.toString();
    particle_input.attr('name', particle_id);
    $(this).before(time_pt_template);
    update_form_validation();
    return id;
};

remove_time_point = function() {
    timepoints = $(this).parent().children('.timepoint')
    if(timepoints.length > 3) {
        timepoints.last().remove();
        update_form_validation();
    }
};