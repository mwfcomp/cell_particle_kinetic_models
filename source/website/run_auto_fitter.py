from fitter import auto_fitter
import sys
import time


def continuously_monitor_folder(secs_between_checks):
    while True:
        print('monitoring folder....')
        auto_fitter.process_all_in_folder()
        time.sleep(secs_between_checks)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: {} <seconds_between_checks> <folder_name>'.format(sys.argv[0]))
        exit()

    secs_between_checks = int(sys.argv[1])
    folder_name = sys.argv[2]
    auto_fitter.monitored_folder = folder_name
    print('beginning monitoring of {}'.format(auto_fitter.monitored_folder))
    continuously_monitor_folder(secs_between_checks)
