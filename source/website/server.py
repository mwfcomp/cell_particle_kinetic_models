from flask import Flask, send_from_directory, request, redirect, render_template, abort
from werkzeug.utils import secure_filename
import configparser
import uuid
import os.path
from decimal import *
from parse import parse
import re
import pandas as pd
import utility.fitter_utility
import fitter.auto_fitter


app = Flask(__name__, static_folder='frontend')
output_folder = 'data/'


@app.route("/estimator")
def send_base():
    return send_from_directory(app.static_folder, 'estimator.html')


# http://10.100.208.81:8031/queue_for_calculation?particle_name=235nm+Web&cell_line_name=RAW+web&particle_radius=117.5e-9&particle_density=1850&culture_width=.022&culture_height=.0053&temperature=310.15&media_viscosity=.00101&media_density=1000&particle_concentration=100&total_cells=100000&cell_surface_area=830e-12&max_capacity=4&time0=0&particle0=0&time1=60&particle1=0.5&time2=120&particle2=0.75&time3=240&particle3=2&time4=480&particle4=4&time5=960&particle5=3&time6=1440&particle6=4&incubation_type=static
@app.route("/queue_for_calculation", methods=['GET'])
def queue_for_calculation():
    f = request.args
    id = create_ini_file(f)
    return redirect('/results/{}'.format(id), 303)


@app.route("/results/<id>")
def results(id):
    ini_path = os.path.join(output_folder, '{}.ini'.format(id))
    model_csv = utility.fitter_utility.get_csv_filename(config_file=ini_path,
                                                        model_desc=fitter.auto_fitter.model_desc)
    max_csv = utility.fitter_utility.get_csv_filename(config_file=ini_path,
                                                      model_desc=fitter.auto_fitter.max_detectable_model_desc)
    err_file = utility.fitter_utility.get_err_filename(config_file=ini_path,
                                                       model_desc=fitter.auto_fitter.model_desc)

    # finished_result_path = os.path.join(output_folder, '{}.csv'.format(id))
    if os.path.exists(model_csv):
        data = pd.read_csv(model_csv)
        max_data = None
        if os.path.exists(max_csv):
            max_data = pd.read_csv(max_csv)
        return render_template('complete_result.html', id=id, data=data, max_data=max_data)
    elif os.path.exists(err_file):
        return render_template('error_result.html', id=id)
    elif os.path.exists(ini_path):
        return render_template('waiting_result.html', id=id)
    else:
        abort(404)
        # return 'Your unique id is {}'.format(id)


@app.route("/result_img/<path:path>")
def result_img(path: str):
    if path_is_img(path):
        return send_from_directory(output_folder, path)
    else:
        abort(404)


def path_is_img(path):
    img_endings = ['png', 'jpg', 'jpeg']
    for ending in img_endings:
        if path.endswith('.{}'.format(ending)):
            return True
    return False


@app.route("/<path:path>")
def send_static(path):
    return send_from_directory('frontend', path)


def add_to_config_mandatory(config, section, value_name, source_dict, key):
    if key in source_dict:
        config.set(section, value_name, source_dict[key])
        return
    raise ValueError('Missing required value {}'.format(key))


def add_to_config_mandatory_decimal_in_range(config, section, value_name, source_dict, key, min, max):
    try:
        val = Decimal(source_dict[key])
        if min <= val and val <= max:
            config.set(section, value_name, val)
            return
        raise ValueError('Value {} must be between {} and {}'.format(key, min, max))

    except InvalidOperation:
        raise ValueError('Value {} must be a number'.format(key))

    except KeyError:
        raise ValueError('Missing required value {}'.format(key))


def add_to_config_vals_in_list(config, section, value_name, source_dict, key):
    try:
        val = source_dict[key]
        vals = val.split(',')
        x = 0  # to prevent accidental compiler optimization
        for i in range(0, len(vals)):
            x += Decimal(vals[i])
        config.set(section, value_name, '[{}]'.format(val))
        return

    except InvalidOperation:
        raise ValueError('Value {} must be a number or comma separated list of numbers'.format(key))
    except KeyError:
        raise ValueError('Missing required value {}'.format(key))


def add_time_pts_to_config(config, source_dict):
    time_point_keys = [k for k in source_dict.keys() if parse('time{:d}', k) is not None]
    for i in range(0, len(time_point_keys)):
        time_id = 'time{}'.format(i)
        particle_id = 'particle{}'.format(i)
        section = 'time_point_{}'.format(i)
        config.add_section(section)
        add_to_config_mandatory_decimal_in_range(config, section, 'time_in_mins', source_dict, time_id, 0, 1e10)
        add_to_config_vals_in_list(config, section, 'particles_per_cell', source_dict, particle_id)


def create_ini_file(args):
    # file_id = '{}_{}_{}'.format(args['particle_name'], args['cell_line_name'], uuid.uuid4())
    # file_id = secure_filename(file_id)
    safe_chars_regex = '[\W _-]+'
    safe_particle_name = re.sub(safe_chars_regex, '_', args['particle_name'])[:100]
    safe_cell_name = re.sub(safe_chars_regex, '_', args['cell_line_name'])[:100]

    file_id = '{}_{}_{}'.format(safe_particle_name, safe_cell_name, uuid.uuid4())
    file_name = '{}.ini'.format(file_id)
    file_path = os.path.join(output_folder, file_name)

    config = configparser.RawConfigParser()

    names = dict()
    names['particle_name'] = safe_particle_name
    names['cell_line_name'] = safe_cell_name
    add_to_config_mandatory(config, 'DEFAULT', 'particle_name', names, 'particle_name')
    add_to_config_mandatory(config, 'DEFAULT', 'cell_line_name', names, 'cell_line_name')

    add_time_pts_to_config(config, args)

    config.add_section('culture')
    add_to_config_mandatory_decimal_in_range(config, 'culture', 'particles_per_cell', args, 'particle_concentration', 1,
                                             1e7)

    config.add_section('cell')
    add_to_config_mandatory_decimal_in_range(config, 'cell', 'total_cells', args, 'total_cells', 1, 1e7)
    add_to_config_mandatory_decimal_in_range(config, 'cell', 'surface_area_per_cell_in_m2', args, 'cell_surface_area',
                                             Decimal(1e-10), Decimal(2e-2))

    config.add_section('particle')

    config.add_section('model')
    # add_to_config_mandatory_decimal_in_range(config, 'model', 'true_capacity', args, 'max_capacity', 0, 1e10)
    config.set('model', 'visualize', 'False')
    config.set('model', 'particles_per_cell_determined', 'True')

    add_to_config_mandatory_decimal_in_range(config, 'culture', 'width_in_m', args, 'culture_width', Decimal("1e-5"), Decimal("0.1"))
    add_to_config_mandatory_decimal_in_range(config, 'culture', 'height_in_m', args, 'culture_height', Decimal("1e-5"), Decimal("0.1"))
    add_to_config_mandatory_decimal_in_range(config, 'culture', 'temp_in_k', args, 'temperature', 1, 500)

    if (args['incubation_type'] == 'static'):
        add_to_config_mandatory_decimal_in_range(config, 'culture', 'media_viscosity_in_kg_per_m_s', args,
                                                 'media_viscosity', Decimal("1e-4"), Decimal("0.1"))
        add_to_config_mandatory_decimal_in_range(config, 'culture', 'media_density_in_kg_per_m3', args, 'media_density',
                                                 100, 10000)

        add_to_config_mandatory_decimal_in_range(config, 'particle', 'radius_in_m', args, 'particle_radius', Decimal("1e-9"), Decimal("1e-5"))
        add_to_config_mandatory_decimal_in_range(config, 'particle', 'density_in_kg_per_m3', args, 'particle_density',
                                                 500, 20000)
        config.set('model', 'condition', 'static_adherent')
    elif (args['incubation_type'] == 'well-mixed'):
        config.set('model', 'condition', 'well_mixed_adherent')
    elif (args['incubation_type'] == 'suspension'):
        config.set('model', 'condition', 'well_mixed_suspension')

    if args['incubation_type'] != 'static':
        config.set('culture', 'media_viscosity_in_kg_per_m_s', float('nan'))
        config.set('culture', 'media_density_in_kg_per_m3', float('nan'))
        config.set('particle', 'radius_in_m', float('nan'))
        config.set('particle', 'density_in_kg_per_m3', float('nan'))

    with open(file_path, 'w') as configfile:
        config.write(configfile)

    return file_id


if __name__ == "__main__":
    app.run(port=8031, debug=True, host='0.0.0.0')
