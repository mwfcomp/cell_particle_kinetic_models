# README #

This is a companion repository for work on kinetic modelling of cell-nanoparticle and microparticle interactions.  
It is used in the paper [Revisiting cell�particle association in vitro: A quantitative method to compare particle performance](https://doi.org/10.1016/j.jconrel.2019.06.027),
and a (hopefully) soon-to-be-published paper on modelling transcytosis.


### What is this repository for? ###

* If you're just interested in <i>applying</i> the analysis in [Revisiting cell�particle association in vitro: A quantitative method to compare particle performance](https://doi.org/10.1016/j.jconrel.2019.06.027) to your own data, you can visit http://bionano.xyz/estimator
* This repository is for people interested in developing their own alternative metrics, or examining how our metric is calculated. 


### How do I get set up? ###

* The easiest way to set up is to use the provided docker files to provision a docker container. 
* If you do not want to use docker, the code is written in Python, and has a number of python library dependencies; the most difficult of these is installing FEniCS (https://fenicsproject.org/) for python v3. Other requirements can be installed fairly trivially through pip
* The "scripts" folder contains a number of useful scripts for analyzing flow cytometric data and calculating the cell particle association affinity
* The most useful of these scripts are likely "visualize_incubation.py" which will allow you to see the particles per cell using an INI and FCS files, and "calculate_targeting_efficiency.py" which calculates the CPAA of a particular system. Both of these files require an INI file as input

### How do I replicate the computational results in the transcytosis paper ###
1. Install docker on your machine
2. Clone this repository. This repository will assume it is located in the directory ```~/cpkm```
3. Download the supplementary .lst and .ini files that describe the experimental data in a machine readable way. These instructions assume these are located in ```~/transcytosis_data```
4. Build a docker image that will allow you to run the results: 
```
cd ~/cpkm/docker_setup
docker build -f BaseDockerfile -t cpkm_base ..
```
5. Run an interactive shell in the newly created docker 
```
docker run -v ~/transcytosis_data:/app/source/website/data --name transcytosis -it cpkm_base bash
```
6. Within the docker, navigate to the correct directory and generate the results
```
cd /app/source/scripts
python3 fit_transcytosis.py --c /app/source/website/data/combined.lst --t /app/source/website/data/transcytosis.lst
```
7. This script will fit the required data and generate PNG images containing output graphs of the experimental data versus the fit (commputational model) data. 

### Who do I talk to? ###

* See the authors of the papers for more details on setup, code, etc

### License? ###
Standard MIT license: 

Copyright 2019

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.